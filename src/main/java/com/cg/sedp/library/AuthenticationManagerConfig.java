package com.cg.sedp.library;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.vaadin.spring.security.config.AuthenticationManagerConfigurer;

import com.cg.sedp.library.ccc.user.common.UserEto;
import com.cg.sedp.library.ccc.user.common.UserException;
import com.cg.sedp.library.ccc.user.common.UserIdType;
import com.cg.sedp.library.ccc.user.service.api.UserCrudService;
import com.cg.sedp.library.system.security.Authorities;

/**
 * Authentication based on database lookup of user.
 */
@Configuration
public class AuthenticationManagerConfig implements AuthenticationManagerConfigurer {

	@Autowired
	private UserCrudService userService;

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(new UserDetailsService() {
			@Override
			public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
				UserEto userEto;
				try {
					userEto = userService.getUser(new UserIdType(username));
					return new UserDetailsWrapper(userEto);
				} catch (UserException e) {
					// TODO handle generic exception better
					e.printStackTrace();
					throw new UsernameNotFoundException("User '" + username + "' not found.");
				}
			}
		});
	}

	/**
	 * Wrapping a user transport object as a {@link UserDetails}.
	 */
	private class UserDetailsWrapper implements UserDetails {

		private static final long serialVersionUID = 1L;

		private UserEto userEto;

		public UserDetailsWrapper(UserEto userEto) {
			this.userEto = userEto;
		}

		@Override
		public Collection<? extends GrantedAuthority> getAuthorities() {
			Collection<GrantedAuthority> returnValue = new ArrayList<>();
			if (userEto.getNormalUserRight()) {
				returnValue.add(Authorities.LIBRARY_USER_AUTHORITY);
			}
			if (userEto.getAdminUserRight()) {
				returnValue.add(Authorities.ADMIN_USER_AUTHORITY);
			}
			if (userEto.getDeveloperUserRight()) {
				returnValue.add(Authorities.DEVELOPER_AUTHORITY);
			}
			return returnValue;
		}

		@Override
		public String getPassword() {
			return userEto.getEncryptedPassword();
		}

		@Override
		public String getUsername() {
			return userEto.getUserId().getUserIdString();
		}

		@Override
		public boolean isAccountNonExpired() {
			return true;
		}

		@Override
		public boolean isAccountNonLocked() {
			return true;
		}

		@Override
		public boolean isCredentialsNonExpired() {
			return true;
		}

		@Override
		public boolean isEnabled() {
			return true;
		}

	}

}