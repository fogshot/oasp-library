package com.cg.sedp.library.branch.borrow.common;


import com.cg.sedp.library.branch.catalog.common.MediaItemEto;

public class BorrowViewReservedItemCto {

	private MediaItemEto mediaItemEto;
	private BorrowViewReservedItemEto reservedItemEto;
	
	public BorrowViewReservedItemCto() {
		
	}

	public BorrowViewReservedItemCto(BorrowViewReservedItemEto reservedItemEto, MediaItemEto mediaItemEto) {
		super();
		this.setMediaItemEto(mediaItemEto);
		this.setReservedItemEto(reservedItemEto);
	}

	public MediaItemEto getMediaItemEto() {
		return mediaItemEto;
	}

	public void setMediaItemEto(MediaItemEto mediaItemEto) {
		this.mediaItemEto = mediaItemEto;
	}

	public BorrowViewReservedItemEto getReservedItemEto() {
		return reservedItemEto;
	}

	public void setReservedItemEto(BorrowViewReservedItemEto reservedItemEto) {
		this.reservedItemEto = reservedItemEto;
	}
	
	
}
