package com.cg.sedp.library.branch.borrow.common;

import java.sql.Date;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;

public class BorrowViewReservedItemEto {

	private MediaItemIdentifier mediaItemIdentifier;
	private Date reservedUpTo;
	
	public BorrowViewReservedItemEto() {
		
	}

	public BorrowViewReservedItemEto(MediaItemIdentifier mediaItemIdentifier, Date reservedUpTo) {
		this();
		this.setMediaItemIdentifier(mediaItemIdentifier);
		this.setReservedUpTo(reservedUpTo);
	}

	public MediaItemIdentifier getMediaItemIdentifier() {
		return mediaItemIdentifier;
	}

	public void setMediaItemIdentifier(MediaItemIdentifier mediaItemIdentifier) {
		this.mediaItemIdentifier = mediaItemIdentifier;
	}

	public Date getReservedUpTo() {
		return reservedUpTo;
	}

	public void setReservedUpTo(Date reservedUpTo) {
		this.reservedUpTo = reservedUpTo;
	}
	
}
