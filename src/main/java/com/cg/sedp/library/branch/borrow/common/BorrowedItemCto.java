package com.cg.sedp.library.branch.borrow.common;

import com.cg.sedp.library.branch.catalog.common.MediaItemEto;

public class BorrowedItemCto {

	private MediaItemEto mediaItemEto;
	private BorrowedItemEto borrowedItemEto;
	
	public BorrowedItemCto(MediaItemEto mediaItemEto,BorrowedItemEto borrowedItemEto) {
		super();
		this.setMediaItemEto(mediaItemEto);
		this.setBorrowedItemEto(borrowedItemEto);
	}

	public MediaItemEto getMediaItemEto() {
		return mediaItemEto;
	}

	public void setMediaItemEto(MediaItemEto mediaItemEto) {
		this.mediaItemEto = mediaItemEto;
	}
	
	public BorrowedItemEto getBorrowedItemEto() {
		return borrowedItemEto;
	}

	public void setBorrowedItemEto(BorrowedItemEto borrowedItemEto) {
		this.borrowedItemEto = borrowedItemEto;
	}
	
}
