package com.cg.sedp.library.branch.borrow.common;

import java.util.Date;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.ccc.user.common.UserIdType;

public class BorrowedItemEto {

	private MediaItemIdentifier mediaItemIdentifier;
	private Date returnBy;
	private int extensionCounter;
	private Date extendedUpTo;
	private UserIdType borrowedBy;

	public BorrowedItemEto() {

	}

	public BorrowedItemEto(MediaItemIdentifier mediaItemIdentifier, Date returnBy, int extensionCounter,
			Date extendedUpTo, UserIdType borrowedBy) {
		setMediaItemIdentifier(mediaItemIdentifier);
		setReturnBy(returnBy);
		setExtensionCounter(extensionCounter);
		setExtendedUpTo(extendedUpTo);
		setBorrowedBy(borrowedBy);
	}

	public MediaItemIdentifier getMediaItemIdentifier() {
		return mediaItemIdentifier;
	}

	public void setMediaItemIdentifier(MediaItemIdentifier mediaItemIdentifier) {
		this.mediaItemIdentifier = mediaItemIdentifier;
	}

	public Date getReturnBy() {
		return returnBy;
	}

	public void setReturnBy(Date returnBy) {
		this.returnBy = returnBy;
	}

	public int getExtensionCounter() {
		return extensionCounter;
	}

	public void setExtensionCounter(int extensionCounter) {
		this.extensionCounter = extensionCounter;
	}

	public Date getExtendedUpTo() {
		return extendedUpTo;
	}

	public void setExtendedUpTo(Date extendedUpTo) {
		this.extendedUpTo = extendedUpTo;
	}

	public void setBorrowedBy(UserIdType borrowedBy) {
		this.borrowedBy = borrowedBy;
	}

	public UserIdType getBorrowedBy() {
		return borrowedBy;
	}
}
