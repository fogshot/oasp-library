package com.cg.sedp.library.branch.borrow.gui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.cg.sedp.library.branch.borrow.common.BorrowViewReservedItemCto;
import com.cg.sedp.library.branch.borrow.common.BorrowedItemCto;
import com.cg.sedp.library.branch.borrow.service.api.BorrowUiService;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifierVaadinConverter;
import com.cg.sedp.library.branch.catalog.common.MediaItemTypeVaadinConverter;
import com.cg.sedp.library.ccc.user.common.UserException;
import com.cg.sedp.library.ccc.user.service.api.UserInformationService;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.GeneratedPropertyContainer;
import com.vaadin.data.util.PropertyValueGenerator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.ButtonRenderer;

@ViewScope
@SpringView(name = BorrowView.VIEW_NAME)
public class BorrowView extends VerticalLayout implements View {

	public static final String VIEW_NAME = "borrow-view";

	private static final long serialVersionUID = 1L;

	private Grid gridBorrowedItems = new Grid();
	private Grid gridReservedItems = new Grid();

	@Autowired
	@Qualifier(value = "Impl")
	private BorrowUiService borrowUiService;

	@Autowired
	private UserInformationService userInformationService;

	@PostConstruct
	private void init()  {
		setMargin(true);
		setSpacing(true);
		configureWidgets();
		addComponent(buildLayout());

		// initial refresh data
		refreshBorrowedItems();
		refreshReservedItems();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// the view is constructed in init() method
	}

	private void configureWidgets() {

		gridBorrowedItems.setContainerDataSource(newBeanItemContainer(BorrowedItemCto.class, Collections.emptyList()));
		// order
		gridBorrowedItems.setColumnOrder( //
				"mediaItemEto.mediaItemIdentifier", //
				"mediaItemEto.title", //
				"mediaItemEto.author", //
				"mediaItemEto.mediaItemType", //

				"borrowedItemEto.returnBy", //
				"borrowedItemEto.extensionCounter", //
				"borrowedItemEto.extendedUpTo", //
				"extend");

		// columns
		gridBorrowedItems.getColumn("mediaItemEto.mediaItemIdentifier").setHeaderCaption("Media ID");
		gridBorrowedItems.getColumn("mediaItemEto.mediaItemIdentifier")
				.setConverter(new MediaItemIdentifierVaadinConverter());
		gridBorrowedItems.getColumn("mediaItemEto.title").setHeaderCaption("Title");
		gridBorrowedItems.getColumn("mediaItemEto.author").setHeaderCaption("Author");
		gridBorrowedItems.getColumn("mediaItemEto.mediaItemType").setHeaderCaption("Type");
		gridBorrowedItems.getColumn("mediaItemEto.mediaItemType").setConverter(new MediaItemTypeVaadinConverter());
		gridBorrowedItems.getColumn("extend").setHeaderCaption("Extension");
		gridBorrowedItems.removeColumn("borrowedItemEto.mediaItemIdentifier");

		// Create a button for the extend column that triggers extension
		gridBorrowedItems.getColumn("extend")
				.setRenderer(new ButtonRenderer(e -> doExtend((BorrowedItemCto) e.getItemId())));

		gridBorrowedItems.setSelectionMode(Grid.SelectionMode.NONE);

		// reservation
		gridReservedItems.setContainerDataSource(
				newReservedBeanItemContainer(BorrowViewReservedItemCto.class, Collections.emptyList()));
		gridReservedItems.setColumnOrder( //
				"mediaItemEto.mediaItemIdentifier", //
				"mediaItemEto.title", //
				"mediaItemEto.author", //
				"mediaItemEto.mediaItemType", //

				"reservedItemEto.reservedUpTo" //
		);

		// do not display direct member/properties
		// gridReservedItems.removeColumn("mediaItemEto");
		// gridReservedItems.removeColumn("reservedItemEto");

		// columns
		gridReservedItems.getColumn("mediaItemEto.mediaItemIdentifier").setHeaderCaption("Media ID.");
		gridReservedItems.getColumn("mediaItemEto.mediaItemIdentifier")
				.setConverter(new MediaItemIdentifierVaadinConverter());
		gridReservedItems.getColumn("mediaItemEto.title").setHeaderCaption("Title");
		gridReservedItems.getColumn("mediaItemEto.author").setHeaderCaption("Author");
		gridReservedItems.getColumn("mediaItemEto.mediaItemType").setHeaderCaption("Type");
		gridReservedItems.getColumn("mediaItemEto.mediaItemType").setConverter(new MediaItemTypeVaadinConverter());

		gridReservedItems.getColumn("reservedItemEto.reservedUpTo").setHeaderCaption("reserved up to");

		gridReservedItems.setSelectionMode(Grid.SelectionMode.SINGLE);

	}

	private HorizontalLayout buildLayout() {

		VerticalLayout filterAndNewButtonAndList = new VerticalLayout(gridBorrowedItems, gridReservedItems);
		filterAndNewButtonAndList.setSpacing(true);
		filterAndNewButtonAndList.setSizeFull();
		gridBorrowedItems.setSizeFull();
		filterAndNewButtonAndList.setExpandRatio(gridBorrowedItems, 1);

		gridReservedItems.setSizeFull();

		HorizontalLayout mainLayout = new HorizontalLayout(filterAndNewButtonAndList);
		mainLayout.setSizeFull();
		mainLayout.setExpandRatio(filterAndNewButtonAndList, 1);

		return mainLayout;
	}

	// ####################################################
	// ##
	// ## refresh
	// ##

	private <BeanType> Container.Indexed newBeanItemContainer(Class<BeanType> clazz, Collection<BeanType> col) {
		BeanItemContainer<BeanType> container = new BeanItemContainer<>(clazz, col);

		// Make the nested ETOs inside BorrowedItemCto known to the container

		container.addNestedContainerBean("mediaItemEto");
		container.addNestedContainerBean("borrowedItemEto");

		// Dynamically generate a property that will represent the extend button
		// column
		GeneratedPropertyContainer dynamicColumnGeneratorContainer = new GeneratedPropertyContainer(container);
		dynamicColumnGeneratorContainer.addGeneratedProperty("extend", new PropertyValueGenerator<String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public String getValue(Item item, Object itemId, Object propertyId) {
				return "Extend";
			}

			@Override
			public Class<String> getType() {
				return String.class;
			}
		});

		return dynamicColumnGeneratorContainer;
	}

	private <BeanType> Container.Indexed newReservedBeanItemContainer(Class<BeanType> clazz, Collection<BeanType> col) {
		BeanItemContainer<BeanType> container = new BeanItemContainer<>(clazz, col);

		// Make the nested ETOs inside BorrowedItemCto known to the container

		container.addNestedContainerBean("mediaItemEto");
		container.addNestedContainerBean("reservedItemEto");

		// Dynamically container (to return same type as {@link
		// #newReservedBeanItemContainer(..).
		GeneratedPropertyContainer dynamicColumnGeneratorContainer = new GeneratedPropertyContainer(container);

		return dynamicColumnGeneratorContainer;
	}

	private void refreshBorrowedItems() {
		// do search
		List<BorrowedItemCto> all;
		try {
			all = borrowUiService.getAllBorrowedMediaForUser(userInformationService.getUser().getUserId());
			// set model
			gridBorrowedItems.setContainerDataSource(newBeanItemContainer(BorrowedItemCto.class, all));
		} catch (UserException e) {
			// TODO [pvonnieb]: handle user exception
			e.printStackTrace();
		}
	}

	private void refreshReservedItems() {
		// do search
		try {
			List<BorrowViewReservedItemCto> all = borrowUiService
					.getAllReservedMediaForUser(userInformationService.getUser().getUserId());
			// set model
			gridReservedItems
					.setContainerDataSource(newReservedBeanItemContainer(BorrowViewReservedItemCto.class, all));
		} catch (UserException e) {
			// TODO [pvonnieb]: handle user exception
			e.printStackTrace();
		}
	}

	// ####################################################
	// ##
	// ## actions
	// ##

	public void doExtend(BorrowedItemCto item) {
		// TODO: connect to service and process item
		refreshBorrowedItems();
	}
}