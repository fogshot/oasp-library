package com.cg.sedp.library.branch.borrow.logic.api;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.cg.sedp.library.branch.borrow.common.AlreadyBorrowedException;
import com.cg.sedp.library.branch.borrow.common.BorrowedItemEto;
import com.cg.sedp.library.branch.borrow.common.NotBorrowedException;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.ccc.user.common.UserIdType;


public interface BorrowedMedia {

	/**
	 * Get all borrowed media from a specified user.
	 * @param userId the user.
	 * @return a list of borrowed {@link MediaItemIdentifier}.
	 */
	List<BorrowedItemEto> getBorrowedMediaIDs(@NotNull UserIdType userId);
	
	/**
	 * Get all borrowed media.
	 * @return a list of borrowed {@link MediaItemIdentifier}.
	 */
	List<BorrowedItemEto> getAllBorrowedMediaIDs();
	
	/**
	 * Returns the information if a media is borrowed.
	 * @param mediaId the media which has to be checked.
	 * @return 	<code>true</code>, if the media is borrowed, 
	 * 			<code>false</code> otherwise (when the media is not borrowed). 
	 * 
	 */
	boolean isBorrowed(MediaItemIdentifier mediaId);
	
	/**
	 * Get the number of borrowed media for a specified user.
	 * @param userId the user.
	 * @return a number of borrowed media for the user.
	 */
	int getNumberOfBorrowedMedia(UserIdType userId);
	
	/**
	 * Get the next date when a borrowed media will expired for a user
	 * @param userId the user.
	 * @return the next date when the user has to return the media.
	 */
	Date getNextExpirationDate(UserIdType userId);
	
	/**
	 * Get the user for a specific borrowed media.
	 * @param mediaId the media id which a user has borrowed.
	 * @return the user, who has borrowed the media or <code>null</code> when no user has borrowed this media.
	 * @throws NotBorrowedException 
	 */
	UserIdType getUserIdWhoBorrowed(MediaItemIdentifier mediaId) throws NotBorrowedException;
	
	/**
	 * Unborrow a media for a specific user.
	 * @param mediaId the media, which will be returned from the user.
	 * @param userId the user, who returns the media.
	 * @throws NotBorrowedException if the media is not borrowed, so that it can't be unborrow.
	 */
	void unborrow(MediaItemIdentifier mediaId, UserIdType userId) throws NotBorrowedException;
	
	/**
	 * Borrow a media for a specific user.
	 * @param mediaId the media, which will be borrow from the user.
	 * @param userId the user, who borrows the media.
	 * @throws AlreadyBorrowedException if the media is already borrowed by another user.
	 */
	void borrow(MediaItemIdentifier mediaId, UserIdType userId) throws AlreadyBorrowedException; 
	
}
