package com.cg.sedp.library.branch.borrow.logic.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cg.sedp.library.branch.borrow.common.AlreadyBorrowedException;
import com.cg.sedp.library.branch.borrow.common.BorrowedItemEto;
import com.cg.sedp.library.branch.borrow.common.NotBorrowedException;
import com.cg.sedp.library.branch.borrow.logic.api.BorrowedMedia;
import com.cg.sedp.library.branch.borrow.persistence.api.BorrowEntryRepositoryDao;
import com.cg.sedp.library.branch.borrow.persistence.entity.BorrowEntry;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.ccc.user.common.UserIdType;

@Service
public class BorrowedMediaImpl implements BorrowedMedia {

	
	@Autowired
	private BorrowEntryRepositoryDao borrowEntryRepositoryDao;
	
	@Override
	public List<BorrowedItemEto> getBorrowedMediaIDs(UserIdType userId) {
		
		return borrowEntryRepositoryDao.findByBorrowedBy(userId)
				.stream()
				.map(this::mapBorrowedItem)
				.collect(Collectors.toList());
	}


	@Override
	public List<BorrowedItemEto> getAllBorrowedMediaIDs() {
				
		return borrowEntryRepositoryDao.findAll()
				.stream()
				.map(this::mapBorrowedItem)
				.collect(Collectors.toList());
	}
	

	@Override
	public boolean isBorrowed(MediaItemIdentifier mediaId) {
		if (borrowEntryRepositoryDao.findByMediaItemIdentifier(mediaId) != null) {
			return true;
		} else {		
		return false;
		}
	}


	@Override
	public int getNumberOfBorrowedMedia(UserIdType userId) {
		
		return getBorrowedMediaIDs(userId).size();
	}


	@Override
	public Date getNextExpirationDate(UserIdType userId) {
		
		borrowEntryRepositoryDao.findByBorrowedBy(userId)
			.stream()
			.map(this::mapBorrowedItem)
			.collect(Collectors.toList());
		
		return null;
	}


	@Override
	public UserIdType getUserIdWhoBorrowed(MediaItemIdentifier mediaId) throws NotBorrowedException {
		
		if (borrowEntryRepositoryDao.findByMediaItemIdentifier(mediaId) != null) {
			return borrowEntryRepositoryDao.findByMediaItemIdentifier(mediaId).getBorrowedBy();
		} else {
			throw new NotBorrowedException();	
		}
	}


	@Override
	public void unborrow(MediaItemIdentifier mediaId, UserIdType userId) throws NotBorrowedException {
		
		//List<BorrowedItemEto> borrowedItemEto = getBorrowedMediaIDs(userId);
		
		//if (borrowEntryRepositoryDao.findByMediaItemIdentifier(mediaId) && borrowEntryRepositoryDao.) {
		BorrowEntry findByMediaItemIdentifierAndBorrowedBy = borrowEntryRepositoryDao.findByMediaItemIdentifierAndBorrowedBy(mediaId, userId);
		borrowEntryRepositoryDao.delete(findByMediaItemIdentifierAndBorrowedBy);
	}


	@Override
	public void borrow(MediaItemIdentifier mediaId, UserIdType userId) throws AlreadyBorrowedException {
		
		if (borrowEntryRepositoryDao.findByMediaItemIdentifier(mediaId) != null) {
			
			int extensionCounter = 0;
			Date extendedUpTo = null;
			Date returnBy = increaseDateByThirtyDays(new Date());
			
			BorrowedItemEto borrowedItemEto = new BorrowedItemEto(mediaId, returnBy, extensionCounter, extendedUpTo, userId);
			
			//TO DO Map EDO To Entity
			borrowEntryRepositoryDao.save(mapBorrowEntry(borrowedItemEto));
			
		} else {
			throw new AlreadyBorrowedException();
		}
		
	}
	
	private Date increaseDateByThirtyDays (Date originalDate) {
			
		Calendar cal = Calendar.getInstance();
		cal.setTime(originalDate);
		cal.add(Calendar.DATE, 30);

		return cal.getTime();	
		}
		
	
	private BorrowedItemEto mapBorrowedItem(BorrowEntry borrowEntry) {
		BorrowedItemEto borrowedItemEto = new BorrowedItemEto();
		borrowedItemEto.setBorrowedBy(borrowEntry.getBorrowedBy());
		borrowedItemEto.setMediaItemIdentifier(borrowEntry.getMediaItemIdentifier());
		borrowedItemEto.setExtendedUpTo(borrowEntry.getExtendedUpTo());
		borrowedItemEto.setExtensionCounter(borrowEntry.getExtensionCounter());
		borrowedItemEto.setReturnBy(borrowEntry.getReturnBy());
		return borrowedItemEto;
	}
	
	private BorrowEntry mapBorrowEntry(BorrowedItemEto borrowedItemEto) {
		BorrowEntry borrowEntry = new BorrowEntry();
		borrowEntry.setBorrowedBy(borrowedItemEto.getBorrowedBy());
		borrowEntry.setMediaItemIdentifier(borrowedItemEto.getMediaItemIdentifier());
		borrowEntry.setExtendedUpTo(borrowedItemEto.getExtendedUpTo());
		borrowEntry.setExtensionCounter(borrowedItemEto.getExtensionCounter());
		borrowEntry.setReturnBy(borrowedItemEto.getReturnBy());
		return borrowEntry;
	}
	
	
	
	
	
}
