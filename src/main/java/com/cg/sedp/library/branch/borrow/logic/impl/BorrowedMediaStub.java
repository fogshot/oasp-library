package com.cg.sedp.library.branch.borrow.logic.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;

import com.cg.sedp.library.branch.borrow.common.AlreadyBorrowedException;
import com.cg.sedp.library.branch.borrow.common.BorrowedItemEto;
import com.cg.sedp.library.branch.borrow.common.NotBorrowedException;
import com.cg.sedp.library.branch.borrow.logic.api.BorrowedMedia;
import com.cg.sedp.library.branch.borrow.persistence.entity.BorrowEntry;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.ccc.user.common.UserIdType;

//@Service
@Qualifier(value="Stub")
public class BorrowedMediaStub implements BorrowedMedia {

	
	@Override
	public List<BorrowedItemEto> getBorrowedMediaIDs(UserIdType userId) {
		// TODO Auto-generated method stub
		BorrowedItemEto btEto = new BorrowedItemEto(new MediaItemIdentifier("1"), null, 0, null, null);
		List<BorrowedItemEto> list = new ArrayList<BorrowedItemEto>();
		list.add(btEto);
		return list;
	}


	@Override
	public List<BorrowedItemEto> getAllBorrowedMediaIDs() {
		// TODO Auto-generated method stub
		BorrowedItemEto borrowedItemEto = new BorrowedItemEto(null, null, 0, null, null);
		List<BorrowedItemEto> list = new ArrayList<BorrowedItemEto>();
		list.add(borrowedItemEto);
		return list;
	}


	@Override
	public boolean isBorrowed(MediaItemIdentifier mediaId) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public int getNumberOfBorrowedMedia(UserIdType userId) {
		// TODO Auto-generated method stub
		return 3;
	}


	@Override
	public Date getNextExpirationDate(UserIdType userId) {
		// TODO Auto-generated method stub
		return new Date();
	}


	@Override
	public UserIdType getUserIdWhoBorrowed(MediaItemIdentifier mediaId) {
		UserIdType userIdType = new UserIdType("ABC");
		return userIdType;
	}


	@Override
	public void unborrow(MediaItemIdentifier mediaId, UserIdType userId) throws NotBorrowedException {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void borrow(MediaItemIdentifier mediaId, UserIdType userId) throws AlreadyBorrowedException {
		// TODO Auto-generated method stub
		
	}
	
	public BorrowedItemEto mapBorrowedItem(BorrowEntry borrowEntry) {
		BorrowedItemEto borrowedItemEto = new BorrowedItemEto();
		borrowedItemEto.setBorrowedBy(new UserIdType("ABC"));
		borrowedItemEto.setMediaItemIdentifier(new MediaItemIdentifier());
		borrowedItemEto.setExtendedUpTo(new Date());
		borrowedItemEto.setExtensionCounter(1);
		borrowedItemEto.setReturnBy(new Date());
		return borrowedItemEto;
	}
	
	
	
	
}
