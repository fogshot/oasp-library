package com.cg.sedp.library.branch.borrow.persistence.api;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cg.sedp.library.branch.borrow.persistence.entity.BorrowEntry;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.ccc.user.common.UserIdType;



public interface BorrowEntryRepositoryDao extends JpaRepository<BorrowEntry, Long> {

	 @NotNull
	    List<BorrowEntry> findByBorrowedBy(@NotNull UserIdType borrowedBy);
	 
	 @NotNull
	    BorrowEntry findByMediaItemIdentifier(@NotNull MediaItemIdentifier mediaItem);
	 
	 @NotNull
	    BorrowEntry findByMediaItemIdentifierAndBorrowedBy(@NotNull MediaItemIdentifier mediaItem, @NotNull UserIdType borrowedBy);
	
	 
}
