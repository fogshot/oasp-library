package com.cg.sedp.library.branch.borrow.persistence.entity;

import java.util.Date;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifierConverter;
import com.cg.sedp.library.ccc.user.common.UserIdType;
import com.cg.sedp.library.ccc.user.common.UserIdTypeConverter;


@Entity
public class BorrowEntry {

	@Id
	@GeneratedValue
	private long id;

	@Convert (converter = MediaItemIdentifierConverter.class)
	private MediaItemIdentifier mediaItemIdentifier;
	
	@NotNull
	private Date returnBy;
	
	private int extensionCounter;
	private Date extendedUpTo;
	
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "userId")
	@Convert (converter = UserIdTypeConverter.class)
	private UserIdType borrowedBy;
	
	
	
	public BorrowEntry() {
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public MediaItemIdentifier getMediaItemIdentifier() {
		return mediaItemIdentifier;
	}

	public void setMediaItemIdentifier(MediaItemIdentifier mediaItemIdentifier) {
		this.mediaItemIdentifier = mediaItemIdentifier;
	}

	public Date getReturnBy() {
		return returnBy;
	}

	public void setReturnBy(Date returnBy) {
		this.returnBy = returnBy;
	}

	public int getExtensionCounter() {
		return extensionCounter;
	}

	public void setExtensionCounter(int extensionCounter) {
		this.extensionCounter = extensionCounter;
	}

	public Date getExtendedUpTo() {
		return extendedUpTo;
	}

	public void setExtendedUpTo(Date extendedUpTo) {
		this.extendedUpTo = extendedUpTo;
	}

	public void setBorrowedBy(UserIdType borrowedBy) {
		this.borrowedBy = borrowedBy;
	}

	public UserIdType getBorrowedBy() {
		return borrowedBy;
	
	} 
	}
	
	
