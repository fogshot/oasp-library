package com.cg.sedp.library.branch.borrow.service.api;

import com.cg.sedp.library.branch.borrow.common.AlreadyBorrowedException;
import com.cg.sedp.library.branch.borrow.common.NotBorrowedException;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.ccc.user.common.UserIdType;

/**
 * This interface provide the functionality to borrow and unborrow a media for a user.
 * @author chuber
 *
 */
public interface BorrowExecutionService {

	/**
	 * Unborrow a media for a specific user.
	 * @param mediaId the media, which will be returned from the user.
	 * @param userId the user, who returns the media.
	 * @throws NotBorrowedException if the media is not borrowed, so that it can't be unborrow.
	 */
	void unborrow(MediaItemIdentifier mediaId, UserIdType userId) throws NotBorrowedException;
	
	/**
	 * Borrow a media for a specific user.
	 * @param mediaId the media, which will be borrow from the user.
	 * @param userId the user, who borrows the media.
	 * @throws AlreadyBorrowedException if the media is already borrowed by another user.
	 */
	void borrow(MediaItemIdentifier mediaId, UserIdType userId) throws AlreadyBorrowedException; 
}
