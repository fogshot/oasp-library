package com.cg.sedp.library.branch.borrow.service.api;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.cg.sedp.library.branch.borrow.common.BorrowedItemEto;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.ccc.user.common.UserIdType;

/**
 * This Interface provides information about borrowed media. 
 * @author chuber
 *
 */
public interface BorrowInformationService {

	/**
	 * Get all borrowed media from a specified user.
	 * @param userId the user.
	 * @return a list of borrowed {@link MediaItemIdentifier}.
	 */
	List<MediaItemIdentifier> getBorrowedMediaIDs(@NotNull UserIdType userId);
	
	/**
	 * Get all borrowed media.
	 * @return a list of borrowed {@link MediaItemIdentifier}.
	 */
	List<BorrowedItemEto> getAllBorrowedMediaIDs();
	
	/**
	 * Returns the information if a media is borrowed.
	 * @param mediaId the media which has to be checked.
	 * @return 	<code>true</code>, if the media is borrowed, 
	 * 			<code>false</code> otherwise (when the media is not borrowed). 
	 * 
	 */
	boolean isBorrowed(MediaItemIdentifier mediaId);
	
	/**
	 * Get the number of borrowed media for a specified user.
	 * @param userId the user.
	 * @return a number of borrowed media for the user.
	 */
	int getNumberOfBorrowedMedia(UserIdType userId);
	
	/**
	 * Get the next date when a borrowed media will expired for a user
	 * @param userId the user.
	 * @return the next date when the user has to return the media.
	 */
	Date getNextExpirationDate(UserIdType userId);
	
	/**
	 * Get the user for a specific borrowed media.
	 * @param mediaId the media id which a user has borrowed.
	 * @return the user, who has borrowed the media or <code>null</code> when no user has borrowed this media.
	 */
	UserIdType getUserIdWhoBorrowed(MediaItemIdentifier mediaId);
	
}
