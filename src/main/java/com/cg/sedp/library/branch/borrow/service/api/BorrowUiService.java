package com.cg.sedp.library.branch.borrow.service.api;

import java.util.List;

import com.cg.sedp.library.branch.borrow.common.BorrowViewReservedItemCto;
import com.cg.sedp.library.branch.borrow.common.BorrowedItemCto;
import com.cg.sedp.library.branch.borrow.common.NoMoreExtensionException;
import com.cg.sedp.library.ccc.user.common.UserIdType;

public interface BorrowUiService {

	List<BorrowedItemCto> getAllBorrowedMediaForUser(UserIdType userId);
	
	boolean extendBorrowedMedia(BorrowedItemCto borrowedItemCto) throws NoMoreExtensionException;
	
	List<BorrowViewReservedItemCto> getAllReservedMediaForUser(UserIdType userId);
}
