package com.cg.sedp.library.branch.borrow.service.impl;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cg.sedp.library.branch.borrow.common.AlreadyBorrowedException;
import com.cg.sedp.library.branch.borrow.common.NotBorrowedException;
import com.cg.sedp.library.branch.borrow.logic.api.BorrowedMedia;
import com.cg.sedp.library.branch.borrow.service.api.BorrowExecutionService;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.ccc.user.common.UserIdType;

@Service
//@Qualifier(value="Impl")
public class BorrowExecutionServiceImpl implements BorrowExecutionService {

	@Autowired
	private BorrowedMedia borrowMedia;
	
	private static Logger LOGGER = Logger.getAnonymousLogger();
	
	@Override
	public void unborrow(MediaItemIdentifier mediaId, UserIdType userId) throws NotBorrowedException {
		// TODO Auto-generated method stub
		LOGGER.info("The media " + mediaId + " will be unborrowed from " + userId);
		borrowMedia.unborrow(mediaId, userId);
	}

	@Override
	public void borrow(MediaItemIdentifier mediaId, UserIdType userId) throws AlreadyBorrowedException {
		// TODO Auto-generated method stub
		LOGGER.info("The media " + mediaId + " will be borrowed from " + userId);
		borrowMedia.borrow(mediaId, userId);
		
	}

}
