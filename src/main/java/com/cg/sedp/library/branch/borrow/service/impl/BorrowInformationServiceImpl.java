/**
 * 
 */
package com.cg.sedp.library.branch.borrow.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.cg.sedp.library.branch.borrow.common.BorrowedItemEto;
import com.cg.sedp.library.branch.borrow.common.NotBorrowedException;
import com.cg.sedp.library.branch.borrow.logic.api.BorrowedMedia;
import com.cg.sedp.library.branch.borrow.service.api.BorrowInformationService;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.ccc.user.common.UserIdType;

/**
 * An implementation for {@link BorrowInformationService}
 * 
 * @author chuber
 *
 */

@Service
@Qualifier(value = "Impl")
public class BorrowInformationServiceImpl implements BorrowInformationService {

	@Autowired
	private BorrowedMedia borrowedMedia;

	@Override
	public List<MediaItemIdentifier> getBorrowedMediaIDs(UserIdType userId) {
		List<BorrowedItemEto> borrowedMediaIDs = borrowedMedia.getBorrowedMediaIDs(userId);

		List<MediaItemIdentifier> mediaItemIdentifiers = new ArrayList<>();
		for (BorrowedItemEto borrowedItemEto : borrowedMediaIDs) {
			mediaItemIdentifiers.add(borrowedItemEto.getMediaItemIdentifier());
		}
		return mediaItemIdentifiers;
	}

	@Override
	public List<BorrowedItemEto> getAllBorrowedMediaIDs() {
		List<BorrowedItemEto> allBorrowedMediaIDs = borrowedMedia.getAllBorrowedMediaIDs();
		return allBorrowedMediaIDs;
	}

	@Override
	public boolean isBorrowed(MediaItemIdentifier mediaId) {
		
		return borrowedMedia.isBorrowed(mediaId);
	}

	@Override
	public int getNumberOfBorrowedMedia(UserIdType userId) {
		
		return borrowedMedia.getNumberOfBorrowedMedia(userId);
	}

	@Override
	public Date getNextExpirationDate(UserIdType userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserIdType getUserIdWhoBorrowed(MediaItemIdentifier mediaId) {
		
		try {
			return borrowedMedia.getUserIdWhoBorrowed(mediaId);
		} catch (NotBorrowedException e) {
			return null;
		}
	}

}
