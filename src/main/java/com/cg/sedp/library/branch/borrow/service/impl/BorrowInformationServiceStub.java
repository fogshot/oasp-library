/**
 * 
 */
package com.cg.sedp.library.branch.borrow.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cg.sedp.library.branch.borrow.common.BorrowedItemEto;
import com.cg.sedp.library.branch.borrow.service.api.BorrowInformationService;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.ccc.user.common.UserIdType;

/**
 * A Stub implementation for {@link BorrowInformationService}
 * 
 * @author chuber
 *
 */
//@Service
//@Qualifier(value = "Stub")
public class BorrowInformationServiceStub implements BorrowInformationService {

	@Override
	public List<MediaItemIdentifier> getBorrowedMediaIDs(UserIdType userId) {
		List<MediaItemIdentifier> mediaIdentifier = new ArrayList<>();
		mediaIdentifier.add(new MediaItemIdentifier("1"));
		return mediaIdentifier;
	}

	@Override
	public List<BorrowedItemEto> getAllBorrowedMediaIDs() {
		List<BorrowedItemEto> borrowedItemEto = new ArrayList<>();
		borrowedItemEto.add(new BorrowedItemEto());
		return borrowedItemEto;
	}

	@Override
	public boolean isBorrowed(MediaItemIdentifier mediaId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getNumberOfBorrowedMedia(UserIdType userId) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Date getNextExpirationDate(UserIdType userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserIdType getUserIdWhoBorrowed(MediaItemIdentifier mediaId) {
		// TODO Auto-generated method stub
		return null;
	}

}
