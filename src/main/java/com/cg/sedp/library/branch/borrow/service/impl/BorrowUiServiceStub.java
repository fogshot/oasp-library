package com.cg.sedp.library.branch.borrow.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.cg.sedp.library.branch.borrow.common.BorrowViewReservedItemCto;
import com.cg.sedp.library.branch.borrow.common.BorrowedItemCto;
import com.cg.sedp.library.branch.borrow.common.BorrowedItemEto;
import com.cg.sedp.library.branch.borrow.logic.api.BorrowedMedia;
import com.cg.sedp.library.branch.borrow.service.api.BorrowUiService;
import com.cg.sedp.library.branch.catalog.common.MediaItemEto;
import com.cg.sedp.library.branch.catalog.common.MediaNotFoundException;
import com.cg.sedp.library.branch.catalog.service.api.MediaService;
import com.cg.sedp.library.ccc.user.common.UserIdType;

//@Service
//@Qualifier(value="Stub")
public class BorrowUiServiceStub implements BorrowUiService {

	@Autowired
	private BorrowedMedia borrowedMedia;

	@Autowired
	private MediaService mediaService;

	@Override
	public List<BorrowedItemCto> getAllBorrowedMediaForUser(UserIdType userId) {

		List<BorrowedItemCto> borrowedItemCtos = new ArrayList<>();

		List<BorrowedItemEto> borrowedItemEtos = borrowedMedia.getBorrowedMediaIDs(userId);

		for (BorrowedItemEto borrowedItemEto : borrowedItemEtos) {
			try {		
				MediaItemEto mediaItemEto = mediaService.getMediaById(borrowedItemEto.getMediaItemIdentifier());
				borrowedItemCtos.add(new BorrowedItemCto(mediaItemEto, borrowedItemEto));
			} catch (MediaNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return borrowedItemCtos;
	}

	@Override
	public boolean extendBorrowedMedia(BorrowedItemCto borrowedItemCto) {

		return false;
	}

	@Override
	public List<BorrowViewReservedItemCto> getAllReservedMediaForUser(UserIdType userId) {
		// TODO Auto-generated method stub
		return null;
	}

}
