package com.cg.sedp.library.branch.catalog.common;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class MediaItemEto {

//	@NotNull
	private MediaItemType mediaItemType;

	private MediaItemIdentifier mediaItemIdentifier;
	
	@NotNull
	@Size(min=0, max=256)
	private String title;

	@NotNull
	@Size(min=0, max=256)
	private String author;
		
	public MediaItemType getMediaItemType() {
		return mediaItemType;
	}

	public void setMediaItemType(MediaItemType mediaItemType) {
		this.mediaItemType = mediaItemType;
	}

	public MediaItemIdentifier getMediaItemIdentifier() {
		return mediaItemIdentifier;
	}

	public void setMediaItemIdentifier(MediaItemIdentifier mediaItemIdentifier) {
		this.mediaItemIdentifier = mediaItemIdentifier;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public int hashCode() {
		return new HashCodeBuilder() //
				.append(getTitle()) //
				.append(getAuthor()) //
				.append(getMediaItemType()) //
				.append(getMediaItemIdentifier()) //
				.hashCode();
	}

	public boolean equals(Object obj) {
		MediaItemEto eto=(MediaItemEto)obj;
		
		return new EqualsBuilder() //
				// .appendSuper(super.equals(eto)) //
				.append(getTitle(), eto.getTitle()) //
				.append(getAuthor(), eto.getAuthor()) //
				.append(getMediaItemType(), eto.getMediaItemType()) //
				.append(getMediaItemIdentifier().getMediaItemIdentifier(), eto.getMediaItemIdentifier().getMediaItemIdentifier()) //
				.isEquals();
	}
	
	@Override
	public String toString() {
		return new ToStringBuilder(this) //
				.append("type", mediaItemType) //
				.append("title", title) //
				.append("author", author) //
				.append("miID", mediaItemIdentifier!=null ? mediaItemIdentifier.getMediaItemIdentifier() : mediaItemIdentifier) //
				.toString();
	}
}