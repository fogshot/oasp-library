package com.cg.sedp.library.branch.catalog.common;


/**
 * this class represents a business media item identifier.
 *
 * NOTE:
 * this is NOT the technical identifier (which depends on used database).
 *
 */
public class MediaItemIdentifier {

	//@NotNull
	private String mediaItemIdentifier;

	public MediaItemIdentifier() {
	}

	public MediaItemIdentifier(String mediaItemIdentifier) {
		super();
		this.mediaItemIdentifier = mediaItemIdentifier;
	}

	public String getMediaItemIdentifier() {
		return mediaItemIdentifier;
	}

	public void setMediaItemIdentifier(String mediaItemIdentifier) {
		this.mediaItemIdentifier = mediaItemIdentifier;
	}
	
	@Override
	public String toString() {
		return mediaItemIdentifier;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mediaItemIdentifier == null) ? 0 : mediaItemIdentifier.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MediaItemIdentifier other = (MediaItemIdentifier) obj;
		if (mediaItemIdentifier == null) {
			if (other.mediaItemIdentifier != null)
				return false;
		} else if (!mediaItemIdentifier.equals(other.mediaItemIdentifier))
			return false;
		return true;
	}
	
}
