package com.cg.sedp.library.branch.catalog.common;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class MediaItemIdentifierConverter implements AttributeConverter <MediaItemIdentifier, String> {

	@Override
	public String convertToDatabaseColumn(MediaItemIdentifier mediaId) {
		StringBuilder sb = new StringBuilder();
		sb.append(mediaId.getMediaItemIdentifier());
		return sb.toString();
	}

	@Override
	public MediaItemIdentifier convertToEntityAttribute(String mediaItemIdString) {
		// TODO Auto-generated method stub
		return new MediaItemIdentifier(mediaItemIdString);
	}

}
