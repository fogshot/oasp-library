package com.cg.sedp.library.branch.catalog.common;

import java.util.Locale;

import com.vaadin.data.util.converter.Converter;

public class MediaItemIdentifierVaadinConverter implements Converter<String, MediaItemIdentifier> {
	private static final long serialVersionUID = 1L;

	@Override
	public MediaItemIdentifier convertToModel(String value, Class<? extends MediaItemIdentifier> targetType,
			Locale locale) throws com.vaadin.data.util.converter.Converter.ConversionException {
		return value!=null ? new MediaItemIdentifier(value) : null;
	}

	@Override
	public String convertToPresentation(MediaItemIdentifier value, Class<? extends String> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		return value!=null ? value.getMediaItemIdentifier() : "";
	}

	@Override
	public Class<MediaItemIdentifier> getModelType() {
		return MediaItemIdentifier.class;
	}

	@Override
	public Class<String> getPresentationType() {
		return String.class;
	}


}
