package com.cg.sedp.library.branch.catalog.common;

public enum MediaItemType {

	Book, // printed books
	DVD, // video/DVD
	CD,	// audio/CD
	BD; // video/BluRay
	
}
