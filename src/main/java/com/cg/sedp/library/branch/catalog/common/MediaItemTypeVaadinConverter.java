package com.cg.sedp.library.branch.catalog.common;

import java.util.Locale;

import com.vaadin.data.util.converter.Converter;

public class MediaItemTypeVaadinConverter implements Converter<String, MediaItemType> {
	private static final long serialVersionUID = 1L;

	@Override
	public MediaItemType convertToModel(String value, Class<? extends MediaItemType> targetType,
			Locale locale) throws com.vaadin.data.util.converter.Converter.ConversionException {
		return Enum.valueOf(getModelType(), value);
	}

	@Override
	public String convertToPresentation(MediaItemType value, Class<? extends String> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		return value!=null ? value.toString() : null;
	}

	@Override
	public Class<MediaItemType> getModelType() {
		return MediaItemType.class;
	}

	@Override
	public Class<String> getPresentationType() {
		return String.class;
	}


}
