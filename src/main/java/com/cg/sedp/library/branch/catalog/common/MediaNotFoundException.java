package com.cg.sedp.library.branch.catalog.common;

public class MediaNotFoundException extends Exception

{
	 public MediaNotFoundException(String exceptionMessage)
	    {
	        // Call constructor of parent Exception
	        super(exceptionMessage);
	    }
}
