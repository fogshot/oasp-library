package com.cg.sedp.library.branch.catalog.common;

import java.io.Serializable;
import java.util.Set;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Access to media search parameters.
 *
 * @author akampa
 * @since dev
 */
public class SearchCriteria implements Serializable {

  /** Generated. */
  private static final long serialVersionUID = -6705545261389207095L;

  private Set<MediaItemType> selectedType;

  private String searchPattern;

  private String patternTitle;

  private String patternAuthor;

  public SearchCriteria() {

  }

  /**
   * Simple Constructor with searchpattern.
   *
   * @param searchPattern to search media for.
   */
  public SearchCriteria(String searchPattern) {

    setSearchPattern(searchPattern);

  }

  /**
   * @return selectedType
   */
  public Set<MediaItemType> getSelectedType() {

    return this.selectedType;
  }

  /**
   * @param selectedType the selectedType to set
   */
  public void setSelectedType(Set<MediaItemType> selectedType) {

    this.selectedType = selectedType;
  }

  /**
   * @return searchPattern
   */
  public String getSearchPattern() {

    return this.searchPattern;
  }

  /**
   * @param searchPattern new value .
   */
  public void setSearchPattern(String searchPattern) {

    this.searchPattern = searchPattern;
  }

  /**
   * @return patternTitle
   */
  public String getPatternTitle() {

    return this.patternTitle;
  }

  /**
   * @param patternTitle the patternTitle to set
   */
  public void setPatternTitle(String patternTitle) {

    this.patternTitle = patternTitle;
  }

  /**
   * @return patternAuthor
   */
  public String getPatternAuthor() {

    return this.patternAuthor;
  }

  /**
   * @param patternAuthor the patternAuthor to set
   */
  public void setPatternAuthor(String patternAuthor) {

    this.patternAuthor = patternAuthor;
  }

	@Override
	public String toString() {
		return new ToStringBuilder(this) //
				.append("searchPattern", searchPattern) //
				.append("patternTitle", patternTitle) //
				.append("patternAuthor", patternAuthor) //
				.append("selectedType", selectedType) //
				.toString();
	}

}
