package com.cg.sedp.library.branch.catalog.gui;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cg.sedp.library.branch.catalog.common.MediaItemEto;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifierVaadinConverter;
import com.cg.sedp.library.branch.catalog.common.MediaItemType;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

public class CatalogMgmtForm extends FormLayout {

	private static final long serialVersionUID = 1L;

	private final Logger logger = LoggerFactory.getLogger(CatalogMgmtForm.class);

	private CatalogMgmtView parent;

	private Button save = new Button("Save", this::save);
	private Button cancel = new Button("Cancel", this::cancel);
	private Button delete = new Button("Delete", this::delete);
	
	private TextField mediaItemIdentifier = new TextField("Media ID");
	private TextField title = new TextField("Title");
	private TextField author = new TextField("Author");
	private ComboBox mediaItemType=new ComboBox("Type");

	private MediaItemEto mediaEto;

	private BeanFieldGroup<MediaItemEto> formFieldBindings;

	public CatalogMgmtForm(CatalogMgmtView parent) {
		this.parent = parent;
		configureWidgets();
		buildLayout();
	}

	private void configureWidgets() {
		save.setStyleName(ValoTheme.BUTTON_PRIMARY);
		save.setClickShortcut(ShortcutAction.KeyCode.ENTER);
		setVisible(false);
		
		mediaItemIdentifier.setConverter(new MediaItemIdentifierVaadinConverter());
		title.setNullRepresentation("");
		author.setNullRepresentation("");
		mediaItemType.addItems(Arrays.asList(MediaItemType.values()));
	}

	private void buildLayout() {
		setSizeUndefined();
		setMargin(true);

		HorizontalLayout actions = new HorizontalLayout(save, delete, cancel);
		actions.setSpacing(true);

		addComponents(actions, mediaItemIdentifier, title, author, mediaItemType);
	}

	public void save(Button.ClickEvent event) {
		try {
			formFieldBindings.commit();
			
			
			
			parent.saveMediaItemEto(mediaEto);
			Notification.show(String.format("Saved '%s'", mediaEto.getMediaItemIdentifier().getMediaItemIdentifier()), Type.TRAY_NOTIFICATION);
			parent.doSearchByFilter();
			this.setVisible(false);
		} catch (FieldGroup.CommitException e) {
			Notification.show("Please fix validation errors first", Type.ERROR_MESSAGE);
			logger.error("error commiting " + mediaEto, e);
		}
	}

	public void delete(Button.ClickEvent event) {
		try {
			formFieldBindings.commit();
			parent.deleteMediaItemEto(mediaEto);
			Notification.show(String.format("Deleted '%s'", mediaEto.getMediaItemIdentifier()), Type.TRAY_NOTIFICATION);
			parent.doSearchByFilter();
			this.setVisible(false);
		} catch (FieldGroup.CommitException e) {
			Notification.show("Please fix validation errors first", Type.ERROR_MESSAGE);
			logger.error("error deleting " + mediaEto, e);
		}
	}

	public void cancel(Button.ClickEvent event) {
		Notification.show("Cancelled", Type.TRAY_NOTIFICATION);
		parent.cancelEdit();
	}

	public void edit(MediaItemEto mediaEto) {
		this.mediaEto = mediaEto;
		if (this.mediaEto != null) {
			formFieldBindings = BeanFieldGroup.bindFieldsBuffered(this.mediaEto, this);
			formFieldBindings.setEnabled(true);
			mediaItemIdentifier.focus();
		}
		setVisible(this.mediaEto != null);
	}

	public void editNewEto() {
		MediaItemEto eto=new MediaItemEto();
		
		edit(eto);
	}

}