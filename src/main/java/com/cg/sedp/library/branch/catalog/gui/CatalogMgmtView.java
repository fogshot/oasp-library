package com.cg.sedp.library.branch.catalog.gui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.cg.sedp.library.branch.catalog.common.MediaItemEto;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifierVaadinConverter;
import com.cg.sedp.library.branch.catalog.common.MediaItemType;
import com.cg.sedp.library.branch.catalog.common.MediaItemTypeVaadinConverter;
import com.cg.sedp.library.branch.catalog.common.MediaNotFoundException;
import com.cg.sedp.library.branch.catalog.common.SearchCriteria;
import com.cg.sedp.library.branch.catalog.service.api.MediaItemCrudService;
import com.cg.sedp.library.branch.catalog.service.api.MediaService;
import com.cg.sedp.library.branch.catalog.service.impl.MediaServiceImpl;
import com.cg.sedp.library.ccc.user.common.UserEto;
import com.cg.sedp.library.ccc.user.service.api.UserInformationService;
import com.google.common.base.Preconditions;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@ViewScope
@SpringView(name = CatalogMgmtView.VIEW_NAME)
public class CatalogMgmtView extends VerticalLayout implements View {
	
	@Autowired 
	private MediaService mediaService;
	@Autowired
	private MediaItemCrudService mediaItemCrudService;
	
	public static final String VIEW_NAME = "catalog-mgmt-view";

	private static final long serialVersionUID = 1L;

	private TextField titleFilter = new TextField("Title");
	private TextField authorFilter = new TextField("Author");
	private TextField identifierFilter = new TextField("Media ID");
	private ComboBox mediaItemTypeFilter = new ComboBox("Type");
	private Button doSearchButton = new Button("Search");
	private Button doResetFilter = new Button("Reset Filter");

	private Grid mediaItemList = new Grid();
	private Button newMediaItemButton = new Button("new MediaItem");

	private CatalogMgmtForm newEtoForm;

	@PostConstruct
	private void init() {
			newEtoForm = new CatalogMgmtForm(this);
			setMargin(true);
			setSpacing(true);
			configureWidgets();
			addComponent(buildLayout());
	
			// initial refresh data
			refreshMediaItemTypes();
			refreshMediaItemList();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// the view is constructed in init() method
	}

	public void cancelEdit() {
		mediaItemList.select(null);
		newEtoForm.setVisible(false);
	}

	private void configureWidgets() {
		newMediaItemButton.addClickListener(e -> newEtoForm.editNewEto());

		// search buttons
		doResetFilter.addClickListener(e -> doResetFilter());
		doSearchButton.addClickListener(e -> doSearchByFilter());
		// converter
		titleFilter.setNullRepresentation("");
		authorFilter.setNullRepresentation("");
		identifierFilter.setNullRepresentation("");

		// order
		BeanItemContainer<MediaItemEto> container = new BeanItemContainer<>(MediaItemEto.class);
		mediaItemList.setContainerDataSource(container);
		mediaItemList.setColumnOrder("mediaItemIdentifier", "title", "author", "mediaItemType");
		// columns
		mediaItemList.getColumn("mediaItemIdentifier").setHeaderCaption("Media ID.");
		mediaItemList.getColumn("mediaItemIdentifier").setConverter(new MediaItemIdentifierVaadinConverter());
		mediaItemList.getColumn("title").setHeaderCaption("Title");
		mediaItemList.getColumn("author").setHeaderCaption("Author");
		mediaItemList.getColumn("mediaItemType").setHeaderCaption("Type");
		mediaItemList.getColumn("mediaItemType").setConverter(new MediaItemTypeVaadinConverter());

		mediaItemList.setSelectionMode(Grid.SelectionMode.SINGLE);
		mediaItemList.addSelectionListener(e -> newEtoForm.edit((MediaItemEto) mediaItemList.getSelectedRow()));

	}

	private HorizontalLayout buildLayout() {

		HorizontalLayout l1 = new HorizontalLayout(identifierFilter, mediaItemTypeFilter);
		l1.setSpacing(true);
		HorizontalLayout l2 = new HorizontalLayout(titleFilter, authorFilter);
		l2.setSpacing(true);
		HorizontalLayout l3 = new HorizontalLayout(doResetFilter, doSearchButton);
		l3.setSpacing(true);
		FormLayout filterPanel = new FormLayout(//
				l1, l2, l3);
		filterPanel.setMargin(false);

		HorizontalLayout newButtonPanel = new HorizontalLayout();
		newButtonPanel.setSpacing(true);
		newButtonPanel.addComponent(newMediaItemButton);
		newButtonPanel.setWidth("100%");
		newButtonPanel.setComponentAlignment(newMediaItemButton, Alignment.TOP_RIGHT);

		VerticalLayout page = new VerticalLayout(filterPanel, newButtonPanel, mediaItemList);
		page.setSizeFull();
		mediaItemList.setSizeFull(); 
		page.setSpacing(true);
		page.setExpandRatio(mediaItemList, 1);

		HorizontalLayout mainLayout = new HorizontalLayout(page, newEtoForm);
		mainLayout.setSizeFull();
		mainLayout.setExpandRatio(page, 1);

		return mainLayout;
	}

	// private void refreshUsers(String userNamePrefixFilterString) {
	// usersList.setContainerDataSource(
	// new BeanItemContainer<>(UserEto.class,
	// userService.getAllUsers(userNamePrefixFilterString)));
	// newEtoForm.setVisible(false);
	// }

	// ####################################################
	// ##
	// ## media item types
	// ##

	public List<MediaItemType> getMediaItemTypes() {
		return Arrays.asList(MediaItemType.values());
	}

	public void refreshMediaItemTypes() {
		mediaItemTypeFilter.removeAllItems();
		mediaItemTypeFilter.addItems(getMediaItemTypes());
	}

	private void refreshMediaItemList() {
		doSearchAll();
	}

	// ####################################################
	// ##
	// ## filter
	// ##

	public void doResetFilter() {
		identifierFilter.clear();
		titleFilter.clear();
		authorFilter.clear();
		mediaItemTypeFilter.clear();
	}

	public void doSearchByFilter() {
		SearchCriteria filterSearchCriteria = new SearchCriteria();

		// get search filter
		filterSearchCriteria.setPatternTitle((String) titleFilter.getConvertedValue());
		filterSearchCriteria.setPatternAuthor((String) authorFilter.getConvertedValue());
		filterSearchCriteria.setSearchPattern((String) identifierFilter.getConvertedValue());

		MediaItemType selectedType = (MediaItemType) mediaItemTypeFilter.getValue();
		filterSearchCriteria
				.setSelectedType(selectedType != null ? Collections.singleton(selectedType) : Collections.emptySet());

		// do search
		System.out.println("216: sc=" + filterSearchCriteria);
		doSearchByFilter(filterSearchCriteria);
	}

	public void doSearchAll() {
		// do search with no filter
//		doSearchByFilter(null);
		List<MediaItemEto> filteredMedia = mediaService.getAllMedia();
		mediaItemList.setContainerDataSource(new BeanItemContainer<>(MediaItemEto.class, filteredMedia));
	}

	public void doSearchByFilter(SearchCriteria sc) {
		// do search
		List<MediaItemEto> filteredMedia = mediaService.getFilteredMedia(sc);
		Preconditions.checkNotNull(filteredMedia);
		
		// set model
		mediaItemList.setContainerDataSource(new BeanItemContainer<>(MediaItemEto.class, filteredMedia));
	}

	// ####################################################
	// ##
	// ## actions
	// ##

	public void saveMediaItemEto(MediaItemEto eto) {
		try {
			mediaService.getMediaById(eto.getMediaItemIdentifier());
			mediaItemCrudService.saveMedia(eto);
		} catch (MediaNotFoundException e) {
			addMediaItemEto(eto);
		}
	}

	public void deleteMediaItemEto(MediaItemEto eto) {
		try {
			mediaItemCrudService.deleteMedia(eto.getMediaItemIdentifier());
		} catch (MediaNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void addMediaItemEto(MediaItemEto eto) {
		mediaItemCrudService.addMedia(eto);
	}

}