package com.cg.sedp.library.branch.catalog.logic.api;

import java.util.List;

import com.cg.sedp.library.branch.catalog.common.MediaItemEto;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.catalog.common.MediaItemType;
import com.cg.sedp.library.branch.catalog.common.MediaNotFoundException;
import com.cg.sedp.library.branch.catalog.common.SearchCriteria;

public interface BrowseCatalogUC 
{
	MediaItemEto getMedia(MediaItemIdentifier mediaID ) throws MediaNotFoundException;
	List<MediaItemEto> getAllMedia();
	List<MediaItemEto> getFilterdMedia(SearchCriteria searchCriteria);
}
