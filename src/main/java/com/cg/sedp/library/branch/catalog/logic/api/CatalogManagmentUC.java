package com.cg.sedp.library.branch.catalog.logic.api;

import com.cg.sedp.library.branch.catalog.common.MediaItemEto;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.catalog.common.MediaNotFoundException;
import com.cg.sedp.library.ccc.user.common.UserEto;

public interface CatalogManagmentUC 
{
public void addMedia(UserEto user, MediaItemEto mediaItemEto);
public void saveMedia(UserEto user,MediaItemEto mediaItemEto) throws MediaNotFoundException;
public void deleteMedia(UserEto user,MediaItemIdentifier mediaIdentifier) throws MediaNotFoundException;
}
