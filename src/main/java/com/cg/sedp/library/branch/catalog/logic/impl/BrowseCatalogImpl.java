package com.cg.sedp.library.branch.catalog.logic.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cg.sedp.library.branch.catalog.common.MediaItemEto;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.catalog.common.MediaNotFoundException;
import com.cg.sedp.library.branch.catalog.common.SearchCriteria;
import com.cg.sedp.library.branch.catalog.logic.api.BrowseCatalogUC;
import com.cg.sedp.library.branch.catalog.persistence.api.MediaItemRepositoryDao;
import com.cg.sedp.library.branch.catalog.persistence.entity.MediaItem;


@Service
public class BrowseCatalogImpl  implements BrowseCatalogUC {

	@Autowired
	private MediaItemRepositoryDao rep;
	
	public MediaItemEto getMedia(MediaItemIdentifier objectMediaID)  throws MediaNotFoundException
	{
		//long longMediaId = longobjectMediaID.getMediaItemIdentifier();
		//rep.getOne(longMediaId);
		if (objectMediaID == null  )
		{
			throw new MediaNotFoundException("MediItemIdentifier is null." );
		}
		
	   MediaItem mediaItem= rep.findByMediaItemIdentifier(objectMediaID);
	   if (mediaItem != null)
	   {
		   return mapFromMediaItem(mediaItem);
	   }
	   else
	   {
		   throw new MediaNotFoundException("Media: " + objectMediaID.toString()   );
	   }
	   
	}

	@Override
	public List<MediaItemEto> getAllMedia() {
		// TODO Auto-generated method stub
		
		List<MediaItem> mediaItemList=  rep.findAll();
		return mediaItemList.stream().map(this::mapFromMediaItem).collect(Collectors.toList());
		
	}

	@Override
	public List<MediaItemEto> getFilterdMedia(SearchCriteria searchCriteria) {
		rep.findAllBySearchCriteria(searchCriteria);
		List<MediaItem> mediaItemList=  rep.findAllBySearchCriteria(searchCriteria);
		return mediaItemList.stream().map(this::mapFromMediaItem).collect(Collectors.toList());
	}

	private MediaItemEto mapFromMediaItem(MediaItem input) 
	
	{
		
		MediaItemEto mediaItemEt = new MediaItemEto();
		// do the mapping
		mediaItemEt.setMediaItemIdentifier(input.getMediaItemIdentifier());
		mediaItemEt.setAuthor(input.getAuthor());
		mediaItemEt.setTitle(input.getTitle());
		mediaItemEt.setMediaItemType(input.getMediaItemType());
		return mediaItemEt;
	}

}
