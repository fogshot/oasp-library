package com.cg.sedp.library.branch.catalog.logic.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cg.sedp.library.branch.catalog.common.MediaItemEto;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.catalog.common.MediaNotFoundException;
import com.cg.sedp.library.branch.catalog.common.SearchCriteria;
import com.cg.sedp.library.branch.catalog.logic.api.BrowseCatalogUC;
import com.cg.sedp.library.branch.catalog.logic.api.CatalogManagmentUC;
import com.cg.sedp.library.branch.catalog.persistence.api.MediaItemRepositoryDao;
import com.cg.sedp.library.branch.catalog.persistence.entity.MediaItem;
import com.cg.sedp.library.ccc.user.common.UserEto;

@Service
public class CatalogManagmentImpl implements CatalogManagmentUC 
{

	@Autowired
	private MediaItemRepositoryDao rep;

	@Override
	public void addMedia(UserEto user,MediaItemEto mediaItemEto) {
		
		if (user.getAdminUserRight() == true)
		{
		MediaItem mediaItem= new MediaItem();
		mediaItem.setAuthor(mediaItemEto.getAuthor());
		mediaItem.setTitle(mediaItemEto.getTitle());
		mediaItem.setMediaItemIdentifier(mediaItemEto.getMediaItemIdentifier());
		mediaItem.setMediaItemType(mediaItemEto.getMediaItemType());
		rep.save(mediaItem);
		}
		
	}

	@Override
	public void saveMedia(UserEto user,MediaItemEto mediaItemEto) throws MediaNotFoundException 
	{
		if (user.getAdminUserRight() == true)
		{
		MediaItem mediaItem= rep.findByMediaItemIdentifier(mediaItemEto.getMediaItemIdentifier());
		if( mediaItem != null )
		{
			mediaItem.setAuthor(mediaItemEto.getAuthor());
			mediaItem.setTitle(mediaItemEto.getTitle());
			mediaItem.setMediaItemIdentifier(mediaItemEto.getMediaItemIdentifier());
			mediaItem.setMediaItemType(mediaItemEto.getMediaItemType());
			rep.save(mediaItem);
		}
		else
	    {
			   throw new MediaNotFoundException("Media: " + mediaItemEto.toString() );
	    }
		}
	}

	@Override
	public void deleteMedia(UserEto user,MediaItemIdentifier mediaIdentifier) throws MediaNotFoundException
	{
		if (user.getAdminUserRight() == true)
		{
		MediaItem mediaItem= rep.findByMediaItemIdentifier(mediaIdentifier);
		if( mediaItem != null )
		{
			rep.delete(mediaItem);
		}
		else
		{
			   throw new MediaNotFoundException("MediaIdentifier not found : " +mediaIdentifier );
		}
		}
	}

}
