package com.cg.sedp.library.branch.catalog.persistence.api;

import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.catalog.persistence.entity.MediaItem;

public interface MediaItemRepositoryDao extends JpaRepository<MediaItem, Long>, MediaItemRepositoryDaoCustom {
	
	@NotNull
	MediaItem findByMediaItemIdentifier(@NotNull MediaItemIdentifier mediaItemIdentifier);
}
