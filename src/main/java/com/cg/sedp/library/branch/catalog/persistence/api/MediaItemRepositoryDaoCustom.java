package com.cg.sedp.library.branch.catalog.persistence.api;

import java.util.List;

import com.cg.sedp.library.branch.catalog.common.SearchCriteria;
import com.cg.sedp.library.branch.catalog.persistence.entity.MediaItem;

public interface MediaItemRepositoryDaoCustom {
	
	List<MediaItem> findAllBySearchCriteria(SearchCriteria searchCriteria);
}
