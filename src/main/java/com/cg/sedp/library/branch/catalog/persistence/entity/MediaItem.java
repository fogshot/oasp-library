package com.cg.sedp.library.branch.catalog.persistence.entity;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.catalog.common.MediaItemType;

@Entity
public class MediaItem {
	
	@Id
	@GeneratedValue
	private long id;
	
	@NotNull
	@Embedded
	private MediaItemIdentifier mediaItemIdentifier;

	public MediaItemIdentifier getMediaItemIdentifier() {
		return mediaItemIdentifier;
	}

	public void setMediaItemIdentifier(MediaItemIdentifier mediaItemIdentifier) {
		this.mediaItemIdentifier = mediaItemIdentifier;
	}

	@Version
	private long version;
	
	@NotNull
	private String title;

	@NotNull
	private String author;
	
	@NotNull
	private MediaItemType mediaItemType;

	public MediaItemType getMediaItemType() {
		return mediaItemType;
	}

	public void setMediaItemType(MediaItemType mediaItemType) {
		this.mediaItemType = mediaItemType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        MediaItem other = (MediaItem) obj;
        if (id != other.id) {
            return false;
        }
        if (version != other.version) {
            return false;
        }
        return true;
    }
    
	@Override
    public String toString() {
        return "MediaItem [mediaItemType=" + mediaItemType + ", version=" + version + ", title=" + title + ", author=" + author + "]";
    }
}
