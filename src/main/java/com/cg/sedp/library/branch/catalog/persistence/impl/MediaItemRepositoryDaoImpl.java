package com.cg.sedp.library.branch.catalog.persistence.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.cg.sedp.library.branch.catalog.common.MediaItemType;
import com.cg.sedp.library.branch.catalog.common.SearchCriteria;
import com.cg.sedp.library.branch.catalog.persistence.api.MediaItemRepositoryDaoCustom;
import com.cg.sedp.library.branch.catalog.persistence.entity.MediaItem;

public class MediaItemRepositoryDaoImpl implements MediaItemRepositoryDaoCustom {
	
    @PersistenceContext
    private EntityManager em;
    
	@SuppressWarnings("unchecked")
	@Override
	public List<MediaItem> findAllBySearchCriteria(SearchCriteria searchCriteria) {
		if(searchCriteria != null){
			return buildQuery(searchCriteria).getResultList();
		}
		return new ArrayList<MediaItem>();
	}
	
	protected Query buildQuery(SearchCriteria searchCriteria) {
		
		// Build query string with all necessary parameters, but at least id, title, and author.
		String queryString = buildQueryString(searchCriteria);
		Query query = em.createQuery(queryString);

		// Because id, title, and author are always in the query string, assign them a wild card if they are not specified as filter property.
		query.setParameter("idPlaceholder", searchCriteria.getSearchPattern() != null && !searchCriteria.getSearchPattern().trim().isEmpty() ? searchCriteria.getSearchPattern() : "%");
		query.setParameter("titlePlaceholder", searchCriteria.getPatternTitle() != null && !searchCriteria.getPatternTitle().trim().isEmpty() ? searchCriteria.getPatternTitle() : "%");
		query.setParameter("authorPlaceholder", searchCriteria.getPatternAuthor() != null && !searchCriteria.getPatternAuthor().trim().isEmpty() ? searchCriteria.getPatternAuthor() : "%");
		
		// MediaItemTypes are only parameterized as necessary.
		if (searchCriteria.getSelectedType() != null) {
			int i = 0;
			for (MediaItemType type : searchCriteria.getSelectedType()) {
				query.setParameter("type"+String.valueOf(i)+"Placeholder", type);
				i++;
			}
		}
		
		return query;
	}
	
	protected String buildQueryString(SearchCriteria searchCriteria) {
		StringBuilder b = new StringBuilder();
		b.append("SELECT m");
		b.append(" FROM MediaItem m");
		b.append(" WHERE");
		b.append(" m.mediaItemIdentifier.mediaItemIdentifier LIKE :idPlaceholder");
		b.append(" AND m.title LIKE :titlePlaceholder");
		b.append(" AND m.author LIKE :authorPlaceholder");
		
		if (searchCriteria.getSelectedType() == null || searchCriteria.getSelectedType().size() == 0)
			return b.toString();
		
		if (searchCriteria.getSelectedType().size() == 1)
			b.append(" AND m.mediaItemType = :type"+String.valueOf(0)+"Placeholder");
		else {
			b.append(" AND (");
			b.append(" m.mediaItemType = :type"+String.valueOf(0)+"Placeholder");
			for (int i = 1; i < searchCriteria.getSelectedType().size(); i++)
				b.append(" OR m.mediaItemType = :type"+String.valueOf(i)+"Placeholder");
			b.append(")");
		}
		return b.toString();
	}
}