package com.cg.sedp.library.branch.catalog.service.api;

import com.cg.sedp.library.branch.catalog.common.MediaItemEto;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.catalog.common.MediaNotFoundException;
import com.cg.sedp.library.ccc.user.common.UserEto;

/** 
 * @author Team3
 * Enables CUD functionality on the Catalog.
 */
public interface MediaItemCrudService {
	
	/**
	 * Adds a MediaItem to the catalog.
	 * @param mediaItemEto MediaItem to be added.
	 */
	public void addMedia(MediaItemEto mediaItemEto);
	
	/**
	 * Saves a media item  to the catalog.
	 * @param mediaItemEto
	 * @throws MediaNotFoundException
	 */
	public void saveMedia(MediaItemEto mediaItemEto) throws MediaNotFoundException;
	
	/**
	 * Deletes a media item to the catalog.
	 * @param mediaIdentifier
	 * @throws MediaNotFoundException
	 */
	public void deleteMedia(MediaItemIdentifier mediaIdentifier) throws MediaNotFoundException;
}
