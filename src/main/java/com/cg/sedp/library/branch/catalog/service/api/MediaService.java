package com.cg.sedp.library.branch.catalog.service.api;

import java.util.List;

import com.cg.sedp.library.branch.catalog.common.MediaItemEto;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.catalog.common.MediaNotFoundException;
import com.cg.sedp.library.branch.catalog.common.SearchCriteria;

/**
 * @author Team3
 * Read functionality on the Catalog.
 */
public interface MediaService {
	
	/**
	 * Returns a media item by id.
	 * @param id of the media item.
	 * @return media item.
	 * @throws MediaNotFoundException
	 */
	MediaItemEto getMediaById(MediaItemIdentifier id) throws MediaNotFoundException;
	
	/**
	 * Returns all media items in the catalog.
	 * @return list of media items.
	 */
	List<MediaItemEto> getAllMedia();
	
	/**
	 * Returns media items based on the search criteria.
	 * @param searchCriteria FilterCriteria
	 * @return filtered list of media items.
	 */
	List<MediaItemEto> getFilteredMedia(SearchCriteria searchCriteria);
}
