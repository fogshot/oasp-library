package com.cg.sedp.library.branch.catalog.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cg.sedp.library.branch.catalog.common.MediaItemEto;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.catalog.common.MediaNotFoundException;
import com.cg.sedp.library.branch.catalog.logic.api.CatalogManagmentUC;
import com.cg.sedp.library.branch.catalog.service.api.MediaItemCrudService;
import com.cg.sedp.library.ccc.user.common.UserEto;
import com.cg.sedp.library.ccc.user.common.UserException;
import com.cg.sedp.library.ccc.user.service.api.UserInformationService;

@Service
public class MediaItemCrudServiceImpl implements MediaItemCrudService {

	@Autowired
	private CatalogManagmentUC catalog;
	
	@Autowired
	private UserInformationService userInformationService;
	
	@Override
	public void addMedia(MediaItemEto mediaItemEto) {
		try {
			catalog.addMedia(userInformationService.getUser(), mediaItemEto);
		} catch (UserException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void saveMedia(MediaItemEto mediaItemEto) throws MediaNotFoundException {
		try {
			catalog.saveMedia(userInformationService.getUser(), mediaItemEto);
		} catch (UserException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteMedia(MediaItemIdentifier mediaIdentifier) throws MediaNotFoundException {
		try {
			catalog.deleteMedia(userInformationService.getUser(), mediaIdentifier);
		} catch (UserException e) {
			e.printStackTrace();
		}
	}
}
