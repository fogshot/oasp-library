package com.cg.sedp.library.branch.catalog.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.cg.sedp.library.branch.catalog.common.MediaItemEto;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.catalog.common.MediaNotFoundException;
import com.cg.sedp.library.branch.catalog.common.SearchCriteria;
import com.cg.sedp.library.branch.catalog.logic.api.BrowseCatalogUC;
import com.cg.sedp.library.branch.catalog.service.api.MediaService;
import com.cg.sedp.library.ccc.user.service.api.UserInformationService;

@Service
public class MediaServiceImpl implements MediaService {
	
	@Autowired
	private BrowseCatalogUC catalog;

	@Override
	public MediaItemEto getMediaById(MediaItemIdentifier id) throws MediaNotFoundException {
		return catalog.getMedia(id);
	}

	@Override
	public List<MediaItemEto> getAllMedia() {
		return catalog.getAllMedia();
	}

	@Override
	public List<MediaItemEto> getFilteredMedia(SearchCriteria searchCriteria) {
		return catalog.getFilterdMedia(searchCriteria);
	}
}
