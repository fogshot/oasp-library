package com.cg.sedp.library.branch.common;

import com.cg.sedp.library.ccc.user.common.UserIdType;

public class OwnedByDifferentUserException extends Exception {

	private static final long serialVersionUID = 1L;
	
	private final UserIdType ownedBy;

	public OwnedByDifferentUserException(UserIdType ownedBy) {
		this.ownedBy = ownedBy;
	}

	public UserIdType getOwnedBy() {
		return ownedBy;
	}
	
	@Override
	public String getMessage() {
		return ownedBy.getUserIdString();
	}
}
