package com.cg.sedp.library.branch.reservation.common;

import com.cg.sedp.library.ccc.user.common.UserIdType;

public class AlreadyReservedException extends Exception {

	private static final long serialVersionUID = 1L;

	private final UserIdType reservedFor;

	public AlreadyReservedException(UserIdType reservedFor) {
		this.reservedFor = reservedFor;
	}

	public UserIdType getReservedFor() {
		return reservedFor;
	}
	
	@Override
	public String getMessage() {
		return reservedFor.getUserIdString();
	}
}
