package com.cg.sedp.library.branch.reservation.common;

import com.cg.sedp.library.common.IdType;

public class ReservationIdType extends IdType<Long>{

	public ReservationIdType(Long id) {
		super(id);
	}

}
