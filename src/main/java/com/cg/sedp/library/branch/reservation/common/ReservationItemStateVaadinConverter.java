package com.cg.sedp.library.branch.reservation.common;

import java.util.Locale;

public class ReservationItemStateVaadinConverter implements com.vaadin.data.util.converter.Converter<String, ReservationItemState> {
	private static final long serialVersionUID = 1L;

	@Override
	public ReservationItemState convertToModel(String value, Class<? extends ReservationItemState> targetType,
			Locale locale) throws com.vaadin.data.util.converter.Converter.ConversionException {
		return Enum.valueOf(getModelType(), value);
	}

	@Override
	public String convertToPresentation(ReservationItemState value, Class<? extends String> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		return value!=null ? value.toString() : null;
	}

	@Override
	public Class<ReservationItemState> getModelType() {
		return ReservationItemState.class;
	}

	@Override
	public Class<String> getPresentationType() {
		return String.class;
	}


}
