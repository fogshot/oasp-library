package com.cg.sedp.library.branch.reservation.common;

import java.util.Set;
import java.util.TreeSet;

import com.cg.sedp.library.branch.borrow.common.BorrowedItemEto;
import com.cg.sedp.library.branch.catalog.common.MediaItemEto;

public class ReservedItemCto {

	private MediaItemEto mediaItemEto;
	private ReservedItemEto reservedItemEto;
	private BorrowedItemEto borrowedItemEto;
	
	public ReservedItemCto(MediaItemEto mediaItemEto, ReservedItemEto reservedItemEto, BorrowedItemEto borrowedItemEto) {
		super();
		this.setMediaItemEto(mediaItemEto);
		this.setReservedItemEto(reservedItemEto);
		this.setBorrowedItemEto(borrowedItemEto);
	}

	public MediaItemEto getMediaItemEto() {
		return mediaItemEto;
	}

	public void setMediaItemEto(MediaItemEto mediaItemEto) {
		this.mediaItemEto = mediaItemEto;
	}
	
	public boolean isReserved() {
		return getReservedItemEto()!=null;
	}
	
	public ReservedItemEto getReservedItemEto() {
		return reservedItemEto;
	}

	public void setReservedItemEto(ReservedItemEto borrowedItemEto) {
		this.reservedItemEto = borrowedItemEto;
	}

	public boolean isBorrowed() {
		return getBorrowedItemEto()!=null;
	}
	
	public BorrowedItemEto getBorrowedItemEto() {
		return borrowedItemEto;
	}

	public void setBorrowedItemEto(BorrowedItemEto borrowedItemEto) {
		this.borrowedItemEto = borrowedItemEto;
	}
	
	public Set<ReservationItemState> getState() {
		Set<ReservationItemState> state=new TreeSet<>();
		
		if (isBorrowed()) state.add(ReservationItemState.BORROWED);
		if (isReserved()) state.add(ReservationItemState.RESERVED);
		
		return state;
	}
}
