package com.cg.sedp.library.branch.reservation.common;

import java.sql.Date;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.ccc.user.common.UserIdType;

public class ReservedItemEto {

	private MediaItemIdentifier mediaItemIdentifier;
	private UserIdType user;
	private Date reservedUpTo;
	
	public ReservedItemEto() {
	}
	
	public ReservedItemEto(MediaItemIdentifier mediaItemIdentifier, Date reservedUpTo) {
		setMediaItemIdentifier(mediaItemIdentifier);
		setReservedUpTo(reservedUpTo);
	}

	public MediaItemIdentifier getMediaItemIdentifier() {
		return mediaItemIdentifier;
	}

	public void setMediaItemIdentifier(MediaItemIdentifier mediaItemIdentifier) {
		this.mediaItemIdentifier = mediaItemIdentifier;
	}

	public UserIdType getUser() {
		return user;
	}

	public void setUser(UserIdType user) {
		this.user = user;
	}

	public Date getReservedUpTo() {
		return reservedUpTo;
	}

	public void setReservedUpTo(Date reservedUpTo) {
		this.reservedUpTo = reservedUpTo;
	}
}
