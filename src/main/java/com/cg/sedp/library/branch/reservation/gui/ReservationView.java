package com.cg.sedp.library.branch.reservation.gui;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.cg.sedp.library.branch.borrow.common.BorrowedItemEto;
import com.cg.sedp.library.branch.borrow.service.api.BorrowExecutionService;
import com.cg.sedp.library.branch.borrow.service.api.BorrowInformationService;
import com.cg.sedp.library.branch.catalog.common.MediaItemEto;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifierVaadinConverter;
import com.cg.sedp.library.branch.catalog.common.MediaItemTypeVaadinConverter;
import com.cg.sedp.library.branch.catalog.common.MediaNotFoundException;
import com.cg.sedp.library.branch.catalog.service.impl.MediaServiceImpl;
import com.cg.sedp.library.branch.reservation.common.ReservedItemCto;
import com.cg.sedp.library.branch.reservation.common.ReservedItemEto;
import com.cg.sedp.library.branch.reservation.service.api.IReservationInformationService;
import com.cg.sedp.library.branch.reservation.service.api.IReservationUIService;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.ButtonRenderer;

@ViewScope
@SpringView(name = ReservationView.VIEW_NAME)
public class ReservationView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "reservation-view";
	
    private static final long serialVersionUID = 1L;
    

	private Grid gridReservedItems=new Grid();
	
	private Button doUnborrow = new Button("unborrow");
	private Button doUnreserve = new Button("unreserve");
	
	@Autowired
	private IReservationUIService reservationUIService;
	@Autowired
	private IReservationInformationService service;
	@Autowired
	private BorrowExecutionService borrowExecutionService;
	@Autowired
	private BorrowInformationService borrowInformationService;
	@Autowired
	private MediaServiceImpl mediaService;

	
    @PostConstruct
    private void init() {
        setMargin(true);
        setSpacing(true);
        configureWidgets();
		addComponent(buildLayout());
		
		// initial refresh data
		refreshItems();
    }

    @Override
    public void enter(ViewChangeEvent event) {
        // the view is constructed in init() method
    }
    
	
	private void configureWidgets() {
		
		
		// order
		BeanItemContainer<ReservedItemCto> containerBorrowedItems = newBeanItemContainer(ReservedItemCto.class, Collections.emptyList());
		gridReservedItems.setContainerDataSource(containerBorrowedItems);
		gridReservedItems.setColumnOrder( //
				"mediaItemEto.mediaItemIdentifier", //
				"mediaItemEto.title", //
				"mediaItemEto.author", //
				"mediaItemEto.mediaItemType", //
				
				"state", //

				"borrowed", //
				"reserved" //
				);
		// do not display direct member/properties
		gridReservedItems.removeColumn("mediaItemEto");
		gridReservedItems.removeColumn("borrowedItemEto");
		gridReservedItems.removeColumn("reservedItemEto");
		
		// columns
		gridReservedItems.getColumn("mediaItemEto.mediaItemIdentifier").setHeaderCaption("Media ID.");
		gridReservedItems.getColumn("mediaItemEto.mediaItemIdentifier").setConverter(new MediaItemIdentifierVaadinConverter());
		gridReservedItems.getColumn("mediaItemEto.title").setHeaderCaption("Title");
		gridReservedItems.getColumn("mediaItemEto.author").setHeaderCaption("Author");
		gridReservedItems.getColumn("mediaItemEto.mediaItemType").setHeaderCaption("Type");
		gridReservedItems.getColumn("mediaItemEto.mediaItemType").setConverter(new MediaItemTypeVaadinConverter());
		
		gridReservedItems.getColumn("state").setHeaderCaption("State");


		// TODO: remove hack using borrowed/reserved to display button for extension
		gridReservedItems.getColumn("borrowed").setRenderer(new ButtonRenderer(e -> doUnborrow((ReservedItemCto)e.getItemId()),"unborrow"));
		gridReservedItems.getColumn("borrowed").setHeaderCaption("unborrow");
		gridReservedItems.getColumn("reserved").setRenderer(new ButtonRenderer(e -> doUnreserve((ReservedItemCto)e.getItemId()),"unreserve"));
		gridReservedItems.getColumn("reserved").setHeaderCaption("unreserve");
		
		gridReservedItems.setSelectionMode(Grid.SelectionMode.SINGLE);
		
		doUnborrow.addClickListener(e -> doUnborrow((ReservedItemCto) gridReservedItems.getSelectedRow()));
		doUnreserve.addClickListener(e -> doUnreserve((ReservedItemCto) gridReservedItems.getSelectedRow()));
	}    
    
    private HorizontalLayout buildLayout() {
		
		HorizontalLayout newButtonPanel=new HorizontalLayout();
		//newButtonPanel.addComponent(doUnborrow);
		//newButtonPanel.addComponent(doUnreserve);
//		newButtonPanel.setWidth("100%");
		newButtonPanel.setDefaultComponentAlignment(ALIGNMENT_DEFAULT);

		VerticalLayout filterAndNewButtonAndList = new VerticalLayout(newButtonPanel, gridReservedItems);
		filterAndNewButtonAndList.setComponentAlignment(newButtonPanel, Alignment.MIDDLE_RIGHT);
		filterAndNewButtonAndList.setSizeFull();
		gridReservedItems.setSizeFull();
		filterAndNewButtonAndList.setExpandRatio(gridReservedItems,1);

		HorizontalLayout mainLayout = new HorizontalLayout(filterAndNewButtonAndList);
		mainLayout.setSizeFull();
		mainLayout.setExpandRatio(filterAndNewButtonAndList, 1);

		return mainLayout;
    }    
    	    
	// ####################################################
	// ##
	// ##  refresh
	// ##
	
    private <BeanType> BeanItemContainer<BeanType> newBeanItemContainer(Class<BeanType> clazz, Collection<BeanType> col) {
		BeanItemContainer<BeanType> container = new BeanItemContainer<>(clazz, col);
		
		container.addNestedContainerProperty("mediaItemEto.mediaItemIdentifier");
		container.addNestedContainerProperty("mediaItemEto.title");
		container.addNestedContainerProperty("mediaItemEto.author");
		container.addNestedContainerProperty("mediaItemEto.mediaItemType");
		
		container.addNestedContainerProperty("state");
		
		container.addNestedContainerProperty("borrowed");
		container.addNestedContainerProperty("reserved");
		
		return container;
    }

	private void refreshItems() {
		// do search
		List<ReservedItemCto> all = new ArrayList<>(); // TODO: connect to service and fetch data
		List<ReservedItemEto> allReservations = service.getAllReservations();

		allReservations.stream().forEach(reservedItem -> all.add(createReservedItemCto(reservedItem)));
		// set model
		gridReservedItems.setContainerDataSource(newBeanItemContainer(ReservedItemCto.class, all));
	}
	
	// ####################################################
	// ##
	// ## actions
	// ##
	
	private ReservedItemCto createReservedItemCto(ReservedItemEto eto) {
		MediaItemEto mediaItemEto;
		
		try {
			mediaItemEto = mediaService.getMediaById(eto.getMediaItemIdentifier());
			
			//TODO genau wie oben
			BorrowedItemEto borrowedItemEto = new BorrowedItemEto();
//		borrowedItemEto.setMediaItemIdentifier(mediaItemIdentifier);
			
			ReservedItemCto cto = new ReservedItemCto(mediaItemEto, eto, borrowedItemEto);
			return cto;
			
		} catch (MediaNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		
	}

	public void doUnborrow(ReservedItemCto item) {
		// TODO: connect to service and process item
		//borrowExecutionService.unborrow(item.getBorrowedItemEto().getMediaItemIdentifier(),item.get);
		refreshItems();
	}
	
	public void doUnreserve(ReservedItemCto item) {
		// TODO: connect to service and process item
		reservationUIService.deleteReservation(item.getMediaItemEto().getMediaItemIdentifier());
		refreshItems();
	}
	
		
}