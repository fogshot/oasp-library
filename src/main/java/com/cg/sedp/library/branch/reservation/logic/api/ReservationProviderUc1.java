package com.cg.sedp.library.branch.reservation.logic.api;

import java.util.List;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.reservation.common.ReservedItemEto;
import com.cg.sedp.library.ccc.user.common.UserIdType;

public interface ReservationProviderUc1 {

	List<ReservedItemEto> getReservations(UserIdType userId);
	List<ReservedItemEto> getAllReservations();
	ReservedItemEto getReservationEto(MediaItemIdentifier mediaId);  
	boolean isReserved(MediaItemIdentifier mediaId);
	boolean isUserReserved(UserIdType userId);
}
