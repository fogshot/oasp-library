package com.cg.sedp.library.branch.reservation.logic.api;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;

public interface ReservationProviderUc2 {
	
	public boolean deleteReservation(MediaItemIdentifier mediaId);
	
	
	
}
