package com.cg.sedp.library.branch.reservation.logic.api;


import java.sql.Date;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.ccc.user.common.UserIdType;

public interface ReservationProviderUc3 {

	boolean setReservation(UserIdType userId, MediaItemIdentifier mediaId, Date reservedUpTo);
}
