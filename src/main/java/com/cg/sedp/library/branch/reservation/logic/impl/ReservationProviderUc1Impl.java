package com.cg.sedp.library.branch.reservation.logic.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.reservation.common.ReservedItemEto;
import com.cg.sedp.library.branch.reservation.logic.api.ReservationProviderUc1;
import com.cg.sedp.library.branch.reservation.persistence.api.ReservationDao;
import com.cg.sedp.library.branch.reservation.persistence.entity.Reservation;
import com.cg.sedp.library.ccc.user.common.UserIdType;

@Service
public class ReservationProviderUc1Impl implements ReservationProviderUc1 {
	
	@Autowired
	ReservationDao reservationDao;

	@Override
	public List<ReservedItemEto> getReservations(UserIdType userId) {
		List<Reservation> allreservations = reservationDao.findAll();
		return allreservations.stream().map(this::mapReservation).collect(Collectors.toList());
	}

	@Override
	public List<ReservedItemEto> getAllReservations() {
		
		List<Reservation> allreservations = reservationDao.findAll();
		return allreservations.stream().map(this::mapReservation).collect(Collectors.toList());
	}
	

	@Override
	public ReservedItemEto getReservationEto(MediaItemIdentifier mediaId) {
		return createReservationEto(null, mediaId);
	}

	@Override
	public boolean isReserved(MediaItemIdentifier mediaId) {
		ReservedItemEto reservedItemEto = getReservationEto(mediaId);
		if (reservedItemEto == null) {
			return false;			
		}
		return true;
	}
	
	@Override
	public boolean isUserReserved(UserIdType userId) {
		return false;
	}
	
	private ReservedItemEto createReservationEto(UserIdType userId, MediaItemIdentifier mediaId) {
		ReservedItemEto reservedItemEto = new ReservedItemEto();
		if (userId != null && userId.getUserIdString() != null) {
			reservedItemEto.setUser(userId);
		} else {
			reservedItemEto.setUser(new UserIdType("Chuck"));
		}
		if (mediaId != null && mediaId.getMediaItemIdentifier() != null) {
			reservedItemEto.setMediaItemIdentifier(mediaId);
		} else {
			reservedItemEto.setMediaItemIdentifier(new MediaItemIdentifier("dumpMedia"));			
		}
		return reservedItemEto;
	}

	
	ReservedItemEto mapReservation(Reservation reservation) {
		ReservedItemEto eto = new ReservedItemEto();
		eto.setMediaItemIdentifier(reservation.getMediaId());
		eto.setReservedUpTo(reservation.getReservedUpTo());
		eto.setUser(reservation.getUserId());
		
		return eto;
	}

}
