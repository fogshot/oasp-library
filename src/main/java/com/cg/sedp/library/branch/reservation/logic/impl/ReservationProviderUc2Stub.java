package com.cg.sedp.library.branch.reservation.logic.impl;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Service;


import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.reservation.logic.api.ReservationProviderUc2;

@Service
public class ReservationProviderUc2Stub implements ReservationProviderUc2 {

	@Override
	public boolean deleteReservation(@NotNull MediaItemIdentifier mediaId) {
		System.out.println(mediaId.getMediaItemIdentifier());
		return false;
	}


}
