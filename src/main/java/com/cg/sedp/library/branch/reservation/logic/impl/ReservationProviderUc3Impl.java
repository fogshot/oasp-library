package com.cg.sedp.library.branch.reservation.logic.impl;


import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.reservation.logic.api.ReservationProviderUc3;
import com.cg.sedp.library.branch.reservation.persistence.api.ReservationDao;
import com.cg.sedp.library.branch.reservation.persistence.entity.Reservation;
import com.cg.sedp.library.ccc.user.common.UserIdType;

@Service
public class ReservationProviderUc3Impl implements ReservationProviderUc3 {
	
	@Autowired
	private ReservationDao dao;
		
	public boolean setReservation(UserIdType userId, MediaItemIdentifier mediaId, Date reservedUpTo) {
		
		
		try {
			Reservation reservationEntry=new Reservation();
			reservationEntry.setUserId(userId);
			reservationEntry.setMediaId(mediaId);
			reservationEntry.setReservedUpTo(reservedUpTo);
			dao.save(reservationEntry);
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

}
