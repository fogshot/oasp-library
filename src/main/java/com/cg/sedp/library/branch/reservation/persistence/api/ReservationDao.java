package com.cg.sedp.library.branch.reservation.persistence.api;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.reservation.persistence.entity.Reservation;
import com.cg.sedp.library.ccc.user.common.UserIdType;

import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ReservationDao extends JpaRepository<Reservation, Long> {
	
	@NotNull
    Reservation findByUserId(@NotNull UserIdType userId);

	@NotNull
    Reservation findByMediaId(@NotNull MediaItemIdentifier mediaItem);
}