package com.cg.sedp.library.branch.reservation.persistence.entity;

import java.sql.Date;

import javax.persistence.Convert;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.ccc.user.common.UserIdType;


@Entity
public class Reservation {

	@Id
	@GeneratedValue
	private long id;

	@Version
	private long version;

	@NotNull
	@Convert(converter = UserIdTypeConverter.class)
	private UserIdType userId;

	//	@ManyToOne(fetch = FetchType.LAZY)
	//	@JoinColumn(name = "media_id"))
	@Embedded
	private MediaItemIdentifier mediaId;

	public Date reservedUpTo;

	public Date getReservedUpTo() {
		return reservedUpTo;
	}

	public void setReservedUpTo(Date reservedUpTo) {
		this.reservedUpTo = reservedUpTo;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public UserIdType getUserId() {
		return userId;
	}

	public void setUserId(UserIdType userId) {
		this.userId = userId;
	}

	public MediaItemIdentifier getMediaId() {
		return mediaId;
	}

	public void setMediaId(MediaItemIdentifier mediaId) {
		this.mediaId = mediaId;
	}

}
