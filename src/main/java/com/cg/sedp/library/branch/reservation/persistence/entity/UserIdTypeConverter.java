package com.cg.sedp.library.branch.reservation.persistence.entity;

import javax.persistence.AttributeConverter;

import com.cg.sedp.library.ccc.user.common.UserIdType;

public class UserIdTypeConverter implements AttributeConverter<UserIdType, String> {

	@Override
	public String convertToDatabaseColumn(UserIdType userId) {
		return userId.getUserIdString();
	}

	@Override
	public UserIdType convertToEntityAttribute(String userId) {
		return new UserIdType(userId);
	}

}
