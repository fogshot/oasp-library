package com.cg.sedp.library.branch.reservation.service.api;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.reservation.common.*;
import com.cg.sedp.library.ccc.user.common.UserIdType;

import java.util.List;

public interface IReservationInformationService {
	List<ReservedItemEto> getReservations (UserIdType userID);
	ReservedItemEto getReservationEto (MediaItemIdentifier itemID);
	List<ReservedItemEto> getAllReservations ();
	boolean isReserved(MediaItemIdentifier itemID);
	boolean isUserReserved(UserIdType userID);
}
