package com.cg.sedp.library.branch.reservation.service.api;
import java.sql.Date;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.ccc.user.common.UserIdType;

public interface IReservationService {
	void setReservation (UserIdType userID, MediaItemIdentifier itemID, Date ReservedUpTo);
}
