package com.cg.sedp.library.branch.reservation.service.api;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;

public interface IReservationUIService {
	boolean deleteReservation (MediaItemIdentifier itemID);
}
