package com.cg.sedp.library.branch.reservation.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.reservation.common.ReservedItemEto;
import com.cg.sedp.library.branch.reservation.logic.api.ReservationProviderUc1;
import com.cg.sedp.library.branch.reservation.service.api.IReservationInformationService;
import com.cg.sedp.library.ccc.user.common.UserIdType;

@Service
public class ReservationInformationServiceImpl implements IReservationInformationService {

	@Autowired
	ReservationProviderUc1 reservationProviderUc1;

	@Override
	public List<ReservedItemEto> getReservations(UserIdType userID) {
		return reservationProviderUc1.getReservations(userID);
	}

	@Override
	public ReservedItemEto getReservationEto(MediaItemIdentifier itemID) {
		return reservationProviderUc1.getReservationEto(itemID);
	}

	@Override
	public List<ReservedItemEto> getAllReservations() {
		return reservationProviderUc1.getAllReservations();
	}

	@Override
	public boolean isReserved(MediaItemIdentifier itemID) {
		return reservationProviderUc1.isReserved(itemID);
	}

	@Override
	public boolean isUserReserved(UserIdType userID) {
		return reservationProviderUc1.isUserReserved(userID);
	}
}
