package com.cg.sedp.library.branch.reservation.service.impl;

import java.util.List;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.reservation.common.ReservedItemEto;
import com.cg.sedp.library.branch.reservation.service.api.IReservationInformationService;
import com.cg.sedp.library.ccc.user.common.UserIdType;
import com.google.gwt.thirdparty.guava.common.collect.Lists;

public class ReservationInformationServiceStub implements IReservationInformationService {

	@Override
	public List<ReservedItemEto> getReservations(UserIdType userID) {
		ReservedItemEto eto = new ReservedItemEto();
		return Lists.newArrayList(eto);
	}

	@Override
	public ReservedItemEto getReservationEto(MediaItemIdentifier itemID) {
		ReservedItemEto eto = new ReservedItemEto();
		return eto;
	}

	@Override
	public List<ReservedItemEto> getAllReservations() {
		ReservedItemEto eto = new ReservedItemEto();
		return Lists.newArrayList(eto);
	}
	
	@Override
	public boolean isReserved(MediaItemIdentifier itemID) {
		return false;
	}

	@Override
	public boolean isUserReserved(UserIdType userID) {
		// TODO Auto-generated method stub
		return false;
	}

}
