package com.cg.sedp.library.branch.reservation.service.impl;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.reservation.logic.api.ReservationProviderUc3;
import com.cg.sedp.library.ccc.user.common.UserIdType;

@Service
public class ReservationServiceImpl implements com.cg.sedp.library.branch.reservation.service.api.IReservationService {

	@Autowired
	ReservationProviderUc3 reservationProviderUc3;
	@Override
	public void setReservation(UserIdType userID, MediaItemIdentifier itemID, Date reservedUpTo) {
		reservationProviderUc3.setReservation(userID, itemID, reservedUpTo);
	}
}
