package com.cg.sedp.library.branch.reservation.service.impl;

import java.sql.Date;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.reservation.service.api.IReservationService;
import com.cg.sedp.library.ccc.user.common.UserIdType;


public class ReservationServiceStub implements IReservationService {
	@Override
	public void setReservation(UserIdType userID,MediaItemIdentifier itemID, Date ReservedUpTo) {
		//no reservations
	}

}
