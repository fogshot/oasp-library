package com.cg.sedp.library.branch.reservation.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.reservation.logic.api.ReservationProviderUc2;
import com.cg.sedp.library.branch.reservation.service.api.IReservationUIService;

@Service
public class ReservationUIServiceImpl implements IReservationUIService {
	
	@Autowired
	ReservationProviderUc2 reservationProviderUc2;

	@Override
	public boolean deleteReservation(MediaItemIdentifier itemID) {
		return reservationProviderUc2.deleteReservation(itemID);
	}
}
