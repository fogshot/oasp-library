package com.cg.sedp.library.ccc.monitoring.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cg.sedp.library.ccc.monitoring.service.api.LogType;

/**
 * Entity transport object for monitoring entries to be displayed in monitoring
 * dialog.
 */
public class MonitoringEntryEto {

	public static final int MAX_CONTENT_LENGTH = 10 * 1024;

	private static final DateFormat TIMESTAMP_DATE_FORMAT = SimpleDateFormat.getDateInstance();

	@NotNull
	@Size(min = 1, max = 8)
	private String userIdString;

	@NotNull
	private LogType logType;

	@NotNull
	@Size(min = 1, max = MAX_CONTENT_LENGTH)
	private String content;

	@NotNull
	private String timestamp;

	/**
	 * Empty constructor.
	 */
	public MonitoringEntryEto() {
	}

	public String getUserIdString() {
		return userIdString;
	}

	public void setUserIdString(String userIdString) {
		this.userIdString = userIdString;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = TIMESTAMP_DATE_FORMAT.format(timestamp);
	}

	public LogType getLogType() {
		return logType;
	}

	public void setLogType(LogType logType) {
		this.logType = logType;
	}

	@Override
	public String toString() {
		return "MonitoringEntryEto [userIdString=" + userIdString + ", logType=" + logType + ", content=" + content
				+ ", timestamp=" + timestamp + "]";
	}

}