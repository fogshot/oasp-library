package com.cg.sedp.library.ccc.monitoring.common;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cg.sedp.library.ccc.monitoring.service.api.LogType;
import com.cg.sedp.library.ccc.user.common.UserIdType;

/**
 * Transport object for filter settings of monitoring entries.
 */
public class MonitoringFilterTo {

	private LogType logType;

	@Size(min = 1, max = MonitoringEntryEto.MAX_CONTENT_LENGTH)
	private String contentPrefix;

	@Size(min = 1, max = UserIdType.MAX_USER_ID_LENGTH)
	private String userIdPrefix;

	@NotNull
	private Date timestampFromInclusive;

	@NotNull
	private Date timestampToInclusive;

	/**
	 * Empty constructor.
	 */
	public MonitoringFilterTo() {
	}

	public LogType getLogType() {
		return logType;
	}

	public void setLogType(LogType logType) {
		this.logType = logType;
	}

	public String getContentPrefix() {
		return contentPrefix;
	}

	public void setContentPrefix(String contentPrefix) {
		this.contentPrefix = contentPrefix;
	}

	public String getUserIdPrefix() {
		return userIdPrefix;
	}

	public void setUserIdPrefix(String userIdPrefix) {
		this.userIdPrefix = userIdPrefix;
	}

	public Date getTimestampFromInclusive() {
		return timestampFromInclusive;
	}

	public void setTimestampFromInclusive(Date timestampFromInclusive) {
		this.timestampFromInclusive = timestampFromInclusive;
	}

	public Date getTimestampToInclusive() {
		return timestampToInclusive;
	}

	public void setTimestampToInclusive(Date timestampToInclusive) {
		this.timestampToInclusive = timestampToInclusive;
	}

	public boolean hasFilterRestrictions() {
		return contentFilterSet() || userIdFilterSet() || logTypeFilterSet() || timestampFromInclusiveFilterSet()
				|| timestampToInclusiveFilterSet();
	}

	public boolean contentFilterSet() {
		return getContentPrefix() != null && getContentPrefix().length() > 0;
	}

	public boolean userIdFilterSet() {
		return getUserIdPrefix() != null && getUserIdPrefix().length() > 0;
	}

	public boolean logTypeFilterSet() {
		return getLogType() != null;
	}

	public boolean timestampToInclusiveFilterSet() {
		return getTimestampToInclusive() != null;
	}

	public boolean timestampFromInclusiveFilterSet() {
		return getTimestampFromInclusive() != null;
	}

	@Override
	public String toString() {
		return "MonitoringFilterTo [logType=" + logType + ", contentPrefix=" + contentPrefix + ", userIdPrefix="
				+ userIdPrefix + ", timestampFromInclusive=" + timestampFromInclusive + ", timestampToInclusive="
				+ timestampToInclusive + "]";
	}

}