package com.cg.sedp.library.ccc.monitoring.gui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import com.cg.sedp.library.ccc.monitoring.common.MonitoringEntryEto;
import com.cg.sedp.library.ccc.monitoring.common.MonitoringFilterTo;
import com.cg.sedp.library.ccc.monitoring.service.api.LogType;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

/**
 * View to display monitoring information.
 */
@ViewScope
@SpringView(name = MonitoringView.VIEW_NAME)
public class MonitoringView extends VerticalLayout implements View {

	private static final long serialVersionUID = 1L;

	public static final String VIEW_NAME = "monitoring-view";

	private TextField logContentFilter = new TextField();
	private ComboBox logTypeFilter = new ComboBox();
	private DateField dateFromFilter = new DateField();
	private DateField dateToFilter = new DateField();
	private TextField userFilter = new TextField();
	private Grid logList = new Grid();
	private Button reload = new Button("Reload");
	private Button clear = new Button("Clear All Filters");

	@PostConstruct
	private void init() {
		setMargin(true);
		setSpacing(true);

		configureWidgets();
		addComponent(buildLayout());

		// initially refresh logs to display current state
		refreshLogs();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// the view is constructed in init() method
	}

	private void refreshLogs() {
		MonitoringFilterTo filterTo = getFilterSettings();
		filterTo.setContentPrefix(logContentFilter.getValue());
		// TODO go to service
		List<MonitoringEntryEto> logs = new ArrayList<>();
		MonitoringEntryEto monitoringEntry = new MonitoringEntryEto();
		logs.add(monitoringEntry);
		monitoringEntry.setContent("content");
		monitoringEntry.setLogType(LogType.INFO);
		monitoringEntry.setUserIdString("su");
		monitoringEntry.setTimestamp(new Date());
		logList.setContainerDataSource(new BeanItemContainer<>(MonitoringEntryEto.class, logs));
	}

	private void clearAllFilters() {
		logContentFilter.setValue("");
		userFilter.setValue("");
		dateFromFilter.setValue(null);
		dateToFilter.setValue(null);
		logTypeFilter.setValue(null);
	}

	private void configureWidgets() {
		dateFromFilter.setResolution(Resolution.MINUTE);
		dateToFilter.setResolution(Resolution.MINUTE);
		userFilter.setInputPrompt("Filter User ...");
		logContentFilter.setInputPrompt("Filter Content ...");

		for (LogType type : LogType.values())
			logTypeFilter.addItem(type);

		logList.setContainerDataSource(new BeanItemContainer<>(MonitoringEntryEto.class));
		logList.setColumnOrder("timestamp", "logType", "userIdString", "content");
		logList.getColumn("timestamp").setHeaderCaption("Timestamp");
		logList.getColumn("userIdString").setHeaderCaption("User");
		logList.getColumn("logType").setHeaderCaption("Type");
		logList.getColumn("content").setHeaderCaption("Content");
		logList.setSelectionMode(Grid.SelectionMode.NONE);

		reload.addClickListener(c -> refreshLogs());

		clear.addClickListener(c -> clearAllFilters());
	}

	private VerticalLayout buildLayout() {
		// 1st Filter Row: from / to / type
		Label dateFromLabel;
		Label dateToLabel;
		Label logTypeLabel;
		HorizontalLayout timestampTypeFilterRow = new HorizontalLayout(//
				dateFromLabel = new Label("Timestamp From:"), //
				dateFromFilter, //
				dateToLabel = new Label("Timestamp To:"), //
				dateToFilter, //
				logTypeLabel = new Label("Type:"), //
				logTypeFilter);
		timestampTypeFilterRow.setSpacing(true);
		timestampTypeFilterRow.setWidth("100%");
		timestampTypeFilterRow.setExpandRatio(dateFromLabel, 1);
		timestampTypeFilterRow.setExpandRatio(dateFromFilter, 5);
		timestampTypeFilterRow.setExpandRatio(dateToLabel, 1);
		timestampTypeFilterRow.setExpandRatio(dateToFilter, 5);
		timestampTypeFilterRow.setExpandRatio(logTypeLabel, 1);
		timestampTypeFilterRow.setExpandRatio(logTypeFilter, 5);
		dateFromFilter.setSizeFull();
		dateToFilter.setSizeFull();
		logTypeFilter.setSizeFull();

		// 2nd Filter Row: user / content
		Label userLabel;
		Label contentLabel;
		HorizontalLayout userContentFilterRow = new HorizontalLayout(//
				userLabel = new Label("User:"), //
				userFilter, //
				contentLabel = new Label("Content:"), //
				logContentFilter);
		userContentFilterRow.setSpacing(true);
		userContentFilterRow.setWidth("100%");
		userContentFilterRow.setExpandRatio(userLabel, 1);
		userContentFilterRow.setExpandRatio(userFilter, 5);
		userContentFilterRow.setExpandRatio(contentLabel, 1);
		userContentFilterRow.setExpandRatio(logContentFilter, 5);
		userFilter.setSizeFull();
		logContentFilter.setSizeFull();

		// combining filter
		VerticalLayout filter = new VerticalLayout(timestampTypeFilterRow, userContentFilterRow);
		filter.setSpacing(true);
		filter.setWidth("100%");

		// Action Row: reload / clear
		HorizontalLayout actions = new HorizontalLayout(reload, clear);
		actions.setSpacing(true);
		actions.setWidth("100%");
		actions.setExpandRatio(reload, 1);
		actions.setExpandRatio(clear, 1);
		reload.setSizeFull();
		clear.setSizeFull();

		// combining filter + action + table
		VerticalLayout filterAndActionsAndList = new VerticalLayout(filter, actions, logList);
		filterAndActionsAndList.setSpacing(true);
		filterAndActionsAndList.setSizeFull();
		logList.setSizeFull();
		filterAndActionsAndList.setExpandRatio(logList, 1);

		return filterAndActionsAndList;
	}

	private MonitoringFilterTo getFilterSettings() {
		MonitoringFilterTo filterTo = new MonitoringFilterTo();
		filterTo.setContentPrefix(logContentFilter.getValue());
		filterTo.setUserIdPrefix(userFilter.getValue());
		filterTo.setTimestampFromInclusive(dateFromFilter.getValue());
		filterTo.setTimestampToInclusive(dateToFilter.getValue());
		filterTo.setLogType((LogType) logTypeFilter.getValue());
		return filterTo;
	}

}