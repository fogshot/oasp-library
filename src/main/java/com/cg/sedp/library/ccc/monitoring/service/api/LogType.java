package com.cg.sedp.library.ccc.monitoring.service.api;

/**
 * Possible categories for a log message.
 */
public enum LogType {

	/** logging information from a business transaction (e.g. like borrowing a book or changing its title) */
	TRANSACTION,
	/** logging some arbitrary information */
	INFO,
	/** logging some error */
	ERROR;
	
}
