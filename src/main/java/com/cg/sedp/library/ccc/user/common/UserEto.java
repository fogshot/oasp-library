package com.cg.sedp.library.ccc.user.common;

import javax.validation.constraints.NotNull;

/**
 * Transfer object for a user
 * 
 * @author pvonnieb
 */
public class UserEto {

	/**
	 * The business id for the user. Unique.
	 */
	@NotNull
	private UserIdType userId;

	/**
	 * the encrypted (hashed) password of the user.
	 */
	private String encryptedPassword;

	/**
	 * The given name of the user.
	 */
	private String firstName;

	/**
	 * The last name of the user.
	 */
	private String lastName;

	/**
	 * Flag indicating whether this user has the rights of a normal user.
	 */
	private boolean normalUserRight;

	/**
	 * Flag indicating whether this user has administrator rights.
	 */
	private boolean adminUserRight;

	/**
	 * Flag indicating whether this user has developer rights.
	 */
	private boolean developerUserRight;

	public boolean getAdminUserRight() {
		return adminUserRight;
	}

	public boolean getDeveloperUserRight() {
		return developerUserRight;
	}

	public String getEncryptedPassword() {
		return encryptedPassword;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getFullname() {
		return firstName + " " + lastName;
	}

	public String getLastName() {
		return lastName;
	}

	public boolean getNormalUserRight() {
		return normalUserRight;
	}

	public UserIdType getUserId() {
		return userId;
	}
	
	public String getUsername() {
		return getUserId().getUserIdString();
	}

	public void setAdminUserRight(boolean adminUserRight) {
		this.adminUserRight = adminUserRight;
	}

	public void setDeveloperUserRight(boolean developerUserRight) {
		this.developerUserRight = developerUserRight;
	}

	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setNormalUserRight(boolean normalUserRight) {
		this.normalUserRight = normalUserRight;
	}

	public void setUserId(UserIdType userId) {
		this.userId = userId;
	}
	
	public void setUserId(String username) {
		this.userId = new UserIdType(username);
	}

	/**
	 * Returns a string representation of this UserEto object. This
	 * implementation of toString returns a String containing String
	 * representations of all the fields and their values of this object in a
	 * human readable format.
	 * 
	 * @return String representing this object.
	 */
	@Override
	public String toString() {
		return "UserEto [userId=" + userId + ", fullname=" + getFullname() + ", normalUserRight=" + normalUserRight
				+ ", adminUserRight=" + adminUserRight + ", developerUserRight=" + developerUserRight + "]";
	}

}
