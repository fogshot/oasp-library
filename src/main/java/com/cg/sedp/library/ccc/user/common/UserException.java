package com.cg.sedp.library.ccc.user.common;

public class UserException extends Exception {

	/**
	 * generate version uid
	 */
	private static final long serialVersionUID = 1194107264397463936L;

	public UserException(String message) {
		super(message);
	}

}
