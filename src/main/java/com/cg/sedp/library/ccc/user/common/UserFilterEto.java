package com.cg.sedp.library.ccc.user.common;

/**
 * Transfer object for filter criteria for users
 * 
 * @author jvolmer
 *
 */
public class UserFilterEto {

	/**
	 * The business id for the user. Unique.
	 */
	private UserIdType userId;

	/**
	 * The given name of the user.
	 */
	private String firstName;

	/**
	 * The last name of the user.
	 */
	private String lastName;

	public UserFilterEto() {
		userId = null;
		firstName = "";
		lastName = "";
	}

	public UserFilterEto(UserIdType userId, String firstName, String lastName) {
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public UserIdType getUserId() {
		return userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setUserId(UserIdType userId) {
		this.userId = userId;
	}

	public void setUserId(String username) {
		this.userId = new UserIdType(username);
	}

	/**
	 * Returns a string representation of this UserFilterEto object. This
	 * implementation of toString returns a String containing String
	 * representations of all the fields and their values of this object in a
	 * human readable format.
	 * 
	 * @return String representing this object.
	 */
	@Override
	public String toString() {
		return "UserFilterEto [UserID=" + userId.getUserIdString() + "firstName=" + firstName + ", lastName="
				+ lastName;
	}
}
