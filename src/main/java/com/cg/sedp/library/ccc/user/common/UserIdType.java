package com.cg.sedp.library.ccc.user.common;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.cg.sedp.library.common.tools.ValidationChecker;

/**
 * System-wide UserId data type encapsulating data and validation logic for
 * valid user IDs.
 * </p>
 * A valid user id is at most 8 characters in length and made up from
 * alphanumeric characters only.
 */
public class UserIdType {

	public static final int MAX_USER_ID_LENGTH = 8;

	@NotNull
	@Size(min = 1, max = MAX_USER_ID_LENGTH)
	@Pattern(regexp = "^[a-zA-Z0-9]*$")
	private String userIdString;

	/**
	 * Creating a non-empty user id.
	 *
	 * @param userIdString
	 *            length 8 not null alphanumeric string to be used as UserId
	 */
	public UserIdType(@NotNull String userIdString) {
		this.userIdString = userIdString;
		ValidationChecker.validateNotNullObject(this);
	}

	public @NotNull String getUserIdString() {
		return userIdString;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userIdString == null) ? 0 : userIdString.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserIdType other = (UserIdType) obj;
		if (userIdString == null) {
			if (other.userIdString != null)
				return false;
		} else if (!userIdString.equals(other.userIdString))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return userIdString;
	}

}
