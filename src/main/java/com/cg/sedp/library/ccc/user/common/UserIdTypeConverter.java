package com.cg.sedp.library.ccc.user.common;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class UserIdTypeConverter implements AttributeConverter<UserIdType, String>{

	@Override
	public String convertToDatabaseColumn(UserIdType userId) {
		StringBuilder sb = new StringBuilder();
		sb.append(userId.getUserIdString());
		return sb.toString();
	}

	@Override
	public UserIdType convertToEntityAttribute(String userIdString) {
		return new UserIdType(userIdString);
	}

}
