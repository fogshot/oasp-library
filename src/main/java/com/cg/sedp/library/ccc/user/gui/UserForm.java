package com.cg.sedp.library.ccc.user.gui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cg.sedp.library.ccc.user.common.UserEto;
import com.cg.sedp.library.ccc.user.common.UserIdType;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

public class UserForm extends FormLayout {

	private static final long serialVersionUID = 1L;

	private final Logger logger = LoggerFactory.getLogger(UserForm.class);

	private UsersView usersView;

	private Button save = new Button("Save", this::save);
	private Button cancel = new Button("Cancel", this::cancel);
	private Button delete = new Button("Delete", this::delete);
	private TextField username = new TextField("Username");
	private TextField password = new TextField("Password");
	private TextField fullname = new TextField("Full Name");
	private CheckBox normalUserRight = new CheckBox("Normal User");
	private CheckBox adminUserRight = new CheckBox("Administrator");
	private CheckBox developerUserRight = new CheckBox("Developer");

	private UserEto userEto;

	private BeanFieldGroup<UserEto> formFieldBindings;

	public UserForm(UsersView usersView) {
		this.usersView = usersView;
		configureWidgets();
		buildLayout();
	}

	private void configureWidgets() {
		save.setStyleName(ValoTheme.BUTTON_PRIMARY);
		save.setClickShortcut(ShortcutAction.KeyCode.ENTER);
		setVisible(false);
	}

	private void buildLayout() {
		setSizeUndefined();
		setMargin(true);

		HorizontalLayout actions = new HorizontalLayout(save, delete, cancel);
		actions.setSpacing(true);

		addComponents(actions, username, password, fullname, normalUserRight, adminUserRight, developerUserRight);
	}

	public void save(Button.ClickEvent event) {
		try {
			formFieldBindings.commit();
			usersView.saveUser(userEto);
			Notification.show(String.format("Saved '%s'", userEto.getUserId().getUserIdString()), Type.TRAY_NOTIFICATION);
			usersView.refreshUsers();
		} catch (FieldGroup.CommitException e) {
			Notification.show("Please fix validation errors first", Type.ERROR_MESSAGE);
			logger.error("error commiting " + userEto, e);
		}
	}

	public void delete(Button.ClickEvent event) {
		try {
			formFieldBindings.commit();
			usersView.deleteUser(userEto.getUserId().getUserIdString());
			Notification.show(String.format("Deleted '%s'", userEto.getUserId().getUserIdString()), Type.TRAY_NOTIFICATION);
			usersView.refreshUsers();
		} catch (FieldGroup.CommitException e) {
			Notification.show("Please fix validation errors first", Type.ERROR_MESSAGE);
			logger.error("error deleting " + userEto, e);
		}
	}

	public void cancel(Button.ClickEvent event) {
		Notification.show("Cancelled", Type.TRAY_NOTIFICATION);
		usersView.cancelUserEdit();
	}
	
	public void edit(UserEto userEto) {
		edit(userEto, null);
	}

	public void edit(UserEto userEto, String password) {
		this.userEto = userEto;
		if (this.userEto != null) {
			formFieldBindings = BeanFieldGroup.bindFieldsBuffered(this.userEto, this);
			formFieldBindings.setEnabled(true);
			username.focus();
		}
		setVisible(this.userEto != null);
	}

	public void editNewUser() {
		UserEto userEto = new UserEto();
		userEto.setUserId(new UserIdType("USERID"));
		// TODO [pvonnieb] removed call to setPassword, ensure this gets done some other way
		userEto.setFirstName("");
		userEto.setLastName("");
		userEto.setAdminUserRight(false);
		userEto.setNormalUserRight(false);
		userEto.setDeveloperUserRight(false);
		edit(userEto, "");
	}

}