package com.cg.sedp.library.ccc.user.gui;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.cg.sedp.library.ccc.user.common.UserEto;
import com.cg.sedp.library.ccc.user.common.UserException;
import com.cg.sedp.library.ccc.user.common.UserIdType;
import com.cg.sedp.library.ccc.user.service.api.UserCrudService;
import com.cg.sedp.library.common.tools.BooleanHtmlCheckboxConverter;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.HtmlRenderer;

@ViewScope
@SpringView(name = UsersView.VIEW_NAME)
public class UsersView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "users-view";

    private static final long serialVersionUID = 1L;

	private TextField userNameFilter = new TextField();
	private Grid usersList = new Grid();
	private Button newUserButton = new Button("New User");
	private UserForm userForm;
	
	@Autowired
	private UserCrudService userService;

    @PostConstruct
    private void init() {
		userForm = new UserForm(this);
        setMargin(true);
        setSpacing(true);
        configureWidgets();
		addComponent(buildLayout());
		// initially refresh to read all data needed
		refreshUsers();
    }

    @Override
    public void enter(ViewChangeEvent event) {
        // the view is constructed in init() method
    }

	public void refreshUsers() {
		refreshUsers(userNameFilter.getValue());
	}

	public void saveUser(UserEto userEto) {
		try {
			userService.updateUser(userEto);
		} catch (UserException e) {
			try {
				userService.createUser(userEto);
			} catch (UserException e1) {
				// TODO [pvonnieb]: handle exception
				e1.printStackTrace();
			}
		}
	}

	public void deleteUser(String username) {
		try {
			userService.deleteUser(new UserIdType("username"));
		} catch (Exception e) {
			// TODO [pvonnieb]: handle exception
			e.printStackTrace();
		}
	}

	public void cancelUserEdit() {
		usersList.select(null);
		userForm.setVisible(false);
	}

	private void configureWidgets() {
		userNameFilter.setInputPrompt("Filter users by username ...");
		userNameFilter.addTextChangeListener(e -> refreshUsers(e.getText()));

		usersList.setContainerDataSource(new BeanItemContainer<>(UserEto.class));
		usersList.setColumnOrder("username", "fullname", "normalUserRight", "adminUserRight", "developerUserRight");
		usersList.removeColumn("userId");
		usersList.getColumn("username").setHeaderCaption("Username");
		usersList.getColumn("fullname").setHeaderCaption("Full Name");
		usersList.getColumn("normalUserRight").setHeaderCaption("Normal User");
		usersList.getColumn("normalUserRight").setRenderer(new HtmlRenderer(), new BooleanHtmlCheckboxConverter());
		usersList.getColumn("adminUserRight").setHeaderCaption("Admin");
		usersList.getColumn("adminUserRight").setRenderer(new HtmlRenderer(), new BooleanHtmlCheckboxConverter());
		usersList.getColumn("developerUserRight").setHeaderCaption("Developer");
		usersList.getColumn("developerUserRight").setRenderer(new HtmlRenderer(), new BooleanHtmlCheckboxConverter());
		usersList.setSelectionMode(Grid.SelectionMode.SINGLE);
		usersList.addSelectionListener(e -> userForm.edit((UserEto) usersList.getSelectedRow()));

		newUserButton.addClickListener(e -> userForm.editNewUser());
	}

    private HorizontalLayout buildLayout() {
		HorizontalLayout filterAndNewButton = new HorizontalLayout(userNameFilter, newUserButton);
		filterAndNewButton.setWidth("100%");
		userNameFilter.setWidth("100%");
		filterAndNewButton.setExpandRatio(userNameFilter, 1);

		VerticalLayout filterAndNewButtonAndList = new VerticalLayout(filterAndNewButton, usersList);
		filterAndNewButtonAndList.setSizeFull();
		usersList.setSizeFull();
		filterAndNewButtonAndList.setExpandRatio(usersList, 1);

		HorizontalLayout mainLayout = new HorizontalLayout(filterAndNewButtonAndList, userForm);
		mainLayout.setSizeFull();
		mainLayout.setExpandRatio(filterAndNewButtonAndList, 1);

		return mainLayout;
    }

	private void refreshUsers(String userNamePrefixFilterString) {
		List<UserEto> allUsers = (List<UserEto>) userService.getUsers();
		usersList.setContainerDataSource(
				new BeanItemContainer<>(UserEto.class, allUsers));
		userForm.setVisible(false);
	}

}