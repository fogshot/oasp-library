package com.cg.sedp.library.ccc.user.logic.api;

import java.util.List;

import com.cg.sedp.library.ccc.user.common.UserEto;
import com.cg.sedp.library.ccc.user.common.UserFilterEto;

/**
 * Interface for UC-USER-1: Browse User
 * 
 * @author jvolmer
 *
 */
public interface BrowseUserUseCase {

	/**
	 * Returns a list of all users for which the given filter applies.
	 * 
	 * @param userFilter
	 *            Filter criteria which should apply for returned users
	 * @return List of users for which the given filter applies
	 */
	public List<UserEto> findUsers(UserFilterEto userFilter);

}
