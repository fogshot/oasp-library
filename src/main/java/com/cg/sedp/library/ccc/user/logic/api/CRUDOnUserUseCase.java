package com.cg.sedp.library.ccc.user.logic.api;

import com.cg.sedp.library.ccc.user.common.UserEto;
import com.cg.sedp.library.ccc.user.common.UserException;
import com.cg.sedp.library.ccc.user.common.UserIdType;

/**
 * Interface for UC-USER-3: CRUD on Users
 * 
 * @author jvolmer
 *
 */
public interface CRUDOnUserUseCase {

	/**
	 * Creates the given user in the database
	 * 
	 * @param userEto
	 *            User that should be created
	 */
	public void createUser(UserEto userEto);

	/**
	 * Reads the user with the given userID from the database
	 * 
	 * @param userID
	 *            userID of user that should be read from database
	 * @throws Exception
	 */
	public UserEto readUser(UserIdType userID) throws UserException;

	/**
	 * Updates the given user in the database
	 * 
	 * @param userEto
	 *            User that should be updated in the database
	 * @throws Exception
	 */
	public void updateUser(UserEto userEto) throws UserException;

	/**
	 * Deletes the given user in the database
	 * 
	 * @param userID
	 *            User that should be deleted in the database
	 * @throws Exception
	 */
	public void deleteUser(UserIdType userID) throws UserException;

}
