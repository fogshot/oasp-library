package com.cg.sedp.library.ccc.user.logic.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cg.sedp.library.ccc.user.common.UserEto;
import com.cg.sedp.library.ccc.user.common.UserFilterEto;
import com.cg.sedp.library.ccc.user.logic.api.BrowseUserUseCase;
import com.cg.sedp.library.ccc.user.persistence.api.UserRepositoryDao;
import com.cg.sedp.library.ccc.user.persistence.entity.User;

/**
 * Implementation for UC-USER-1: Browse User
 * 
 * @author jvolmer
 *
 */
@Service
public class BrowseUserUseCaseImpl implements BrowseUserUseCase {

	@Autowired
	private UserRepositoryDao userRepositoryDao;

	@Override
	public List<UserEto> findUsers(UserFilterEto userFilter) {

		// If filter is empty, return all Users
		if (userFilter == null) {
			for (User u : userRepositoryDao.findAll()) {
				System.out.println(u.getLogin());
			}
			return mapUsersToUserEtos(userRepositoryDao.findAll());
		}

		// If filter contains UserID, search only for UserID
		if (userFilter.getUserId() != null) {
			return mapUsersToUserEtos(userRepositoryDao.findByLogin(userFilter.getUserId().getUserIdString()));
		}

		// If filter is empty, return all Users
		if (userFilter.getFirstName().isEmpty() && userFilter.getLastName().isEmpty()) {
			return mapUsersToUserEtos(userRepositoryDao.findAll());
		}

		// If firstName is empty, search for lastName
		if (userFilter.getFirstName().isEmpty()) {
			return mapUsersToUserEtos(userRepositoryDao.findByLastName(userFilter.getLastName()));
		}

		// If lastName is empty, search for firstName
		if (userFilter.getLastName().isEmpty()) {
			return mapUsersToUserEtos(userRepositoryDao.findByFirstName(userFilter.getFirstName()));
		}

		// Else, search for firstName and lastName
		return mapUsersToUserEtos(userRepositoryDao.findByFirstNameEqualsAndLastNameEquals(userFilter.getFirstName(),
				userFilter.getLastName()));
	}

	private List<UserEto> mapUsersToUserEtos(List<User> userList) {

		List<UserEto> userEtoList = new ArrayList<UserEto>();

		for (User user : userList) {
			UserEto userEto = new UserEto();
			userEto.setAdminUserRight(user.isAdminUserRight());
			userEto.setDeveloperUserRight(user.isDeveloperUserRight());
			userEto.setNormalUserRight(user.isNormalUserRight());
			userEto.setFirstName(user.getFirstName());
			userEto.setLastName(user.getLastName());
			userEto.setUserId(user.getLogin());

			userEtoList.add(userEto);
		}

		return userEtoList;
	}
}
