package com.cg.sedp.library.ccc.user.logic.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cg.sedp.library.ccc.user.common.UserEto;
import com.cg.sedp.library.ccc.user.common.UserException;
import com.cg.sedp.library.ccc.user.common.UserIdType;
import com.cg.sedp.library.ccc.user.logic.api.CRUDOnUserUseCase;
import com.cg.sedp.library.ccc.user.persistence.api.UserRepositoryDao;
import com.cg.sedp.library.ccc.user.persistence.entity.User;

@Service
public class CRUDOnUserUserUseCaseImpl implements CRUDOnUserUseCase {

	@Autowired
	private UserRepositoryDao userRepositoryDao;

	@Override
	public void createUser(UserEto userEto) {

		User user = new User();
		this.mapUserEtoToUser(userEto, userEto.getEncryptedPassword(), user);

		userRepositoryDao.save(user);
	}

	@Override
	public UserEto readUser(UserIdType userID) throws UserException {

		System.out.println("Suche nach " + userID.getUserIdString());
		List<User> userList = userRepositoryDao.findByLogin(userID.getUserIdString());

		UserEto userEto = new UserEto();
		this.mapUserToUserEto(getOnlyElement(userList), userEto);

		return userEto;
	}

	@Override
	public void updateUser(UserEto userEto) throws UserException {

		List<User> userList = userRepositoryDao.findByLogin(userEto.getUserId().getUserIdString());
		User user = getOnlyElement(userList);

		// TODO [pvonnieb]: use UserEto::getPassword() once implemented
		mapUserEtoToUser(userEto, encryptPassword(userEto.getEncryptedPassword()), user);

		userRepositoryDao.save(user);
	}

	@Override
	public void deleteUser(UserIdType userID) throws UserException {

		List<User> userList = userRepositoryDao.findByLogin(userID.getUserIdString());

		userRepositoryDao.delete(getOnlyElement(userList));
	}

	private void mapUserEtoToUser(UserEto userEto, User user) {

		user.setAdminUserRight(userEto.getAdminUserRight());
		user.setDeveloperUserRight(userEto.getDeveloperUserRight());
		user.setNormalUserRight(userEto.getNormalUserRight());
		user.setFirstName(userEto.getFirstName());
		user.setLastName(userEto.getLastName());
		user.setLogin(userEto.getUserId().getUserIdString());
	}

	private void mapUserEtoToUser(UserEto userEto, String password, User user) {

		mapUserEtoToUser(userEto, user);
		user.setPassword(password);
	}

	private void mapUserToUserEto(User user, UserEto userEto) {

		userEto.setAdminUserRight(user.isAdminUserRight());
		userEto.setDeveloperUserRight(user.isDeveloperUserRight());
		userEto.setNormalUserRight(user.isNormalUserRight());
		userEto.setFirstName(user.getFirstName());
		userEto.setLastName(user.getLastName());
		userEto.setUserId(user.getLogin());
		userEto.setEncryptedPassword(user.getPassword());
	}

	private User getOnlyElement(List<User> userList) throws UserException {

		// Check that size of list is 1, else throw exceptions
		if (userList.isEmpty()) {
			// TODO: Correct Exception
			throw new UserException("Leer!");
		}

		if (userList.size() > 1) {
			// TODO: Correct Exception
			throw new UserException("Mehr als einer!");
		}

		return userList.get(0);
	}
	
	private String encryptPassword(String password) {
		// TODO: hash password
		return password;
	}
}
