package com.cg.sedp.library.ccc.user.persistence.api;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cg.sedp.library.ccc.user.persistence.entity.User;

/**
 * JPA repository handling Users.
 */
public interface UserRepositoryDao extends JpaRepository<User, Long> {
	
	/**
	 * Find all Users with exact last name.
	 * @param lastname
	 * Last name of user.
	 * @return List of users with given last name.
	 */
	List<User> findByLastName(String lastname);
	
	/**
	 * Find all Users with exact first name.
	 * @param firstname
	 * First name of user.
	 * @return List of users with given first name.
	 */
	List<User> findByFirstName(String firstname);
	
	/**
	 * Find all Users with exact login name.
	 * @param login
	 * Login of user.
	 * @return List of users with given login.
	 */
	List<User> findByLogin(String login);
	
	/**
	 * Find all Users with exact combination of first name and last name.
	 * @param firstName
	 * First name of user.
	 * @param lastName
	 * Last name of user
	 * @return List of users with given login.
	 */
	List<User> findByFirstNameEqualsAndLastNameEquals(String firstName, String lastName);
}
