package com.cg.sedp.library.ccc.user.persistence.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity
public class User {

	@Id
	@GeneratedValue
	private long id;

	@Version
	private long version;

	private String firstName;

	private String lastName;

	private String login;

	private String password;

	private boolean adminUserRight;
	
	private boolean developerUserRight;
	
	private boolean normalUserRight;

	
	

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean isAdminUserRight() {
		return adminUserRight;
	}

	public void setAdminUserRight(boolean adminUserRight) {
		this.adminUserRight = adminUserRight;
	}

	public boolean isDeveloperUserRight() {
		return developerUserRight;
	}

	public void setDeveloperUserRight(boolean developerUserRight) {
		this.developerUserRight = developerUserRight;
	}

	public boolean isNormalUserRight() {
		return normalUserRight;
	}

	public void setNormalUserRight(boolean normalUserRight) {
		this.normalUserRight = normalUserRight;
	}
	
	
}
