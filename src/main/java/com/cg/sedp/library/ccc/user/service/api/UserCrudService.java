package com.cg.sedp.library.ccc.user.service.api;

import java.util.Collection;

import javax.annotation.Nonnull;

import com.cg.sedp.library.ccc.user.common.UserEto;
import com.cg.sedp.library.ccc.user.common.UserException;
import com.cg.sedp.library.ccc.user.common.UserIdType;

/**
 * This interface provides methods to perform select CRUD operations on users.
 * 
 * @author pvonnieb
 */
public interface UserCrudService {

	/**
	 * Find a user by its ID.
	 * 
	 * @param userId
	 *            the user Id to look up.
	 * 
	 * @return UserEto a user transfer object.
	 * @throws UserException
	 *             if no matching user could be found.
	 */
	@Nonnull
	UserEto getUser(@Nonnull UserIdType userId) throws UserException;

	/**
	 * Get a collection containing all users.
	 * 
	 * @return a collection of user transfer objects. If there are no users, an
	 *         empty collection will be returned.
	 */
	@Nonnull
	Collection<UserEto> getUsers();

	/**
	 * Delete a user
	 * 
	 * @throws UserException
	 *             if user can not be deleted or does not exist.
	 */
	void deleteUser(@Nonnull UserIdType userId) throws Exception;
	
	/**
	 * Update a user.
	 * 
	 * @throws UserException
	 * 				if user can not be updated or does not exist.
	 */
	void updateUser(@Nonnull UserEto userEto) throws UserException;
	
	/**
	 * Create a user.
	 * @param UserEto
	 * 
	 * @throws UserException
	 * 				if user could not be created.
	 */
	void createUser(@Nonnull UserEto userEto) throws UserException;

}