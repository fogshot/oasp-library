package com.cg.sedp.library.ccc.user.service.api;

import com.cg.sedp.library.ccc.user.common.UserEto;
import com.cg.sedp.library.ccc.user.common.UserException;

public interface UserInformationService {

	UserEto getUser() throws UserException;

}