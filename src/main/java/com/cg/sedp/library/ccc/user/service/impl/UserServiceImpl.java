package com.cg.sedp.library.ccc.user.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.cg.sedp.library.ccc.user.common.UserEto;
import com.cg.sedp.library.ccc.user.common.UserException;
import com.cg.sedp.library.ccc.user.common.UserIdType;
import com.cg.sedp.library.ccc.user.logic.api.BrowseUserUseCase;
import com.cg.sedp.library.ccc.user.logic.api.CRUDOnUserUseCase;
import com.cg.sedp.library.ccc.user.service.api.UserCrudService;
import com.cg.sedp.library.ccc.user.service.api.UserInformationService;

@Service
public class UserServiceImpl implements UserCrudService, UserInformationService {

	@Autowired
	CRUDOnUserUseCase userCrud;

	@Autowired
	BrowseUserUseCase browseUserUseCase;

	public UserEto getUser(UserIdType userId) throws UserException {
		return userCrud.readUser(userId);
	}

	public UserEto getUser() throws UserException {
		SecurityContext context = SecurityContextHolder.getContext();
		Authentication authentication = context.getAuthentication();
		Object principal = authentication.getPrincipal();

		String username;
		if (principal instanceof UserDetails) {
			username = ((UserDetails) principal).getUsername();
		} else {
			username = principal.toString();
		}

		return getUser(new UserIdType(username));
	}

	@Override
	public Collection<UserEto> getUsers() {
		return browseUserUseCase.findUsers(null);
	}

	@Override
	public void deleteUser(UserIdType userId) throws Exception {
		userCrud.deleteUser(userId);
	}

	@Override
	public void updateUser(UserEto userEto) throws UserException {
		userCrud.updateUser(userEto);
	}

	@Override
	public void createUser(UserEto userEto) throws UserException {
		userCrud.createUser(userEto);
	}

}
