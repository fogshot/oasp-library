package com.cg.sedp.library.common;

import javax.validation.constraints.NotNull;

import com.cg.sedp.library.common.tools.ValidationChecker;

/**
 * Base class for ID types which checks the ID for not-null
 * @author cschoenf
 *
 * @param <T> the type of the ID to use
 */
public class IdType<T> {

	@NotNull
	private final T id;

	public IdType(@NotNull T id) {
		this.id = id;
		ValidationChecker.validateNotNullObject(this);
	}
	
	public T getId() {
		return id;
	}
	
}
