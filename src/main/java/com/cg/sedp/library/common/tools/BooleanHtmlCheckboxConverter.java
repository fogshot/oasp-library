package com.cg.sedp.library.common.tools;

import java.util.Locale;

import com.vaadin.data.util.converter.Converter;

public class BooleanHtmlCheckboxConverter implements Converter<String, Boolean> {

	private static final long serialVersionUID = 1L;

	private static final String CHECKED = "<input type=\"checkbox\" checked>";
	private static final String UNCHECKED = "<input type=\"checkbox\">";

	@Override
	public Boolean convertToModel(String value, Class<? extends Boolean> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		return null;
	}

	@Override
	public String convertToPresentation(Boolean value, Class<? extends String> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		if (null == value)
			return "";
		else if (value)
			return CHECKED;
		else
			return UNCHECKED;
	}

	@Override
	public Class<Boolean> getModelType() {
		return Boolean.class;
	}

	@Override
	public Class<String> getPresentationType() {
		return String.class;
	}

}
