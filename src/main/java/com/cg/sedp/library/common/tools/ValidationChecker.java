package com.cg.sedp.library.common.tools;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;

import org.springframework.util.Assert;

/**
 * Helper class wrapping {@link Validator} to check all defined validation annotations
 * are met with.
 */
public class ValidationChecker {

	private final static Validator VALIDATOR = Validation.buildDefaultValidatorFactory().getValidator();

	public static void validateNotNullObject(@NotNull Object object) {
		Assert.notNull(object, "object to get validated is not allowed to be null");
		Set<ConstraintViolation<Object>> violations = VALIDATOR.validate(object);
		Assert.state(violations.size() == 0,
				"not all validation annotations are met on " + object + " ( violating " + violations + ")");
	}

}
