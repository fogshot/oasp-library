package com.cg.sedp.library.crossbranch.common;

import javax.xml.bind.annotation.XmlRootElement;

import com.cg.sedp.library.branch.borrow.common.BorrowedItemEto;
import com.cg.sedp.library.branch.catalog.common.MediaItemEto;
import com.cg.sedp.library.branch.reservation.common.ReservedItemEto;

@XmlRootElement
public class MediaSearchResultCto {

	private MediaItemEto media;
	private ReservedItemEto reservedItem;
	private BorrowedItemEto borrowedItem;
	
	MediaSearchResultCto() {
		
	}

	public MediaSearchResultCto(MediaItemEto media) {
		super();
		this.media = media;
	}

	public String getBorrowedBy() {
		return null == borrowedItem ? null : borrowedItem.getBorrowedBy().getUserIdString();
	}

	public MediaItemEto getMedia() {
		return media;
	}

	public String getReservedFor() {
		return null == reservedItem ? null : reservedItem.getUser().getUserIdString();
	}

	public void setReservedItem(ReservedItemEto reservedItem) {
		this.reservedItem = reservedItem;
	}
	public void setBorrowedItem(BorrowedItemEto borrowedItem) {
		this.borrowedItem = borrowedItem;
	}
	
	public boolean isBorrowed() {
		return null != borrowedItem;
	}

	public boolean isReserved() {
		return null != reservedItem;
	}

	public boolean isAvailable() {
		return !(isBorrowed() || isReserved());
	}
}
