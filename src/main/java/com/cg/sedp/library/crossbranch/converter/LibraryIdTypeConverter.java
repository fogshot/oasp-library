package com.cg.sedp.library.crossbranch.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.cg.sedp.library.crossbranch.datatypes.LibraryIdType;

@Converter
public class LibraryIdTypeConverter implements AttributeConverter<LibraryIdType, String> {
	

	 /**
	  * Convert LibraryIdType object to a String 
	  * 
	  */
	 @Override
	 public String convertToDatabaseColumn(LibraryIdType libraryIdType) {
	  return libraryIdType.getLibraryIdString();
	 }

	 /**
	  * Convert a String LibraryIdType
	  */
	 @Override
	 public LibraryIdType convertToEntityAttribute(String libraryIdString) {
	  return new LibraryIdType(libraryIdString);
	 }


}
