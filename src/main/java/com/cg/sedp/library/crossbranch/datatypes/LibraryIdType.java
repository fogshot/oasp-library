package com.cg.sedp.library.crossbranch.datatypes;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.cg.sedp.library.common.tools.ValidationChecker;

// @Embeddable // not needed because of converter.
public class LibraryIdType {
	
	/**
	 * System-wide LibraryId data type encapsulating data and validation logic for
	 * valid Library Ids.
	 * </p>
	 * A valid Library id is exactly 8 size length and made up from alphanumeric
	 * characters only.
	 */
		public static final int MAX_LIBRARY_ID_LENGTH = 8;

		@NotNull
		@Size(min = 1, max = MAX_LIBRARY_ID_LENGTH)
		@Pattern(regexp = "^[a-zA-Z0-9]*$")
		private String libraryIdString;

		/**
		 * Creating a non-empty library id.
		 *
		 * @param libraryIdString
		 *            length 8 not null alphanumeric string to be used as LibraryId
		 */
		public LibraryIdType(@NotNull String libraryIdString) {
			this.libraryIdString = libraryIdString;
			ValidationChecker.validateNotNullObject(this);
		}

		public @NotNull String getLibraryIdString() {
			return libraryIdString;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((libraryIdString == null) ? 0 : libraryIdString.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			LibraryIdType other = (LibraryIdType) obj;
			if (libraryIdString == null) {
				if (other.libraryIdString != null)
					return false;
			} else if (!libraryIdString.equals(other.libraryIdString))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "LibraryIdType [libraryIdString=" + libraryIdString + "]";
		}

}
