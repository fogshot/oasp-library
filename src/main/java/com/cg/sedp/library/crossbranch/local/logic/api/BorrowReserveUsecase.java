package com.cg.sedp.library.crossbranch.local.logic.api;

import com.cg.sedp.library.branch.borrow.common.AlreadyBorrowedException;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;

public interface BorrowReserveUsecase{
	
	void borrowMedia(MediaItemIdentifier mediaId) throws AlreadyBorrowedException;
	void reserveMedia(MediaItemIdentifier mediaId);

}
