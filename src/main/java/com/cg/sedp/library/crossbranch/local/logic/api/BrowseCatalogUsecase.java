package com.cg.sedp.library.crossbranch.local.logic.api;

import java.util.List;

import javax.validation.Valid;

import com.cg.sedp.library.branch.catalog.common.SearchCriteria;
import com.cg.sedp.library.crossbranch.common.MediaSearchResultCto;
import com.cg.sedp.library.crossbranch.datatypes.LibraryIdType;

/**
 * Browse Catalog Usecase (UC-Catalog-1)
 */
public interface BrowseCatalogUsecase {

	/**
	 * Get list of media which fulfill search criteria.
	 * 
	 * @param searchCriteria {@link SearchCriteria} to filter media catalog.
	 * 
	 * @param libraryId {@link LibraryIdType} of library to search in.
	 * 
	 * @return List of {@link MediaSearchResultCto} (maybe empty, but all objects are valid).
	 */
//	@NotNull
	@Valid
	List<MediaSearchResultCto> getMediaSearchResults(SearchCriteria searchCriteria, LibraryIdType libraryId);
	
}
