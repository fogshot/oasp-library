package com.cg.sedp.library.crossbranch.local.logic.api;

import java.util.List;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.reservation.common.ReservedItemEto;
import com.cg.sedp.library.ccc.user.common.UserIdType;

/**
 * Usecase for Users to check their reservations and borrows.
 */
public interface UserInfosystemUsecase {

	public boolean hasUserReservedMedia(UserIdType userId);
	
	public boolean hasUserBorrowedMedia(UserIdType userId);
	
	/**
	 * Returns a list of reserved media for local library.
	 * @param userId
	 */
	List<ReservedItemEto> getReservedMediaByUser(UserIdType userId);

	/**
	 * Returns a list of MediaItemIdentifier of borrowed media for local library.
	 * @param userId
	 */
	List<MediaItemIdentifier> getBorrowedMediaIdentifierByUser(UserIdType userId);

}
