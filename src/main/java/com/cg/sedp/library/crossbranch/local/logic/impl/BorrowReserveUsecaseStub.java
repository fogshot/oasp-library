package com.cg.sedp.library.crossbranch.local.logic.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cg.sedp.library.branch.borrow.common.AlreadyBorrowedException;
import com.cg.sedp.library.branch.borrow.service.api.BorrowExecutionService;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.reservation.service.api.IReservationService;
import com.cg.sedp.library.ccc.user.common.UserException;
import com.cg.sedp.library.ccc.user.service.api.UserInformationService;
import com.cg.sedp.library.crossbranch.local.logic.api.BorrowReserveUsecase;

@Service
public class BorrowReserveUsecaseStub implements BorrowReserveUsecase {
	
	@Autowired
	BorrowExecutionService borrowExecutionService;
	@Autowired
	IReservationService iReservationService;
	@Autowired
	UserInformationService userInformationService;
	
	
	@Override
	public void borrowMedia(MediaItemIdentifier mediaId) throws AlreadyBorrowedException{
		
		try {
			borrowExecutionService.borrow(mediaId, userInformationService.getUser().getUserId());
		} catch (UserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void reserveMedia(MediaItemIdentifier mediaId) {
		
		try {
			iReservationService.setReservation(userInformationService.getUser().getUserId(), mediaId, returnNextMonth());
		} catch (UserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	private java.sql.Date returnNextMonth(){
		Date date = new Date();
		int days = 30;
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);			
		return new java.sql.Date(cal.getTime().getTime());
	}

}
