package com.cg.sedp.library.crossbranch.local.logic.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cg.sedp.library.branch.borrow.common.BorrowedItemEto;
import com.cg.sedp.library.branch.catalog.common.MediaItemEto;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.catalog.common.SearchCriteria;
import com.cg.sedp.library.branch.reservation.common.ReservedItemEto;
import com.cg.sedp.library.ccc.user.common.UserEto;
import com.cg.sedp.library.ccc.user.common.UserIdType;
import com.cg.sedp.library.crossbranch.common.MediaSearchResultCto;
import com.cg.sedp.library.crossbranch.datatypes.LibraryIdType;
import com.cg.sedp.library.crossbranch.local.logic.api.BrowseCatalogUsecase;

@Service
public class BrowseCatalogUsecaseStub implements BrowseCatalogUsecase {
	
	@Override
	public List<MediaSearchResultCto> getMediaSearchResults(SearchCriteria searchCriteria, LibraryIdType libraryId) {
		
		List<MediaSearchResultCto> resultList = new ArrayList<MediaSearchResultCto>();
		
		if (searchCriteria != null && searchCriteria.getSearchPattern().equalsIgnoreCase("in$anep4tt3rn")) {
			return resultList;
		}
		
		UserIdType userId = new UserIdType("user0001");
		
		MediaItemIdentifier mediaId = new MediaItemIdentifier("stubmedium0001");
		
		UserEto user = new UserEto();
		user.setUserId(userId);
		user.setFirstName("Patrick");
		user.setLastName("TestUser");
		
		MediaItemEto mediaItem = new MediaItemEto();
		mediaItem.setMediaItemIdentifier(mediaId);
		mediaItem.setAuthor("Stub Buchautor");
		mediaItem.setTitle("Stub Buchtitel");
		
		ReservedItemEto reservedItem = new ReservedItemEto();
		reservedItem.setMediaItemIdentifier(mediaId);
		reservedItem.setUser(userId);
		
		BorrowedItemEto borrowedItem = null; // null means the item is not borrowed
		
		MediaSearchResultCto result1 = new MediaSearchResultCto(mediaItem);
		result1.setReservedItem(reservedItem);
		result1.setBorrowedItem(borrowedItem);
		
		resultList.add(result1);
		
		return resultList;
	}

}
