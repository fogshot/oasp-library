package com.cg.sedp.library.crossbranch.local.logic.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cg.sedp.library.branch.borrow.service.api.BorrowInformationService;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.reservation.common.ReservedItemEto;
import com.cg.sedp.library.branch.reservation.service.api.IReservationInformationService;
import com.cg.sedp.library.ccc.user.common.UserIdType;
import com.cg.sedp.library.crossbranch.local.logic.api.UserInfosystemUsecase;

@Service
public class UserInfosystemUsecaseImpl implements UserInfosystemUsecase {

	@Autowired
	IReservationInformationService reservationService;
	
	@Autowired
	BorrowInformationService borrowService;
	
	@Override
	public boolean hasUserReservedMedia(UserIdType userId) {
		return (getReservedMediaByUser(userId).size() > 0);
	}

	@Override
	public boolean hasUserBorrowedMedia(UserIdType userId) {
		return (getBorrowedMediaIdentifierByUser(userId).size() > 0);
	}

	@Override
	public List<ReservedItemEto> getReservedMediaByUser(UserIdType userId) {
		return reservationService.getReservations(userId);
	}

	@Override
	public List<MediaItemIdentifier> getBorrowedMediaIdentifierByUser(UserIdType userId) {
		return borrowService.getBorrowedMediaIDs(userId);
	}

}
