package com.cg.sedp.library.crossbranch.local.service.api;

import java.util.List;

import com.cg.sedp.library.branch.borrow.common.AlreadyBorrowedException;
import com.cg.sedp.library.branch.borrow.common.NotBorrowedException;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.catalog.common.SearchCriteria;
import com.cg.sedp.library.branch.common.OwnedByDifferentUserException;
import com.cg.sedp.library.branch.reservation.common.AlreadyReservedException;
import com.cg.sedp.library.branch.reservation.common.NotReservedException;
import com.cg.sedp.library.crossbranch.common.MediaSearchResultCto;
import com.cg.sedp.library.crossbranch.remote.common.RemoteBranchCommunicationException;
import com.cg.sedp.library.crossbranch.virtual.common.BranchInfo;
import com.cg.sedp.library.crossbranch.virtual.gui.NoBranchSelectedException;

public interface CatalogService {
	
	void borrow(MediaItemIdentifier id) throws AlreadyBorrowedException, NoBranchSelectedException, 
	NotBorrowedException, OwnedByDifferentUserException;

	void reserve(MediaItemIdentifier id) throws AlreadyReservedException, NoBranchSelectedException,
		RemoteBranchCommunicationException, NotReservedException, OwnedByDifferentUserException;
	
	List<MediaSearchResultCto> getCatalogList(SearchCriteria search);

}
