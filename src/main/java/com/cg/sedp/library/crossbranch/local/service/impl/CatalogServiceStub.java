package com.cg.sedp.library.crossbranch.local.service.impl;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cg.sedp.library.branch.borrow.common.AlreadyBorrowedException;
import com.cg.sedp.library.branch.borrow.common.NotBorrowedException;
import com.cg.sedp.library.branch.catalog.common.MediaItemEto;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.catalog.common.MediaItemType;
import com.cg.sedp.library.branch.catalog.common.SearchCriteria;
import com.cg.sedp.library.branch.common.OwnedByDifferentUserException;
import com.cg.sedp.library.branch.reservation.common.AlreadyReservedException;
import com.cg.sedp.library.branch.reservation.common.NotReservedException;
import com.cg.sedp.library.crossbranch.common.MediaSearchResultCto;
import com.cg.sedp.library.crossbranch.local.logic.api.BorrowReserveUsecase;
import com.cg.sedp.library.crossbranch.local.logic.api.BrowseCatalogUsecase;
import com.cg.sedp.library.crossbranch.local.service.api.CatalogService;
import com.cg.sedp.library.crossbranch.remote.common.BranchEto;
import com.cg.sedp.library.crossbranch.remote.common.RemoteBranchCommunicationException;
import com.cg.sedp.library.crossbranch.remote.service.api.LibraryService;
import com.cg.sedp.library.crossbranch.virtual.common.BranchInfo;
import com.cg.sedp.library.crossbranch.virtual.gui.NoBranchSelectedException;
import com.google.gwt.thirdparty.guava.common.collect.Lists;

@Service
public class CatalogServiceStub implements CatalogService{
	
	@Autowired
	BorrowReserveUsecase borrowReserveUsecase;
	
	private static final Logger LOGGER = Logger.getAnonymousLogger();
	
	public void borrow(MediaItemIdentifier id) throws AlreadyBorrowedException, NoBranchSelectedException, 
		NotBorrowedException, OwnedByDifferentUserException {
		LOGGER.info("Borrowed id: " + id);
		borrowReserveUsecase.borrowMedia(id);
	}

	public void reserve(MediaItemIdentifier id) throws AlreadyReservedException, NoBranchSelectedException,
		RemoteBranchCommunicationException, NotReservedException, OwnedByDifferentUserException {
		LOGGER.info("Reserved id: " + id);
		
	}

	@Override
	public List<MediaSearchResultCto> getCatalogList(SearchCriteria search) {
		LOGGER.info("SearchCriteria: " + search);
		MediaItemEto mediaEto = new MediaItemEto();
		mediaEto.setAuthor("Hans");
		mediaEto.setMediaItemIdentifier(new MediaItemIdentifier("1"));
		mediaEto.setMediaItemType(MediaItemType.Book);
		mediaEto.setTitle("abook");
		return Lists.newArrayList(
				new MediaSearchResultCto(mediaEto)
				);
	}
}