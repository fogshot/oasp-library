package com.cg.sedp.library.crossbranch.local.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.reservation.common.ReservedItemEto;
import com.cg.sedp.library.ccc.user.common.UserIdType;
import com.cg.sedp.library.crossbranch.local.logic.api.UserInfosystemUsecase;
import com.cg.sedp.library.crossbranch.local.service.api.UserInfosystemService;

@Service
public class UserInfosystemServiceImpl implements UserInfosystemService {

	@Autowired
	UserInfosystemUsecase userInfosystemUsecase;

	@Override
	public boolean hasUserReservedMedia(UserIdType userId) {
		return userInfosystemUsecase.hasUserReservedMedia(userId);
	}

	@Override
	public boolean hasUserBorrowedMedia(UserIdType userId) {
		return userInfosystemUsecase.hasUserBorrowedMedia(userId);
	}

	@Override
	public List<ReservedItemEto> getReservedMediaByUser(UserIdType userId) {
		return userInfosystemUsecase.getReservedMediaByUser(userId);
	}

	@Override
	public List<MediaItemIdentifier> getBorrowedMediaIdentifierByUser(UserIdType userId) {
		return userInfosystemUsecase.getBorrowedMediaIdentifierByUser(userId);
	}

}
