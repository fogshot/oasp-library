package com.cg.sedp.library.crossbranch.remote.common;

import javax.validation.constraints.NotNull;

import com.cg.sedp.library.crossbranch.datatypes.LibraryIdType;

public class BranchEto {

//	@NotNull
//	private long idTech;
	@NotNull
	private String id;
//	@NotNull
//	private LibraryIdType libraryIdType;
	@NotNull
	private String name;
	@NotNull
	private String webserviceUrl;

	
	public String getId() {
		//TODO Wenn alles läuft, hier sauber nacharbeiten. Id muss zur LibraryID werden.
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

//	public LibraryIdType getLibraryIdType() {
//		return libraryIdType;
//	}
//
//	public void setLibraryIdType(LibraryIdType libraryIdType) {
//		this.libraryIdType = libraryIdType;
//	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWebserviceUrl() {
		return webserviceUrl;
	}

	public void setWebserviceUrl(String webserviceUrl) {
		this.webserviceUrl = webserviceUrl;
	}
//	public long getIdTech() {
//		return idTech;
//	}
//
//	public void setIdTech(long idTech) {
//		this.idTech = idTech;
//	}
}