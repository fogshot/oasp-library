package com.cg.sedp.library.crossbranch.remote.common;

public class RemoteBranchCommunicationException extends Exception {

	private static final long serialVersionUID = 1L;

	public RemoteBranchCommunicationException(String message) {
		super(message);
	}

}
