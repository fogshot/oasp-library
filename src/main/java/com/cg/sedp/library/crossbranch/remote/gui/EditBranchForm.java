package com.cg.sedp.library.crossbranch.remote.gui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cg.sedp.library.crossbranch.remote.common.BranchEto;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

public class EditBranchForm extends FormLayout {

	private static final long serialVersionUID = 1L;

	private final Logger logger = LoggerFactory.getLogger(EditBranchForm.class);

	private ManageBranchesView view;

	private Button save = new Button("Save", this::save);
	private Button cancel = new Button("Cancel", this::cancel);
	private Button delete = new Button("Delete", this::delete);

	private TextField id = new TextField("Branch ID");
	private TextField name = new TextField("Name");
	private TextField webserviceUrl = new TextField("WebService URL");
	private Button ping = new Button("Ping", this::ping);

	// Model
	private BranchEto branch;
	// In case the ID is changed, store the original here for updating
	private String originalId;

	// Bindings for the model
	private BeanFieldGroup<BranchEto> formFieldBindings;

	public EditBranchForm(ManageBranchesView view) {
		this.view = view;
		configureWidgets();
		buildLayout();
	}

	private void configureWidgets() {
		id.setNullRepresentation("");
		name.setNullRepresentation("");
		webserviceUrl.setNullRepresentation("");
		save.setStyleName(ValoTheme.BUTTON_PRIMARY);
		save.setClickShortcut(ShortcutAction.KeyCode.ENTER);
		hide();
	}

	private void buildLayout() {
		setSizeUndefined();
		setMargin(true);
		HorizontalLayout actions = new HorizontalLayout(save, delete, cancel);
		actions.setSpacing(true);

		FormLayout branchAttributeFields = new FormLayout(id, name, webserviceUrl, ping);
		branchAttributeFields.setMargin(false);

		addComponents(actions, branchAttributeFields);
	}

	public void edit(BranchEto branch) {
		this.branch = branch;
		if (this.branch != null) {
			formFieldBindings = BeanFieldGroup.bindFieldsBuffered(this.branch, this);
			formFieldBindings.setEnabled(true);
			id.focus();

			// Show delete button only if the branch already had an ID when the
			// branch edit form is opened.
			originalId = branch.getId();
			delete.setVisible(originalId != null);
		}
		setVisible(this.branch != null);
	}

	/**
	 * Save the current branch
	 * 
	 * @param event
	 */
	public void save(Button.ClickEvent event) {
		try {
			formFieldBindings.commit();

			view.saveBranch(originalId, branch);
			hide();
			
		} catch (FieldGroup.CommitException e) {
			Notification.show("Please fix validation errors first", Notification.Type.ERROR_MESSAGE);
			logger.error("error commiting " + branch, e);
		}
	}

	/**
	 * Cancel editing
	 * 
	 * @param event
	 */
	public void cancel(Button.ClickEvent event) {
		hide();
		
		Notification.show("Cancelled", Notification.Type.TRAY_NOTIFICATION);
	}

	/**
	 * Delete the currently edited branch
	 * 
	 * @param event
	 */
	public void delete(Button.ClickEvent event) {
		try {
			formFieldBindings.commit();
			view.deleteBranch(branch);
			hide();
		} catch (FieldGroup.CommitException e) {
			Notification.show("Please fix validation errors first", Notification.Type.ERROR_MESSAGE);
			logger.error("error deleting " + branch, e);
		}
	}

	/**
	 * Ping the currently entered url value
	 * 
	 * @param event
	 */
	public void ping(Button.ClickEvent event) {
		view.pingBranch(webserviceUrl.getValue());
	}

	/**
	 * Close the form
	 */
	void hide() {
		setVisible(false);
	}
}