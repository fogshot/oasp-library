package com.cg.sedp.library.crossbranch.remote.gui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.cg.sedp.library.crossbranch.remote.common.BranchEto;
import com.cg.sedp.library.crossbranch.remote.service.api.BranchCrudUsecaseService;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.GeneratedPropertyContainer;
import com.vaadin.data.util.PropertyValueGenerator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.ButtonRenderer;

@ViewScope
@SpringView(name = ManageBranchesView.VIEW_NAME)
public class ManageBranchesView extends VerticalLayout implements View {

	private static final long serialVersionUID = 1L;

	public static final String VIEW_NAME = "manage-branches-view";
	
	@Autowired
	public BranchCrudUsecaseService branchCrudUserService;

	private Grid branchTable;
	private Button add = new Button("+", this::addBranch);
	private EditBranchForm editForm;


	@PostConstruct
	private void init() {

		setMargin(true);
		setSpacing(true);
		configureWidgets();
		buildLayout();

		loadBranches();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// the view is constructed in init() method
	}

	private void configureWidgets() {

		editForm = new EditBranchForm(this);

		branchTable = new Grid();
		initializeMediaList(null);

		// order
		branchTable.setColumnOrder("id", "name", "webserviceUrl", "delete", "ping");
		// columns
		branchTable.getColumn("id").setHeaderCaption("Branch ID");
		branchTable.getColumn("name").setHeaderCaption("Name");
		branchTable.getColumn("webserviceUrl").setHeaderCaption("WebService URL");
		branchTable.getColumn("delete").setHeaderCaption("");
		branchTable.getColumn("ping").setHeaderCaption("");

		branchTable.setSelectionMode(Grid.SelectionMode.SINGLE);
		branchTable.addSelectionListener(e -> editBranch((BranchEto) branchTable.getSelectedRow()));

		// Render a button that deletes the data row (item)
		branchTable.getColumn("delete").setRenderer(new ButtonRenderer(e -> deleteBranch((BranchEto) e.getItemId())));
		branchTable.getColumn("ping")
				.setRenderer(new ButtonRenderer(e -> pingBranch(((BranchEto) e.getItemId()).getWebserviceUrl())));

	}

	private void initializeMediaList(List<BranchEto> branches) {
		BeanItemContainer<BranchEto> container = new BeanItemContainer<>(BranchEto.class,
				branches);

		GeneratedPropertyContainer dynamicColumnGeneratorContainer = new GeneratedPropertyContainer(container);
		dynamicColumnGeneratorContainer.addGeneratedProperty("delete", new PropertyValueGenerator<String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public String getValue(Item item, Object itemId, Object propertyId) {
				return "Delete";
			}

			@Override
			public Class<String> getType() {
				return String.class;
			}
		});

		dynamicColumnGeneratorContainer.addGeneratedProperty("ping", new PropertyValueGenerator<String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public String getValue(Item item, Object itemId, Object propertyId) {
				return "Ping";
			}

			@Override
			public Class<String> getType() {
				return String.class;
			}
		});

		branchTable.setContainerDataSource(dynamicColumnGeneratorContainer);
	}

	private void buildLayout() {
		
		VerticalLayout table = new VerticalLayout(add, branchTable);
		table.setSpacing(true);
		
		HorizontalLayout layout = new HorizontalLayout(table, editForm);
		layout.setSpacing(true);
		branchTable.setSizeFull();
		layout.setSizeFull();

		// Initially hide the edit form
		editForm.hide();
		
		addComponent(layout);
	}

	public void loadBranches() {
		// TODO go to service
		List<BranchEto> hardCodedBranches = new ArrayList<BranchEto>(branchCrudUserService.getAllBranches());
//		BranchEto self = new BranchEto();
//		self.setId("selfId");
//		self.setName("self");
//		self.setWebserviceUrl("http://localhost:8088");
		//hardCodedBranches.add(self);
		initializeMediaList(hardCodedBranches);
	}
	
	private void addBranch(Button.ClickEvent event) {
		editForm.edit(new BranchEto());
	}

	void editBranch(BranchEto branch) {
		editForm.edit(branch);
	}

	void saveBranch(String originalId, BranchEto branch) {
		// TODO go to service
		Notification.show(String.format("Saved %s branch", branch.getId()), Notification.Type.TRAY_NOTIFICATION);
		branchCrudUserService.saveBranch(branch);
		loadBranches();
	}

	void deleteBranch(BranchEto branch) {
		// TODO go to service
		branchCrudUserService.deleteBranch(branch);
		loadBranches();
		Notification.show(String.format("Deleted %s branch", branch.getId()), Notification.Type.TRAY_NOTIFICATION);
	}

	/**
	 * Ping the given URL
	 * 
	 * @param url
	 */
	void pingBranch(String url) {
		// TODO ping branch
	}
	
}