package com.cg.sedp.library.crossbranch.remote.persistence.api;

import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.repository.JpaRepository;
import com.cg.sedp.library.crossbranch.remote.persistence.entity.Branch;

/**
 * JPA repository handling comments.
 */
public interface BranchDao extends JpaRepository<Branch, Long> {
	
	Branch findById(@NotNull String id);

}
