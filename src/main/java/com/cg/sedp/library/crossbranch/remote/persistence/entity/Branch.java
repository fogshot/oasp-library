package com.cg.sedp.library.crossbranch.remote.persistence.entity;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

import com.cg.sedp.library.crossbranch.converter.LibraryIdTypeConverter;
import com.cg.sedp.library.crossbranch.datatypes.LibraryIdType;

@Entity
public class Branch {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idTech;
	
	@Version
	private long version;
	
//	@Convert(converter = LibraryIdTypeConverter.class)
//	private LibraryIdType libraryIdType;
	
	private String id;
	
	private String name;
	
	private String webserviceUrl;

	public String getWebserviceUrl() {
		return webserviceUrl;
	}

	public void setWebserviceUrl(String webserviceUrl) {
		this.webserviceUrl = webserviceUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public long getIdTech() {
		return idTech;
	}

	public void setIdTech(long idTech) {
		this.idTech = idTech;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

//	public LibraryIdType getLibraryIdType() {
//		return libraryIdType;
//	}
//
//	public void setLibraryIdType(LibraryIdType libraryIdType) {
//		this.libraryIdType = libraryIdType;
//	}
	
}
