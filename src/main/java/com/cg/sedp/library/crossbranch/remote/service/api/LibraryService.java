package com.cg.sedp.library.crossbranch.remote.service.api;

import java.util.List;

import com.cg.sedp.library.crossbranch.virtual.common.BranchInfo;

public interface LibraryService {
	
	List<BranchInfo> getBranches();
	
	
	/**
	 * Get all branches (maybe restricted to prefix)
	 * 
	 * @return List of branches (maybe empty, but all media objects are valid)
	 */
//	@NotNull
//	@Valid
//	Collection<BranchEto> getAllBranches();

	/**
	 * Save or update the branch for given branch object
	 * 
	 * @param branchEto object with name, url and libraryId to be saved or updated
	 */
//	void saveBranch(@NotNull @Valid BranchEto branchEto);

	/**
	 * Delete branch for given media object and given user
	 * 
	 * @param branchEto object with name, url and libraryId to be deleted
	 */
//	void deleteBranch(@NotNull @Valid BranchEto branchEto);
	

}