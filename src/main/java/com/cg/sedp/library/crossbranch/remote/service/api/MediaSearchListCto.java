package com.cg.sedp.library.crossbranch.remote.service.api;

import java.util.List;

import com.cg.sedp.library.crossbranch.common.MediaSearchResultCto;

public class MediaSearchListCto {
	
	private List<MediaSearchResultCto> items;
	
	

	public MediaSearchListCto() {
		super();
	}

	public MediaSearchListCto(List<MediaSearchResultCto> items) {
		super();
		this.items = items;
	}

	public List<MediaSearchResultCto> getItems() {
		return items;
	}

	public void setItems(List<MediaSearchResultCto> items) {
		this.items = items;
	}
	
	

}
