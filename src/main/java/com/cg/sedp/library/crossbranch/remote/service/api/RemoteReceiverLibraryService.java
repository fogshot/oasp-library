package com.cg.sedp.library.crossbranch.remote.service.api;

import com.cg.sedp.library.branch.borrow.common.AlreadyBorrowedException;
import com.cg.sedp.library.branch.borrow.common.NotBorrowedException;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.catalog.common.SearchCriteria;
import com.cg.sedp.library.branch.common.OwnedByDifferentUserException;
import com.cg.sedp.library.branch.reservation.common.AlreadyReservedException;
import com.cg.sedp.library.branch.reservation.common.NotReservedException;
import com.cg.sedp.library.crossbranch.remote.common.BranchEto;
import com.cg.sedp.library.crossbranch.remote.common.RemoteBranchCommunicationException;
import com.cg.sedp.library.crossbranch.virtual.gui.NoBranchSelectedException;

public interface RemoteReceiverLibraryService {
	
	void ping();
	
	void borrow(String id) throws AlreadyBorrowedException, NoBranchSelectedException, 
	NotBorrowedException, OwnedByDifferentUserException;

	void reserve(MediaItemIdentifier id) throws AlreadyReservedException, NoBranchSelectedException,
		RemoteBranchCommunicationException, NotReservedException, OwnedByDifferentUserException;
	
	MediaSearchListCto getCatalog(SearchCriteria search);

}
