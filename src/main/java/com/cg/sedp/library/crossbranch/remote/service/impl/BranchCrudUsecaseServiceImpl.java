package com.cg.sedp.library.crossbranch.remote.service.impl;

import java.util.Collection;

import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.cg.sedp.library.crossbranch.remote.common.BranchEto;
import com.cg.sedp.library.crossbranch.remote.service.api.BranchCrudUsecaseService;
import com.cg.sedp.library.crossbranch.virtual.logic.api.BranchCrudUsecase;
import com.cg.sedp.library.demo.playground.service.impl.MediaCrudServiceImpl;

@Service
public class BranchCrudUsecaseServiceImpl implements BranchCrudUsecaseService {
	
	@Autowired
	private BranchCrudUsecase branchCrudUsecase;
	
	@Autowired
	private Validator validator;

	private final Logger logger = LoggerFactory.getLogger(BranchCrudUsecaseServiceImpl.class);

	@Override
	public Collection<BranchEto> getAllBranches() {
		logger.info("getAllBranches()");
		Collection<BranchEto> returnValue = branchCrudUsecase.getAllBranches();
		logger.info("getAlLBranches() return: " + returnValue);
		return returnValue;
	}

	@Override
	public void saveBranch(BranchEto branchEto) {
		logger.info("saveBranch(branchEto): " + branchEto.getId() + branchEto.getName() + branchEto.getWebserviceUrl());
		Assert.notNull(branchEto);
		Assert.state(validator.validate(branchEto).isEmpty());
		branchCrudUsecase.saveBranch(branchEto);
		logger.info("saveBranch(branchEto)");
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteBranch(BranchEto branchEto) {
		logger.info("deleteBranch(branchEto): " + branchEto);
		Assert.notNull(branchEto);
		Assert.state(validator.validate(branchEto).isEmpty());
		branchCrudUsecase.deleteBranch(branchEto);
		logger.info("deleteBranch(branchEto)");
	}

}
