package com.cg.sedp.library.crossbranch.remote.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cg.sedp.library.branch.borrow.common.AlreadyBorrowedException;
import com.cg.sedp.library.branch.borrow.common.NotBorrowedException;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.catalog.common.SearchCriteria;
import com.cg.sedp.library.branch.common.OwnedByDifferentUserException;
import com.cg.sedp.library.branch.reservation.common.AlreadyReservedException;
import com.cg.sedp.library.branch.reservation.common.NotReservedException;
import com.cg.sedp.library.crossbranch.local.service.api.CatalogService;
import com.cg.sedp.library.crossbranch.remote.common.RemoteBranchCommunicationException;
import com.cg.sedp.library.crossbranch.remote.service.api.MediaSearchListCto;
import com.cg.sedp.library.crossbranch.remote.service.api.RemoteReceiverLibraryService;
import com.cg.sedp.library.crossbranch.virtual.gui.NoBranchSelectedException;



@RestController
@RequestMapping("remote")
public class RemoteReceiverLibraryServiceImpl implements RemoteReceiverLibraryService{
	
	@Autowired
	private CatalogService catalogService;
	
	private final Logger logger = LoggerFactory.getLogger(RemoteReceiverLibraryServiceImpl.class);

	
	@RequestMapping("ping")
	@Override
	public void ping() {
		
	}

	@RequestMapping("borrow/{id}")
	@Override
	public void borrow(@PathVariable String id) throws AlreadyBorrowedException, NoBranchSelectedException,
			NotBorrowedException, OwnedByDifferentUserException {
		if(id == null) {
			throw new NullPointerException("id is null");
		}
		logger.info("Webservice was called. User wants to borrow " + id);

		MediaItemIdentifier mediaItemIdentifier = new MediaItemIdentifier();
		mediaItemIdentifier.setMediaItemIdentifier(id);
		catalogService.borrow(mediaItemIdentifier);
	}

	@RequestMapping("reserve/{id}")
	@Override
	public void reserve(@PathVariable MediaItemIdentifier id) throws AlreadyReservedException, NoBranchSelectedException,
			RemoteBranchCommunicationException, NotReservedException, OwnedByDifferentUserException {
		if(id == null) {
			throw new NullPointerException("id is null");
		}
		logger.info("Webservice was called. User wants to reserve " + id);
		catalogService.reserve(id);
	}

	@RequestMapping(value = "catalog", method = RequestMethod.POST)
	@Override
	public MediaSearchListCto getCatalog(@RequestBody SearchCriteria search) {
		return new MediaSearchListCto(catalogService.getCatalogList(search));
	}

}
