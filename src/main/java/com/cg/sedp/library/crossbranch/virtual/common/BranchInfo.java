package com.cg.sedp.library.crossbranch.virtual.common;

import com.cg.sedp.library.crossbranch.remote.common.BranchEto;

public class BranchInfo {

	private final String title;
	private final BranchEto remoteBranch;
	private final boolean local;

	public BranchInfo(String localBranchName) {
		this.local = true;
		this.title = localBranchName;
		this.remoteBranch = null;
	}

	public BranchInfo(BranchEto remoteBranch) {
		this.local = false;
		this.title = remoteBranch.getName();
		this.remoteBranch = remoteBranch;
	}

	public String getTitle() {
		return this.title + (isLocal() ? " (local)" : "");
	}

	public boolean isLocal() {
		return local;
	}
	
	public String getRemoteUrl() {
		return remoteBranch.getWebserviceUrl();
	}

	public String getRemoteId() {
		return remoteBranch.getId();
	}
}
