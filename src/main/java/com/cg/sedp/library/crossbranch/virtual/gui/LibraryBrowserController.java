package com.cg.sedp.library.crossbranch.virtual.gui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cg.sedp.library.branch.borrow.common.AlreadyBorrowedException;
import com.cg.sedp.library.branch.borrow.common.NotBorrowedException;
import com.cg.sedp.library.branch.catalog.common.MediaItemType;
import com.cg.sedp.library.branch.common.OwnedByDifferentUserException;
import com.cg.sedp.library.branch.reservation.common.AlreadyReservedException;
import com.cg.sedp.library.branch.reservation.common.NotReservedException;
import com.cg.sedp.library.crossbranch.common.MediaSearchResultCto;
import com.cg.sedp.library.crossbranch.remote.common.RemoteBranchCommunicationException;
import com.cg.sedp.library.crossbranch.remote.service.api.LibraryService;
import com.cg.sedp.library.crossbranch.virtual.common.BranchInfo;
import com.cg.sedp.library.crossbranch.virtual.service.api.VirtualCatalogService;
import com.vaadin.spring.annotation.ViewScope;

/**
 * Dialog core controller for the catalog browser managing all the actions on
 * top of the model and independent of the actual view technology.
 * 
 * @author cschoenf
 */
@Component
@ViewScope
class LibraryBrowserController {

	private LibraryBrowserModel model;
	
	@Autowired
	private VirtualCatalogService catalogService;
	
	@PostConstruct
	public void initLibraryBrowserController() {
		this.model = new LibraryBrowserModel();
		//this.libraryService = new LibraryServiceStub();

		// Put the available media types in the model
		// TODO fetch from service		
		List<MediaItemType> mediaTypesHardCoded = new ArrayList<>();
		mediaTypesHardCoded.add(MediaItemType.CD);
		mediaTypesHardCoded.add(MediaItemType.Book);
		mediaTypesHardCoded.add(MediaItemType.DVD);
		this.model.setMediaTypes(mediaTypesHardCoded);

		// Load the branches into the model
		List<BranchInfo> branches = catalogService.getBranches();
		branches.add(0, new BranchInfo("local"));
		this.model.setBranches(branches);
	}

	void resetFilter() {
		model.resetFilter();
	}

	/**
	 * Executes search using the current model search criteria and puts the
	 * results into the model
	 * 
	 * @throws NoBranchSelectedException
	 * @throws RemoteBranchCommunicationException
	 */
	void search() throws NoBranchSelectedException, RemoteBranchCommunicationException {
		model.setMediaSearchResults(catalogService.getCatalogList(this.model.getSelectedBranch(), model.getSearch()));
	}

	public void toggleBorrow(MediaSearchResultCto media) throws AlreadyBorrowedException, NoBranchSelectedException,
			NotBorrowedException, OwnedByDifferentUserException {
		catalogService.borrow(media.getMedia().getMediaItemIdentifier(), this.model.getSelectedBranch());
	}

	public void toggleReserve(MediaSearchResultCto media) throws AlreadyReservedException, NoBranchSelectedException,
			RemoteBranchCommunicationException, NotReservedException, OwnedByDifferentUserException {
		catalogService.reserve(media.getMedia().getMediaItemIdentifier(), this.model.getSelectedBranch());
	}

	public LibraryBrowserModel getModel() {
		return model;
	}
}
