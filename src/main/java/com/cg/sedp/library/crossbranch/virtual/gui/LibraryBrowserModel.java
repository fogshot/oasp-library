package com.cg.sedp.library.crossbranch.virtual.gui;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.cg.sedp.library.branch.catalog.common.MediaItemType;
import com.cg.sedp.library.branch.catalog.common.SearchCriteria;
import com.cg.sedp.library.crossbranch.common.MediaSearchResultCto;
import com.cg.sedp.library.crossbranch.virtual.common.BranchInfo;
import com.google.common.collect.Sets;

/**
 * Business model of the library browser UI data independent of any actual
 * technical UI framework.
 * 
 * @author cschoenf
 *
 */
public class LibraryBrowserModel {

	private String itemNr;

	private MediaItemType selectedMediaType;
	private List<MediaItemType> mediaTypes;

	private BranchInfo selectedBranch;
	private List<BranchInfo> branches;

	private String title;
	private String author;

	private List<MediaSearchResultCto> mediaSearchResults;

	public String getAuthor() {
		return author;
	}

	public List<BranchInfo> getBranches() {
		return branches;
	}

	public String getItemNr() {
		return itemNr;
	}

	public List<MediaSearchResultCto> getMediaSearchResults() {
		return mediaSearchResults;
	}

	public List<MediaItemType> getMediaTypes() {
		return mediaTypes;
	}

	public BranchInfo getSelectedBranch() {
		return selectedBranch;
	}

	public MediaItemType getSelectedMediaType() {
		return selectedMediaType;
	}

	public String getTitle() {
		return title;
	}

	public void resetFilter() {
		this.itemNr = null;
		this.selectedMediaType = null;
		this.selectedBranch = null;
		this.title = null;
		this.author = null;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setBranches(List<BranchInfo> branches) {
		this.branches = new ArrayList<>(branches);
		this.selectedBranch = branches.isEmpty() ? null : branches.get(0);
		// TODO sort branches
	}

	public void setItemNr(String itemNr) {
		this.itemNr = itemNr;
	}

	public void setMediaSearchResults(List<MediaSearchResultCto> mediaSearchResults) {
		this.mediaSearchResults = mediaSearchResults;
	}

	public void setMediaTypes(List<MediaItemType> mediaTypes) {
		this.mediaTypes = new ArrayList<>(mediaTypes);
		// TODO sort media types
	}

	public void setSelectedBranch(BranchInfo selectedBranch) {
		this.selectedBranch = selectedBranch;
	}

	public void setSelectedMediaType(MediaItemType selectedMediaType) {
		this.selectedMediaType = selectedMediaType;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public SearchCriteria getSearch() {
		SearchCriteria search = new SearchCriteria();
		search.setPatternAuthor(author);
		search.setPatternTitle(title);
		search.setSearchPattern(itemNr);
		search.setSelectedType(Sets.newHashSet(mediaTypes));
		return search;
	}
}