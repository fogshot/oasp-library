package com.cg.sedp.library.crossbranch.virtual.gui;

import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.cg.sedp.library.branch.borrow.common.AlreadyBorrowedException;
import com.cg.sedp.library.branch.borrow.common.NotBorrowedException;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifierVaadinConverter;
import com.cg.sedp.library.branch.catalog.common.MediaItemType;
import com.cg.sedp.library.branch.catalog.common.MediaItemTypeVaadinConverter;
import com.cg.sedp.library.branch.common.OwnedByDifferentUserException;
import com.cg.sedp.library.branch.reservation.common.AlreadyReservedException;
import com.cg.sedp.library.branch.reservation.common.NotReservedException;
import com.cg.sedp.library.crossbranch.common.MediaSearchResultCto;
import com.cg.sedp.library.crossbranch.remote.common.RemoteBranchCommunicationException;
import com.cg.sedp.library.crossbranch.virtual.common.BranchInfo;
import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.GeneratedPropertyContainer;
import com.vaadin.data.util.PropertyValueGenerator;
import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.themes.ValoTheme;

@ViewScope
@SpringView(name = LibraryBrowserView.VIEW_NAME)
public class LibraryBrowserView extends VerticalLayout implements View {

	public static final String VIEW_NAME = "library-browser-view";

	private static final long serialVersionUID = 1L;

	// Vaadin-independent controller for this view and its model
	@Autowired
	private LibraryBrowserController controller;

	private LibraryBrowserModel model;

	private TextField itemNr;
	private TextField title;
	private TextField author;
	private NativeSelect selectedMediaType;
	private NativeSelect selectedBranch;

	private Grid mediaItemList = new Grid();

	private Button resetFilterButton = new Button("Reset Filter", this::reset);
	private Button searchButton = new Button("Search", this::search);

	private BeanFieldGroup<LibraryBrowserModel> formFieldBindings;

	@PostConstruct
	private void init() {
		//controller = new LibraryBrowserController();
		model = controller.getModel();

		setMargin(true);
		setSpacing(false);
		configureWidgets();
		addComponent(buildLayout());

		formFieldBindings = BeanFieldGroup.bindFieldsUnbuffered(model, this);
		formFieldBindings.setEnabled(true);
		
		searchButton.click();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// the view is constructed in init() method
	}

	private void configureWidgets() {
		// converter
		title = new TextField("Title");
		title.setNullRepresentation("");

		author = new TextField("Author");
		author.setNullRepresentation("");

		itemNr = new TextField("Media ID");
		itemNr.setNullRepresentation("");

		selectedMediaType = new NativeSelect("Type");
		selectedMediaType
				.addValueChangeListener(e -> model.setSelectedMediaType((MediaItemType) selectedMediaType.getValue()));
		selectedMediaType.addItems(model.getMediaTypes());

		BeanItemContainer<BranchInfo> branches = new BeanItemContainer<BranchInfo>(BranchInfo.class,
				model.getBranches());
		selectedBranch = new NativeSelect("Branch", branches);
		selectedBranch.setItemCaptionPropertyId("title");
		selectedBranch.addValueChangeListener(e -> model.setSelectedBranch((BranchInfo) selectedBranch.getValue()));
		selectedBranch.setNullSelectionAllowed(false);

		// order
		initializeMediaList(null);
		mediaItemList.setColumnOrder("media.mediaItemIdentifier", "media.title", "media.author", "media.mediaItemType",
				"available", "borrowedBy", "reservedFor", "toggleReserve", "toggleBorrow");
		// columns
		mediaItemList.getColumn("media.mediaItemIdentifier").setHeaderCaption("Media ID");
		mediaItemList.getColumn("media.mediaItemIdentifier").setConverter(new MediaItemIdentifierVaadinConverter());
		mediaItemList.getColumn("media.title").setHeaderCaption("Title");
		mediaItemList.getColumn("media.author").setHeaderCaption("Author");
		mediaItemList.getColumn("media.mediaItemType").setHeaderCaption("Type");
		mediaItemList.getColumn("media.mediaItemType").setConverter(new MediaItemTypeVaadinConverter());
		mediaItemList.getColumn("available").setHeaderCaption("Available");
		mediaItemList.getColumn("borrowedBy").setHeaderCaption("Borrowed by");
		mediaItemList.getColumn("reservedFor").setHeaderCaption("Reserved for");

		mediaItemList.getColumn("toggleReserve").setHeaderCaption("Reservation");
		mediaItemList.getColumn("toggleBorrow").setHeaderCaption("Hand-out");

		mediaItemList.removeColumn("borrowed");
		mediaItemList.removeColumn("reserved");

		mediaItemList.setSelectionMode(Grid.SelectionMode.NONE);

		// Render a button that deletes the data row (item)
		mediaItemList.getColumn("toggleReserve")
				.setRenderer(new ButtonRenderer(e -> toggleReserve((MediaSearchResultCto) e.getItemId())));
		mediaItemList.getColumn("toggleBorrow")
				.setRenderer(new ButtonRenderer(e -> toggleBorrow((MediaSearchResultCto) e.getItemId())));

		searchButton.setStyleName(ValoTheme.BUTTON_PRIMARY);
		searchButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);
	}

	private void initializeMediaList(List<MediaSearchResultCto> mediaItems) {
		BeanItemContainer<MediaSearchResultCto> container = new BeanItemContainer<>(MediaSearchResultCto.class,
				mediaItems);

		// The nested Eto inside needs to be made known to the container
		container.addNestedContainerBean("media");

		GeneratedPropertyContainer dynamicColumnGeneratorContainer = new GeneratedPropertyContainer(container);
		dynamicColumnGeneratorContainer.addGeneratedProperty("toggleReserve", new PropertyValueGenerator<String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public String getValue(Item item, Object itemId, Object propertyId) {
				@SuppressWarnings("unchecked")
				MediaSearchResultCto eto = ((BeanItem<MediaSearchResultCto>) item).getBean();
				if (eto.isReserved()) {
					return "Cancel";
				} else {
					return "Reserve";
				}
			}

			@Override
			public Class<String> getType() {
				return String.class;
			}
		});

		dynamicColumnGeneratorContainer.addGeneratedProperty("toggleBorrow", new PropertyValueGenerator<String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public String getValue(Item item, Object itemId, Object propertyId) {
				@SuppressWarnings("unchecked")
				MediaSearchResultCto eto = ((BeanItem<MediaSearchResultCto>) item).getBean();
				if (eto.isBorrowed()) {
					return "Return";
				} else {
					return "Hand out";
				}
			}

			@Override
			public Class<String> getType() {
				return String.class;
			}
		});

		mediaItemList.setContainerDataSource(dynamicColumnGeneratorContainer);
	}

	private HorizontalLayout buildLayout() {

		HorizontalLayout filterLine1 = new HorizontalLayout(itemNr, selectedMediaType, selectedBranch);
		filterLine1.setSpacing(true);
		HorizontalLayout filterLine2 = new HorizontalLayout(title, author);
		filterLine2.setSpacing(true);
		HorizontalLayout filterButtons = new HorizontalLayout(resetFilterButton, searchButton);
		filterButtons.setSpacing(true);

		FormLayout filterPanel = new FormLayout(filterLine1, filterLine2, filterButtons);
		filterPanel.setMargin(false);

		VerticalLayout filterAndList = new VerticalLayout(filterPanel, mediaItemList);
		filterAndList.setSpacing(true);
		filterAndList.setSizeFull();
		mediaItemList.setSizeFull();
		filterAndList.setExpandRatio(mediaItemList, 1);

		HorizontalLayout mainLayout = new HorizontalLayout(filterAndList);
		mainLayout.setSizeFull();
		mainLayout.setExpandRatio(filterAndList, 1);

		return mainLayout;
	}

	private void reset(Button.ClickEvent event) {
		controller.resetFilter();
	}

	private void search(Button.ClickEvent event) {
		// do search
		try {
			controller.search();
		} catch (NoBranchSelectedException e) {
			indicateMissingBranchSelection();
		} catch (RemoteBranchCommunicationException e) {
			remoteBranchCommunicationError(e);
		}

		// set model
		initializeMediaList(model.getMediaSearchResults());
	}

	private void toggleBorrow(MediaSearchResultCto media) {
		Logger.getAnonymousLogger().info("borrowing :: " + media.getClass());
		Logger.getAnonymousLogger().info("borrowing :: " + media);

		try {
			controller.toggleBorrow(media);
		} catch (NoBranchSelectedException e) {
			indicateMissingBranchSelection();
		} catch (AlreadyBorrowedException e) {
			Notification.show("The media is already borrowed by someone else.", Type.ERROR_MESSAGE);
		} catch (NotBorrowedException e) {
			Notification.show("The media is not borrowed. Cannot unborrow.", Type.ERROR_MESSAGE);
		} catch (OwnedByDifferentUserException e) {
			differentUserMessage(e);
		}
	}

	private void toggleReserve(MediaSearchResultCto media) {
		Logger.getAnonymousLogger().info("reserving :: " + media);
		try {
			controller.toggleReserve(media);
		} catch (NoBranchSelectedException e) {
			indicateMissingBranchSelection();
		} catch (RemoteBranchCommunicationException e) {
			remoteBranchCommunicationError(e);
		} catch (AlreadyReservedException e) {
			Notification.show("The media is already reserved by someone else.", Type.ERROR_MESSAGE);
		} catch (NotReservedException e) {
			Notification.show("The media is not borrowed. Cannot unborrow.", Type.ERROR_MESSAGE);
		} catch (OwnedByDifferentUserException e) {
			differentUserMessage(e);
		}
	}

	private void differentUserMessage(OwnedByDifferentUserException e) {
		Notification.show("The media is not borrowed/reserved by a different user: " + e.getOwnedBy().getUserIdString(),
				Type.ERROR_MESSAGE);
	}

	private void indicateMissingBranchSelection() {
		Notification.show("Please select a branch first.", Type.ASSISTIVE_NOTIFICATION);
	}

	private void remoteBranchCommunicationError(RemoteBranchCommunicationException e) {
		Notification.show("Talking to remote branch failed. " + e.getMessage(), Type.ERROR_MESSAGE);
	}
}