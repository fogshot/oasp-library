package com.cg.sedp.library.crossbranch.virtual.logic.api;

import java.util.Collection;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.cg.sedp.library.crossbranch.remote.common.BranchEto;
//import com.cg.sedp.library.demo.playground.common.CommentEto;
//import com.cg.sedp.library.demo.playground.common.CommentedMediaTo;

/**
 * CRUD use cases for branch objects.
 */
public interface BranchCrudUsecase {
	
	/**
	 * Get all branches (maybe restricted to prefix)
	 * 
	 * @return List of branches (maybe empty, but all media objects are valid)
	 */
	@NotNull
	@Valid
	Collection<BranchEto> getAllBranches();

	/**
	 * Save or update the branch for given branch object
	 * 
	 * @param branchEto object with name, url and libraryId to be saved or updated
	 */
	void saveBranch(@NotNull @Valid BranchEto branchEto);

	/**
	 * Delete branch for given media object and given user
	 * 
	 * @param branchEto object with name, url and libraryId to be deleted
	 */
	void deleteBranch(@NotNull @Valid BranchEto branchEto);

}
