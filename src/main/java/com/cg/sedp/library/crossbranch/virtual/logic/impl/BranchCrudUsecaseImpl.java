package com.cg.sedp.library.crossbranch.virtual.logic.impl;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.cg.sedp.library.crossbranch.remote.common.BranchEto;
import com.cg.sedp.library.crossbranch.remote.persistence.api.BranchDao;
import com.cg.sedp.library.crossbranch.remote.persistence.entity.Branch;
import com.cg.sedp.library.crossbranch.remote.service.impl.BranchCrudUsecaseServiceImpl;
import com.cg.sedp.library.crossbranch.virtual.logic.api.BranchCrudUsecase;

@Service
public class BranchCrudUsecaseImpl implements BranchCrudUsecase {
	
	@Autowired
	private BranchDao branchDao;
	
	@Autowired
	private Validator validator;
	
	private final Logger logger = LoggerFactory.getLogger(BranchCrudUsecaseServiceImpl.class);

	@Override
	public Collection<BranchEto> getAllBranches() {
		System.out.println("RETURN ALL BRANCHES");
		return branchDao.findAll().stream().map(this::mapBranch).collect(Collectors.toList());
	}

	@Override
	public void saveBranch(BranchEto branchEto) {
		logger.info(String.valueOf(branchEto == null));
		Branch toBeUpdatedBranch = branchDao.findById(branchEto.getId());
		if(toBeUpdatedBranch == null) {
			toBeUpdatedBranch = new Branch();
		}
		toBeUpdatedBranch.setId(branchEto.getId());
		toBeUpdatedBranch.setName(branchEto.getName());
		toBeUpdatedBranch.setWebserviceUrl(branchEto.getWebserviceUrl());
		branchDao.save(toBeUpdatedBranch);
		logger.info("save done: " + branchEto.getId() + " " +  branchEto.getName());
	}

	@Override
	public void deleteBranch(BranchEto branchEto) {
		Branch toBeUpdatedBranch = branchDao.findById(branchEto.getId());
		branchDao.delete(toBeUpdatedBranch);
		logger.info("delete done: " + branchEto.getId() + " " + branchEto.getName());
		
	}
	
	private BranchEto mapBranch(Branch branch) {
		logger.info("Map to ETO: " + branch.getId());
		BranchEto branchEto = new BranchEto();
		//TODO wenn alles funktioniert hier ID zu long switchen
		branchEto.setId(String.valueOf(branch.getId()));
//		branchEto.setIdTech(branch.getIdTech());
//		branchEto.setLibraryIdType(branch.getLibraryIdType());
		branchEto.setName(branch.getName());
		branchEto.setWebserviceUrl(branch.getWebserviceUrl());
//		Assert.state(validator.validate(branchEto).isEmpty());
		return branchEto;
	}
	
}
