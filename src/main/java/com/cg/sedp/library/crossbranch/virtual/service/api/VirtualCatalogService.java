package com.cg.sedp.library.crossbranch.virtual.service.api;

import java.util.List;

import com.cg.sedp.library.branch.borrow.common.AlreadyBorrowedException;
import com.cg.sedp.library.branch.borrow.common.NotBorrowedException;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.catalog.common.SearchCriteria;
import com.cg.sedp.library.branch.common.OwnedByDifferentUserException;
import com.cg.sedp.library.branch.reservation.common.AlreadyReservedException;
import com.cg.sedp.library.branch.reservation.common.NotReservedException;
import com.cg.sedp.library.crossbranch.common.MediaSearchResultCto;
import com.cg.sedp.library.crossbranch.remote.common.RemoteBranchCommunicationException;
import com.cg.sedp.library.crossbranch.virtual.common.BranchInfo;
import com.cg.sedp.library.crossbranch.virtual.gui.NoBranchSelectedException;

public interface VirtualCatalogService {
	
	void borrow(MediaItemIdentifier id, BranchInfo branch) throws AlreadyBorrowedException, NoBranchSelectedException, 
	NotBorrowedException, OwnedByDifferentUserException;

	void reserve(MediaItemIdentifier id, BranchInfo branch) throws AlreadyReservedException, NoBranchSelectedException,
		RemoteBranchCommunicationException, NotReservedException, OwnedByDifferentUserException;
	
	List<BranchInfo> getBranches();
	
	List<MediaSearchResultCto> getCatalogList(BranchInfo branch, SearchCriteria searchCriteria);

}
