package com.cg.sedp.library.crossbranch.virtual.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.cg.sedp.library.branch.borrow.common.AlreadyBorrowedException;
import com.cg.sedp.library.branch.borrow.common.NotBorrowedException;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.catalog.common.SearchCriteria;
import com.cg.sedp.library.branch.common.OwnedByDifferentUserException;
import com.cg.sedp.library.branch.reservation.common.AlreadyReservedException;
import com.cg.sedp.library.branch.reservation.common.NotReservedException;
import com.cg.sedp.library.crossbranch.common.MediaSearchResultCto;
import com.cg.sedp.library.crossbranch.local.service.api.CatalogService;
import com.cg.sedp.library.crossbranch.remote.common.BranchEto;
import com.cg.sedp.library.crossbranch.remote.common.RemoteBranchCommunicationException;
import com.cg.sedp.library.crossbranch.remote.service.api.MediaSearchListCto;
import com.cg.sedp.library.crossbranch.virtual.common.BranchInfo;
import com.cg.sedp.library.crossbranch.virtual.gui.NoBranchSelectedException;
import com.cg.sedp.library.crossbranch.virtual.logic.api.BranchCrudUsecase;
import com.cg.sedp.library.crossbranch.virtual.service.api.VirtualCatalogService;
import com.google.gwt.thirdparty.guava.common.collect.Lists;

@Service
public class VirtualCatalogServiceImpl implements VirtualCatalogService{
	
	@Autowired
	private BranchCrudUsecase branchService;
	
	@Autowired
	private CatalogService catalogService;

	@Override
	public void borrow(MediaItemIdentifier id, BranchInfo branch) throws AlreadyBorrowedException,
			NoBranchSelectedException, NotBorrowedException, OwnedByDifferentUserException {
		if (branch.isLocal()){
			catalogService.borrow(id);
		}
		else{
			RestTemplate rest = new RestTemplate();
			rest.getForObject(branch.getRemoteUrl()+"/remote/borrow/"+id, String.class);
		}
		
	}

	@Override
	public void reserve(MediaItemIdentifier id, BranchInfo branch) throws AlreadyReservedException, NoBranchSelectedException,
			RemoteBranchCommunicationException, NotReservedException, OwnedByDifferentUserException {
		if (branch.isLocal()){
			catalogService.reserve(id);
		}
		else{
			RestTemplate rest = new RestTemplate();
			rest.getForObject(branch.getRemoteUrl()+"/remote/reserve/"+id, String.class);
		}
		
	}

	@Override
	public List<BranchInfo> getBranches() {
		return branchService.getAllBranches().stream()
				.map(eto -> new BranchInfo(eto))
				.collect(Collectors.toList());
	}

	@Override
	public List<MediaSearchResultCto> getCatalogList(BranchInfo branch, SearchCriteria search) {
		if(branch.isLocal()) {
			return catalogService.getCatalogList(search);
		}
		RestTemplate rest = new RestTemplate();
		MediaSearchListCto response = rest.postForObject(branch.getRemoteUrl() + "/remote/catalog", search, MediaSearchListCto.class);
		return response.getItems();
	}

}
