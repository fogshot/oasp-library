package com.cg.sedp.library.demo.dummy.gui;

import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.cg.sedp.library.demo.dummy.service.api.DummyService;
import com.cg.sedp.library.demo.playground.common.DummyInputTo;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;

@ViewScope
@SpringView(name = DummyView.VIEW_NAME)
public class DummyView extends VerticalLayout implements View {

	private static final long serialVersionUID = 1L;

	public static final String VIEW_NAME = "dummy-view";

	// view widgets (only the ones to interact with)
	private TextArea serverMessages = new TextArea("Ping Results");

	// services called from view
	@Autowired
	private DummyService dummyService;

	// configure widgets + layout them
	@PostConstruct
	private void init() {
		VerticalLayout layout = new VerticalLayout();
		layout.addComponent(new Label("Hello Dummy!"));
		layout.addComponent(new Button("Button", this::onButton));
		layout.addComponent(serverMessages = new TextArea("Service Results"));
		addComponent(layout);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// the view is constructed in init() method
	}

	// actions
	private void onButton(Button.ClickEvent event) {
		String messageNew = dummyService.doSomething(new DummyInputTo(new Date())).getMessage();
		String messagesOld = serverMessages.getValue();
		serverMessages.setValue(messagesOld+messageNew);
	}

}