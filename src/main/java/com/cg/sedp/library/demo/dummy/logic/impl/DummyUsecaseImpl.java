package com.cg.sedp.library.demo.dummy.logic.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cg.sedp.library.demo.dummy.logic.api.DummyUsecase;
import com.cg.sedp.library.demo.dummy.persistence.api.DummyRepositoryDao;
import com.cg.sedp.library.demo.dummy.persistence.entity.DummyEntity;
import com.cg.sedp.library.demo.playground.common.DummyInputTo;
import com.cg.sedp.library.demo.playground.common.DummyOutputTo;

@Service
public class DummyUsecaseImpl implements DummyUsecase {

	@Autowired
	private DummyRepositoryDao dummyRepositoryDao;

	@Override
	public DummyOutputTo doSomething(DummyInputTo input) {
		DummyEntity entity = mapToDummyEntity(input);
		return mapToDummyOutputTo(dummyRepositoryDao.findByStringValue(entity.getStringValue()));
	}

	private DummyOutputTo mapToDummyOutputTo(DummyEntity entity) {
		if (null == entity)
			return new DummyOutputTo("nothing found");
		else
			return new DummyOutputTo(entity.toString());
	}

	private DummyEntity mapToDummyEntity(DummyInputTo input) {
		DummyEntity dummyEntity = new DummyEntity();
		dummyEntity.setStringValue(input.getCallTimestamp().toString());
		return dummyEntity;
	}

}