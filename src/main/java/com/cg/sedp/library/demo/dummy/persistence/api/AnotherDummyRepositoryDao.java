package com.cg.sedp.library.demo.dummy.persistence.api;

import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cg.sedp.library.demo.dummy.persistence.entity.AnotherDummyEntity;
import com.cg.sedp.library.demo.dummy.persistence.entity.DummyType;

public interface AnotherDummyRepositoryDao extends JpaRepository<AnotherDummyEntity, Long> {

    @NotNull
    AnotherDummyEntity findByDummyType(@NotNull DummyType dummyType);

}