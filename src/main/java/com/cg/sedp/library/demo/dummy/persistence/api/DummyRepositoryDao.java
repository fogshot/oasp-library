package com.cg.sedp.library.demo.dummy.persistence.api;

import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cg.sedp.library.demo.dummy.persistence.entity.DummyEntity;

public interface DummyRepositoryDao extends JpaRepository<DummyEntity, Long> {

	@NotNull
	DummyEntity findByStringValue(@NotNull String stringValue);

}