package com.cg.sedp.library.demo.dummy.persistence.entity;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

@Entity
public class AnotherDummyEntity {

    @Id
    @GeneratedValue
    private long id;

    @Version
    private long version;

    @Embedded
    @NotNull
    private DummyType dummyType;

    @NotNull
    private String stringValue;

    public AnotherDummyEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    public DummyType getDummyType() {
        return dummyType;
    }

    public void setDummyType(DummyType dummyType) {
        this.dummyType = dummyType;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime * result + (int) (version ^ (version >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AnotherDummyEntity other = (AnotherDummyEntity) obj;
        if (id != other.id) {
            return false;
        }
        if (version != other.version) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AnotherDummyEntity [id=" + id + ", version=" + version + ", dummyType=" + dummyType + ", stringValue=" + stringValue + "]";
    }

}