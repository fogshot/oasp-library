package com.cg.sedp.library.demo.dummy.persistence.entity;

public class DummyType {

    private String dummyId;

    public DummyType() {
        super();
    }

    public DummyType(String dummyId) {
        super();
        this.dummyId = dummyId;
    }

    public String getDummyId() {
        return dummyId;
    }

    public void setDummyId(String dummyId) {
        this.dummyId = dummyId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dummyId == null) ? 0 : dummyId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        DummyType other = (DummyType) obj;
        if (dummyId == null) {
            if (other.dummyId != null) {
                return false;
            }
        }
        else if (!dummyId.equals(other.dummyId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DummyType [dummyId=" + dummyId + "]";
    }

}