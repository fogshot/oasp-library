package com.cg.sedp.library.demo.dummy.service.api;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.cg.sedp.library.demo.playground.common.DummyInputTo;
import com.cg.sedp.library.demo.playground.common.DummyOutputTo;

public interface DummyService {
	
	@NotNull
	@Valid
	DummyOutputTo doSomething(@NotNull @Valid DummyInputTo input);

}
