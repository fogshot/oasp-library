package com.cg.sedp.library.demo.dummy.service.impl;

import javax.validation.Valid;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.cg.sedp.library.demo.dummy.logic.api.DummyUsecase;
import com.cg.sedp.library.demo.dummy.service.api.DummyService;
import com.cg.sedp.library.demo.playground.common.DummyInputTo;
import com.cg.sedp.library.demo.playground.common.DummyOutputTo;

@Service
public class DummyServiceImpl implements DummyService {

	@Autowired
	private Validator validator;

	private final Logger logger = LoggerFactory.getLogger(DummyServiceImpl.class);

	@Autowired
	private DummyUsecase usecase;

	@Transactional // at least for services that write to database
	@Override
	public @NotNull @Valid DummyOutputTo doSomething(@NotNull @Valid DummyInputTo input) {
		// log
		logger.info("doSomething(input): " + input);
		
		// validate: check there is no validation error
		Assert.notNull(input);
		Assert.state(validator.validate(input).isEmpty());
		
		// call usecase
		DummyOutputTo returnValue = usecase.doSomething(input);
		
		// check result is valid + log return value
		Assert.notNull(returnValue);
		Assert.state(validator.validate(returnValue).isEmpty());
		logger.info("doSomething(): " + returnValue);

		return returnValue;
	}

}