package com.cg.sedp.library.demo.playground.common;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cg.sedp.library.ccc.user.common.UserIdType;

/**
 * Transport object for a users comment.
 */
public class CommentEto {

	private Long id;

	private long version;

	@NotNull
	private UserIdType userId;

	@Size(min = 0, max = 128)
	private String comment;

	@NotNull
	private long mediaId;
	
	public CommentEto() {
	}
	
	public CommentEto(UserIdType userId, String comment, long mediaId) {
		super();
		this.userId = userId;
		this.comment = comment;
		this.mediaId = mediaId;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public UserIdType getUserId() {
		return userId;
	}

	public void setUserId(UserIdType userId) {
		this.userId = userId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public long getMediaId() {
		return mediaId;
	}

	public void setMediaId(long mediaId) {
		this.mediaId = mediaId;
	}

	@Override
	public String toString() {
		return "CommentEto [id=" + id + ", version=" + version + ", userId=" + userId + ", comment=" + comment
				+ ", mediaId=" + mediaId + "]";
	}	

}