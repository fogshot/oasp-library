package com.cg.sedp.library.demo.playground.common;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Transport object for a media object and a connected user comment.
 */
public class CommentedMediaTo {

	private long mediaId;

	private long version;
		
	@NotNull
	private PlaygroundMediaType mediaType;

	@NotNull
	@Size(min=0, max=256)
	private String title;

	@NotNull
	@Size(min=0, max=1024)
	private String comments = "";

	private boolean available;
	
	private Date availableFrom;
		
	@NotNull
	private CommentEto userComment;

	public CommentedMediaTo() {
	}

	public long getMediaId() {
		return mediaId;
	}

	public void setMediaId(long mediaId) {
		this.mediaId = mediaId;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public PlaygroundMediaType getMediaType() {
		return mediaType;
	}

	public void setMediaType(PlaygroundMediaType mediaType) {
		this.mediaType = mediaType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public CommentEto getUserComment() {
		return userComment;
	}

	public void setUserComment(CommentEto userComment) {
		this.userComment = userComment;
	}

	public Date getAvailableFrom() {
		return availableFrom;
	}

	public void setAvailableFrom(Date availableFrom) {
		this.availableFrom = availableFrom;
	}

	@Override
	public String toString() {
		return "CommentedMediaTo [mediaId=" + mediaId + ", version=" + version + ", mediaType=" + mediaType + ", title="
				+ title + ", comments=" + comments + ", available=" + available + ", availableFrom=" + availableFrom
				+ ", userComment=" + userComment + "]";
	}
	
}