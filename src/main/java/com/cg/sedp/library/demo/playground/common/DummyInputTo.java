package com.cg.sedp.library.demo.playground.common;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

public class DummyInputTo implements Serializable {

	private static final long serialVersionUID = 8582535377041717121L;

	@NotNull
	private Date callTimestamp;

	protected DummyInputTo() {
		// needed for JSON
	}

	public DummyInputTo(@NotNull Date callTimestamp) {
		this.callTimestamp = callTimestamp;
	}

	public Date getCallTimestamp() {
		return callTimestamp;
	}

	@Override
	public String toString() {
		return "DummyInputTo [callTimestamp=" + callTimestamp + "]";
	}


}