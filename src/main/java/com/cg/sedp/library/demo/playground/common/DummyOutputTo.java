package com.cg.sedp.library.demo.playground.common;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

public class DummyOutputTo implements Serializable {

	private static final long serialVersionUID = -1939247632780868329L;

	@NotNull
	private String message;

	protected DummyOutputTo() {
		// needed for JSON
	}

	public DummyOutputTo(@NotNull String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public String toString() {
		return "DummyOutputTo [message=" + message + "]";
	}

}
