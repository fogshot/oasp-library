package com.cg.sedp.library.demo.playground.common;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.constraints.NotNull;

public class PingInputTo implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
	
	@NotNull
	private String callerId;
	
	@NotNull
	private Date callTimestamp;
	
	protected PingInputTo() {
		// needed for JSON
	}
	
	public PingInputTo(@NotNull String callerId) {
		this.callerId = callerId;
		this.callTimestamp = new Date();
	}

	public String getCallerId() {
		return callerId;
	}

	public Date getCallTimestamp() {
		return callTimestamp;
	}

	public String getCallTimestampFormatted() {
		return dateFormat.format(getCallTimestamp());
	}

	@Override
	public String toString() {
		return "PingInputTo [callerId=" + callerId + ", callTimestamp=" + getCallTimestampFormatted() + "]";
	}
	
}
