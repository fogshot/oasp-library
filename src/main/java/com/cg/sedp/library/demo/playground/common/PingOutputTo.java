package com.cg.sedp.library.demo.playground.common;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.constraints.NotNull;

public class PingOutputTo implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");

	@NotNull
	private String callerId;

	@NotNull
	private Date callTimestamp;

	@NotNull
	private Date responseTimestamp;

	@NotNull
	private String message;

	protected PingOutputTo() {
		// needed for JSON
	}
	
	public PingOutputTo(@NotNull String message, @NotNull PingInputTo inputTo) {
		this.callerId = inputTo.getCallerId();
		this.callTimestamp = inputTo.getCallTimestamp();
		this.message = message;
		this.responseTimestamp = new Date();
	}

	public String getCallerId() {
		return callerId;
	}

	public Date getCallTimestamp() {
		return callTimestamp;
	}

	public Date getResponseTimestamp() {
		return responseTimestamp;
	}

	public String getMessage() {
		return message;
	}

	public String getCallTimestampFormatted() {
		return dateFormat.format(getCallTimestamp());
	}

	public String getResponseTimestampFormatted() {
		return dateFormat.format(getResponseTimestamp());
	}

	@Override
	public String toString() {
		return "PingOutputTo [callerId=" + callerId + ", callTimestamp=" + getCallTimestampFormatted()
				+ ", responseTimestamp=" + getResponseTimestampFormatted() + ", message=" + message + "]";
	}

}
