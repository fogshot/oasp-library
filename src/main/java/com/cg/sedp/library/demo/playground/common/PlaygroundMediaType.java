package com.cg.sedp.library.demo.playground.common;

public enum PlaygroundMediaType {

	Book,
	DVD,
	CD;
	
}
