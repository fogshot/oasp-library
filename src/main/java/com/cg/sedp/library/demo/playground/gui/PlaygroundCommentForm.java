package com.cg.sedp.library.demo.playground.gui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cg.sedp.library.demo.playground.common.CommentEto;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

public class PlaygroundCommentForm extends FormLayout {

	private static final long serialVersionUID = 1L;

	private final Logger logger = LoggerFactory.getLogger(PlaygroundCommentForm.class);

	private PlaygroundView playgroundView;

	private Button save = new Button("Save", this::save);
	private Button cancel = new Button("Cancel", this::cancel);
	private Button delete = new Button("Delete", this::delete);
	private TextField comment = new TextField("Comment");

	private CommentEto commentEto;

	private BeanFieldGroup<CommentEto> formFieldBindings;

	public PlaygroundCommentForm(PlaygroundView playgroundView) {
		this.playgroundView = playgroundView;
		configureWidgets();
		buildLayout();
	}

	private void configureWidgets() {
		save.setStyleName(ValoTheme.BUTTON_PRIMARY);
		save.setClickShortcut(ShortcutAction.KeyCode.ENTER);
		setVisible(false);
	}

	private void buildLayout() {
		setSizeUndefined();
		setMargin(true);
		HorizontalLayout actions = new HorizontalLayout(save, delete, cancel);
		actions.setSpacing(true);
		addComponents(actions, comment);
	}

	public void save(Button.ClickEvent event) {
		try {
			formFieldBindings.commit();
			playgroundView.saveComment(commentEto);
			Notification.show(String.format("Saved %s comment", commentEto.getUserId().getUserIdString()), Notification.Type.TRAY_NOTIFICATION);
			playgroundView.refreshMedia();
		} catch (FieldGroup.CommitException e) {
			Notification.show("Please fix validation errors first", Notification.Type.ERROR_MESSAGE);
			logger.error("error commiting " + commentEto, e);
		}
	}

	public void delete(Button.ClickEvent event) {
		try {
			formFieldBindings.commit();
			playgroundView.deleteComment(commentEto);
			Notification.show(String.format("Deleted %s comment", commentEto.getUserId().getUserIdString()), Notification.Type.TRAY_NOTIFICATION);
			playgroundView.refreshMedia();
		} catch (FieldGroup.CommitException e) {
			Notification.show("Please fix validation errors first", Notification.Type.ERROR_MESSAGE);
			logger.error("error deleting " + commentEto, e);
		}
	}

	public void cancel(Button.ClickEvent event) {
		Notification.show("Cancelled", Notification.Type.TRAY_NOTIFICATION);
		playgroundView.cancelComment();
	}

	public void edit(CommentEto commentEto) {
		this.commentEto = commentEto;
		if (this.commentEto != null) {
			formFieldBindings = BeanFieldGroup.bindFieldsBuffered(this.commentEto, this);
			formFieldBindings.setEnabled(true);
			comment.focus();
		}
		setVisible(this.commentEto != null);
	}

}