package com.cg.sedp.library.demo.playground.gui;

import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.client.RestTemplate;

import com.cg.sedp.library.ccc.user.common.UserException;
import com.cg.sedp.library.ccc.user.service.api.UserInformationService;
import com.cg.sedp.library.common.tools.BooleanHtmlCheckboxConverter;
import com.cg.sedp.library.demo.playground.common.CommentEto;
import com.cg.sedp.library.demo.playground.common.CommentedMediaTo;
import com.cg.sedp.library.demo.playground.common.PingInputTo;
import com.cg.sedp.library.demo.playground.common.PingOutputTo;
import com.cg.sedp.library.demo.playground.service.api.MediaCrudService;
import com.cg.sedp.library.demo.playground.service.impl.PingRestServiceImpl;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.HtmlRenderer;

@ViewScope
@SpringView(name = PlaygroundView.VIEW_NAME)
@ConfigurationProperties(prefix="server")
public class PlaygroundView extends VerticalLayout implements View {

	public static final String VIEW_NAME = "playground-view";

	private static final long serialVersionUID = 1L;

	private TextField restUrl = new TextField("URL");
	private Button restCallButton = new Button("Ping", this::onCallRestButton);
	private TextArea restResult = new TextArea("Ping Results");

	private TextField mediaTitleFilter = new TextField();
	private Grid mediaList = new Grid();
	private PlaygroundCommentForm commentForm;

	private static final Pattern HTTP_URL_PATTERN = Pattern
			.compile("^(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");

	private String serverPort;

	@Autowired
	private UserInformationService userInformationService;

	@Autowired
	private MediaCrudService mediaCrudService;

	@PostConstruct
	private void init() {
		configureWidgets();
		addComponent(buildLayout());
		refreshMedia();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// the view is constructed in init() method
	}

	public void refreshMedia() {
		refreshMedia(mediaTitleFilter.getValue());
	}

	public void saveComment(CommentEto commentEto) {
		mediaCrudService.saveComment(commentEto);
	}

	public void deleteComment(CommentEto commentEto) {
		mediaCrudService.deleteComment(commentEto);
	}

	public void cancelComment() {
		commentForm.setVisible(false);
	}

	public void setPort(String serverPort) {
		this.serverPort = serverPort;
	}

	private void configureWidgets() {
		// configure widgets to do rest example with
		restUrl.setInputPrompt("REST URL of library PING service");
		restUrl.addValidator(value -> validateRestUrlInput(value));
		restUrl.addTextChangeListener(event -> restUrlHasChanged());
		restUrl.addValueChangeListener(event -> restUrlHasChanged());
		restUrl.setValue("http://localhost:" + serverPort + PingRestServiceImpl.PING_URL);

		// configure widgets to do CRUD example with
		mediaTitleFilter.setInputPrompt("Filter media by title ...");
		mediaTitleFilter.addTextChangeListener(e -> refreshMedia(e.getText()));

		mediaList.setContainerDataSource(new BeanItemContainer<>(CommentedMediaTo.class));
		mediaList.setColumnOrder("mediaId", "title", "mediaType", "comments", "available", "availableFrom");
		mediaList.removeColumn("version");
		mediaList.removeColumn("userComment");
		mediaList.getColumn("mediaId").setHeaderCaption("ID");
		mediaList.getColumn("title").setHeaderCaption("Title");
		mediaList.getColumn("mediaType").setHeaderCaption("Type");
		mediaList.getColumn("comments").setHeaderCaption("Comments");
		mediaList.getColumn("available").setRenderer(new HtmlRenderer(), new BooleanHtmlCheckboxConverter());
		mediaList.getColumn("available").setHeaderCaption("Available");
		mediaList.getColumn("availableFrom").setHeaderCaption("Available From");
		mediaList.setSelectionMode(Grid.SelectionMode.SINGLE);
		mediaList.addSelectionListener(e -> {
			if (null == mediaList.getSelectedRow()) return;
			CommentedMediaTo commentedMediaTo = (CommentedMediaTo) mediaList.getSelectedRow();
			commentForm.edit(commentedMediaTo.getUserComment());
		});
		commentForm = new PlaygroundCommentForm(this);

		// setting for this view
		setMargin(true);
		setSpacing(true);
	}

	private VerticalLayout buildLayout() {
		VerticalLayout returnValue = new VerticalLayout();
		Label label;
		returnValue.addComponent(label = new Label("<b>Library Playground</b>"));
		label.setContentMode(ContentMode.HTML);

		// render area to make rest calls with
		returnValue.addComponent(new Label());
		returnValue.addComponent(label = new Label("<i>Example for REST calls</i>"));
		label.setContentMode(ContentMode.HTML);
		VerticalLayout restLayout = new VerticalLayout(restUrl, restCallButton, restResult);
		restLayout.setSpacing(true);
		restUrl.setSizeFull();
		restCallButton.setSizeFull();
		restResult.setSizeFull();
		returnValue.addComponent(restLayout);

		// render area with example CRUD view
		returnValue.addComponent(new Label());
		returnValue.addComponent(label = new Label("<i>Example for CRUD</i>"));
		label.setContentMode(ContentMode.HTML);
		HorizontalLayout filterAndNewButton = new HorizontalLayout(mediaTitleFilter);
		filterAndNewButton.setWidth("100%");
		mediaTitleFilter.setWidth("100%");
		filterAndNewButton.setExpandRatio(mediaTitleFilter, 1);
		VerticalLayout filterAndNewButtonAndList = new VerticalLayout(filterAndNewButton, mediaList);
		filterAndNewButtonAndList.setSizeFull();
		mediaList.setSizeFull();
		filterAndNewButtonAndList.setExpandRatio(mediaList, 1);
		HorizontalLayout mediaLayout = new HorizontalLayout(filterAndNewButtonAndList, commentForm);
		mediaLayout.setSizeFull();
		mediaLayout.setExpandRatio(filterAndNewButtonAndList, 1);
		returnValue.addComponent(mediaLayout);

		return returnValue;
	}

	private void refreshMedia(String mediaTitlePrefixFilterString) {
		mediaList.setContainerDataSource(new BeanItemContainer<>(CommentedMediaTo.class,
				mediaCrudService.getMedia(mediaTitlePrefixFilterString)));
		commentForm.setVisible(false);
	}

	private void restUrlHasChanged() {
		restCallButton.setEnabled(restUrl.isValid());
	}

	private void validateRestUrlInput(Object input) throws InvalidValueException {
		if (null == input || !(input instanceof String))
			return;
		if (!HTTP_URL_PATTERN.matcher((String) input).matches())
			throw new InvalidValueException("Not a valid HTTP URL!");
	}

	private void onCallRestButton(Button.ClickEvent event) {
		try {
			restUrl.commit();
			PingOutputTo pingOutput = callPing(restUrl.getValue());
			restResult.setValue((restResult.getValue().length() == 0) ? pingOutput.toString()
					: (restResult.getValue() + "\n" + pingOutput));
		} catch (InvalidValueException e) {
			Notification.show("Please fix validation errors first", Type.ERROR_MESSAGE);
		}
	}

	private PingOutputTo callPing(String url) {
		PingInputTo pingInput;
		try {
			pingInput = new PingInputTo(userInformationService.getUser().getUsername());
			return new RestTemplate().postForObject(url, pingInput, PingOutputTo.class);
		} catch (UserException e) {
			e.printStackTrace();
			// TODO [pvonnieb]: handle user exception
			return null;
		}
	}

}