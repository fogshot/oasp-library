package com.cg.sedp.library.demo.playground.guimvc;

import java.util.Collection;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.cg.sedp.library.ccc.user.common.UserException;
import com.cg.sedp.library.ccc.user.service.api.UserInformationService;
import com.cg.sedp.library.demo.playground.common.CommentEto;
import com.cg.sedp.library.demo.playground.common.CommentedMediaTo;
import com.cg.sedp.library.demo.playground.common.PingInputTo;
import com.cg.sedp.library.demo.playground.common.PingOutputTo;
import com.cg.sedp.library.demo.playground.service.api.MediaCrudService;
import com.vaadin.spring.annotation.ViewScope;


@Component
@ViewScope
public class PlaygroundMvcController {
	private final static Logger LOGGER=Logger.getAnonymousLogger();

	private static final Pattern HTTP_URL_PATTERN = Pattern
			.compile("^(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");

	@Autowired
	private UserInformationService userInformationService;

	@Autowired
	private MediaCrudService mediaCrudService;
	@Autowired
	private PlaygroundMvcModel model;


	// ####################################################
	// ##
	// ## getter/setter
	// ##


	public PlaygroundMvcModel getModel() {
		return model;
	}


	// ####################################################
	// ##
	// ## ping action handling
	// ##
	
	public void onCallRestButton() {
		LOGGER.info("onCallRestButton()");
		{
			String u=getModel().getRestUrl();
			PingOutputTo pingOutput = callPing(u!=null ? u : "");
			getModel().setPingOutput(pingOutput);
		}
	}

	private PingOutputTo callPing(String url) {
		PingInputTo pingInput;
		try {
			pingInput = new PingInputTo(userInformationService.getUser().getUsername());
			return new RestTemplate().postForObject(url, pingInput, PingOutputTo.class);
		} catch (UserException e) {
			// TODO Auto-generated catch block
			// TODO [pvonnieb] handle exception and return something sensible or continue throwing
			e.printStackTrace();
			return null;
		}
	}

	// ##
	// ####################################################
	// ##
	
	public boolean isValidateRestUrlInput(Object input) {
		if (null == input || !(input instanceof String))
			return true;
		if (!HTTP_URL_PATTERN.matcher((String) input).matches())
			return false;
		// accept
		return true;
	}

	// ##
	// ####################################################
	// ##
	// ## item action handling
	// ##
	
	/**
	 * default method to refresh media.
	 * 
	 * the refreshment will be done by searching items using search filter criteria.
	 */
	public void refreshMedia() {
		refreshMedia(getModel().getSearchFilter());
	}

	public void refreshMedia(String mediaTitlePrefixFilterString) {
		LOGGER.info("refresh items ("+mediaTitlePrefixFilterString+")");
		if (mediaTitlePrefixFilterString==null) mediaTitlePrefixFilterString="";
		
		// load data from service
		Collection<CommentedMediaTo> items = mediaCrudService.getMedia(mediaTitlePrefixFilterString);
		getModel().setItems(items);
	}

	public void saveComment(CommentEto commentEto) {
		mediaCrudService.saveComment(commentEto);
	}

	public void deleteComment(CommentEto commentEto) {
		mediaCrudService.deleteComment(commentEto);
	}

}
