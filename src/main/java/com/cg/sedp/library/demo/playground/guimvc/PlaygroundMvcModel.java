package com.cg.sedp.library.demo.playground.guimvc;

import java.util.Collection;
import java.util.Observable;
import java.util.logging.Logger;

import com.cg.sedp.library.demo.playground.common.CommentEto;
import com.cg.sedp.library.demo.playground.common.CommentedMediaTo;
import com.cg.sedp.library.demo.playground.common.PingInputTo;
import com.cg.sedp.library.demo.playground.common.PingOutputTo;

import org.springframework.stereotype.Component;
import com.vaadin.spring.annotation.ViewScope;


@Component
@ViewScope
public class PlaygroundMvcModel extends Observable {
	private final static Logger LOGGER=Logger.getAnonymousLogger();
	
	/** enum values signalizes on observer pattern, which field has changed. */
	enum FieldChange {
		FIELD_PING_URL,
		FIELD_PING_INPUT,
		FIELD_PING_OUTPUT,
		
		FIELD_SEARCH_FILTER,
		FIELD_COMMENTS,
		FIELD_ITEMS,
		FIELD_SELECTED_ITEM,
	};


	private PingInputTo pingInput;
	private PingOutputTo pingOutput;
	private String restUrl;

	private String searchFilter="";
	private Collection<CommentedMediaTo> availableItems;
	private Collection<CommentEto> comments;

	private CommentedMediaTo selectedItems;


	// ####################################################
	// ##
	// ## ping getter/setter
	// ##

	public PingInputTo getPingInput() {
		return pingInput;
	}

	public void setPingInput(PingInputTo pingInput) {
		this.pingInput = pingInput;
		setChangedNotifyObserver(FieldChange.FIELD_PING_INPUT);
	}

	public PingOutputTo getPingOutput() {
		return pingOutput;
	}

	public void setPingOutput(PingOutputTo pingOutput) {
		this.pingOutput = pingOutput;
		setChangedNotifyObserver(FieldChange.FIELD_PING_OUTPUT);
	}
	
	public String getRestUrl() {
		return restUrl;
	}

	public void setRestUrl(String restUrl) {
		this.restUrl = restUrl;
		setChangedNotifyObserver(FieldChange.FIELD_PING_URL);
	}

	// ####################################################
	// ##
	// ## item getter/setter
	// ##
	
	public String getSearchFilter() {
		return searchFilter;
	}

	public void setSearchFilter(String searchFilter) {
		this.searchFilter = searchFilter;
		setChangedNotifyObserver(FieldChange.FIELD_SEARCH_FILTER);
	}

	public Collection<CommentEto> getComments() {
		return comments;
	}

	public void setComments(Collection<CommentEto> comments) {
		this.comments = comments;
		setChangedNotifyObserver(FieldChange.FIELD_COMMENTS);
	}

	public Collection<CommentedMediaTo> getItems() {
		return availableItems;
	}

	public void setItems(Collection<CommentedMediaTo> items) {
		this.availableItems = items;
		setChangedNotifyObserver(FieldChange.FIELD_ITEMS);
	}

	public CommentedMediaTo getSelectedItems() {
		return selectedItems;
	}

	public void setSelectedItems(CommentedMediaTo selectedItems) {
		this.selectedItems = selectedItems;
		setChangedNotifyObserver(FieldChange.FIELD_SELECTED_ITEM);
	}

	// ####################################################
	// ##
	// ## observer notification
	// ##

	protected void setChangedNotifyObserver(FieldChange changedFieldLabel) {
		LOGGER.info("changed :: "+changedFieldLabel);
		setChanged();
		notifyObservers(changedFieldLabel);
	}
}
