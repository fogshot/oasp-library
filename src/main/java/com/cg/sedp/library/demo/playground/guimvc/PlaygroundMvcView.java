package com.cg.sedp.library.demo.playground.guimvc;

import java.util.Collection;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;

import com.cg.sedp.library.common.tools.BooleanHtmlCheckboxConverter;
import com.cg.sedp.library.demo.playground.common.CommentEto;
import com.cg.sedp.library.demo.playground.common.CommentedMediaTo;
import com.cg.sedp.library.demo.playground.common.PingOutputTo;
import com.cg.sedp.library.demo.playground.service.impl.PingRestServiceImpl;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.renderers.HtmlRenderer;

/**
 * this view is the entry point to component's main view.
 * 
 * this entry point initializes the controller of MVC, 
 * which creates the required model and real ui/view.
 *
 */
@ViewScope
@SpringView(name = PlaygroundMvcView.VIEW_NAME)
@ConfigurationProperties(prefix="server")
public class PlaygroundMvcView extends VerticalLayout implements View, Observer {
	final static Logger LOGGER=Logger.getAnonymousLogger();

	public static final String VIEW_NAME = "playground-view-mvc";

	private static final long serialVersionUID = 1L;

	String serverPort;

	// model to be synchronized with view
	private PlaygroundMvcModel model;
	// controller to use for actions
	@Autowired
	private PlaygroundMvcController controller;

	// widgets "ping"
	protected TextField restUrl;
	protected Button restCallButton;
	protected TextArea restResult;
	// widgets "item"
	protected TextField searchFilter;
	protected Grid mediaList;
	protected CommentForm commentForm;
	
	// form field mapper
	private BeanFieldGroup<PlaygroundMvcModel> formFieldBindings;

	
	@PostConstruct
	protected void init() {
		// get model from controller
		this.model=controller.getModel();
		// register as observer
		model.addObserver(this);
		
		// configure UI widgets, wire events to controller methods
		initWidgets();
		addComponent(buildLayoutPingWidgets());	
		addComponent(buildLayoutItemWidgets());	
		configureWidgets();
		initBindings();

		getCommentForm().setPlaygroundController(controller);

		// set up data binding between UI widgets and model
		formFieldBindings = BeanFieldGroup.bindFieldsUnbuffered(model, this);
		formFieldBindings.setEnabled(true);

		getModel().setRestUrl("http://localhost:" + serverPort + PingRestServiceImpl.PING_URL);
		getController().refreshMedia();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// the view is constructed in init() method
	}

	public void setPort(String serverPort) {
		this.serverPort = serverPort;
	}

	
	// ####################################################
	// ##
	// ## getter/setter
	// ##


	public PlaygroundMvcModel getModel() {
		return model;
	}

	public PlaygroundMvcController getController() {
		return controller;
	}

	public TextField getRestUrl() {
		return restUrl;
	}

	public Button getRestCallButton() {
		return restCallButton;
	}

	public TextArea getRestResult() {
		return restResult;
	}

	public TextField getSearcheFilter() {
		return searchFilter;
	}

	public Grid getMediaList() {
		return mediaList;
	}

	public CommentForm getCommentForm() {
		return commentForm;
	}



	// ####################################################
	// ##
	// ## widgets
	// ##

	protected void initWidgets() {
		restUrl = new TextField("URL");
		restCallButton = new Button("Ping"); //, this::onCallRestButton);
		restResult = new TextArea("Ping Results");
		
		searchFilter = new TextField();
		mediaList = new Grid();
		commentForm = new CommentForm(null);
	}

	protected VerticalLayout buildLayoutPingWidgets() {
		VerticalLayout returnValue = new VerticalLayout();
		Label label;
		returnValue.addComponent(label = new Label("<b>Library Playground (mvc)</b>"));
		label.setContentMode(ContentMode.HTML);
	
		// render area to make rest calls with
		returnValue.addComponent(new Label());
		returnValue.addComponent(label = new Label("<i>Example for REST calls</i>"));
		label.setContentMode(ContentMode.HTML);
		VerticalLayout restLayout = new VerticalLayout(restUrl, restCallButton, restResult);
		restLayout.setSpacing(true);
		restUrl.setSizeFull();
		restCallButton.setSizeFull();
		restResult.setSizeFull();
		returnValue.addComponent(restLayout);

		return returnValue;
	}

	protected VerticalLayout buildLayoutItemWidgets() {
		VerticalLayout returnValue = new VerticalLayout();
		Label label;
		returnValue.addComponent(label = new Label("<b>Library Playground</b>"));
		label.setContentMode(ContentMode.HTML);
	
		// render area with example CRUD view
		returnValue.addComponent(new Label());
		returnValue.addComponent(label = new Label("<i>Example for CRUD</i>"));
		label.setContentMode(ContentMode.HTML);
		
		HorizontalLayout filterAndNewButton = new HorizontalLayout(searchFilter);
		filterAndNewButton.setWidth("100%");
		searchFilter.setWidth("100%");
		filterAndNewButton.setExpandRatio(searchFilter, 1);
		VerticalLayout filterAndNewButtonAndList = new VerticalLayout(filterAndNewButton, mediaList);
		filterAndNewButtonAndList.setSizeFull();
		mediaList.setSizeFull();
		filterAndNewButtonAndList.setExpandRatio(mediaList, 1);
		HorizontalLayout mediaLayout = new HorizontalLayout(filterAndNewButtonAndList, commentForm);
		mediaLayout.setSizeFull();
		mediaLayout.setExpandRatio(filterAndNewButtonAndList, 1);
		returnValue.addComponent(mediaLayout);
	
		return returnValue;
	}

	protected void configureWidgets() {
		// configure widgets to do rest example with
		restUrl.setInputPrompt("REST URL of library PING service");
		restUrl.setNullRepresentation("");
		
		restResult.setNullRepresentation("");

		
		// configure widgets to do CRUD example with
		searchFilter.setInputPrompt("Filter media by title ...");
		searchFilter.setNullRepresentation("");
	
		mediaList.setContainerDataSource(new BeanItemContainer<>(CommentedMediaTo.class));
		mediaList.setColumnOrder("mediaId", "title", "mediaType", "comments", "available", "availableFrom");
		mediaList.removeColumn("version");
		mediaList.removeColumn("userComment");
		mediaList.getColumn("mediaId").setHeaderCaption("ID");
		mediaList.getColumn("title").setHeaderCaption("Title");
		mediaList.getColumn("mediaType").setHeaderCaption("Type");
		mediaList.getColumn("comments").setHeaderCaption("Comments");
		mediaList.getColumn("available").setRenderer(new HtmlRenderer(), new BooleanHtmlCheckboxConverter());
		mediaList.getColumn("available").setHeaderCaption("Available");
		mediaList.getColumn("availableFrom").setHeaderCaption("Available From");
		mediaList.setSelectionMode(Grid.SelectionMode.SINGLE);
	
		// setting for this view
		setMargin(true);
		setSpacing(true);

	}


	public void initBindings() {
		// init bindings for "ping" functionality
		
		getRestCallButton().addClickListener(this::onCallRestButton);	
		
		getRestUrl().addValidator(value -> this.validateRestUrlInput(value));
		getRestUrl().addTextChangeListener(event -> getModel().setRestUrl(event.getText()));
		getRestUrl().addValueChangeListener(event -> restUrlHasChanged());

		
		// init bindings for "item" functionality
		
		getSearcheFilter().addTextChangeListener(e -> {
			getModel().setSearchFilter(e.getText());
			getController().refreshMedia(e.getText());

			// new data loaded, no comment should be opened anymore
			getCommentForm().setVisible(false);
		});
		
		getMediaList().addSelectionListener(e -> {
			Grid mediaList = (Grid) e.getSource();
			if (null == mediaList.getSelectedRow()) return;
			
			CommentedMediaTo commentedMediaTo = (CommentedMediaTo) mediaList.getSelectedRow();
			this.getCommentForm().edit(commentedMediaTo.getUserComment());
		});
	}


	// ####################################################
	// ##
	// ## actions
	// ##


	
	public void onCallRestButton(Button.ClickEvent event) {
		LOGGER.info("onCallRestButton()");
		try {
			getController().onCallRestButton();

			// clean up button's errors (if available form any previous call of an invalid URL)
			getRestCallButton().setComponentError(null);
		} catch (InvalidValueException e) {
			Notification.show("Please fix validation errors first", Type.ERROR_MESSAGE);
		}
	}

	public void restUrlHasChanged() {
		LOGGER.info("restUrlHasChanged()");
		
		this.getRestUrl().commit();
		
		this.getRestCallButton().setEnabled(this.getRestUrl().isValid());
		getModel().setRestUrl(this.getRestUrl().getValue());
	}

	public void validateRestUrlInput(Object input) throws InvalidValueException {
		if (!getController().isValidateRestUrlInput(input))
			throw new InvalidValueException("Not a valid HTTP URL!");
	}


	// ####################################################
	// ##
	// ## observer
	// ##


	@Override
	public void update(Observable o, Object arg) {
		updateViewFromModel((PlaygroundMvcModel) o, (PlaygroundMvcModel.FieldChange) arg);
	}

	public void updateViewFromModel(PlaygroundMvcModel model, PlaygroundMvcModel.FieldChange changedField) {
		LOGGER.info("update(view) :: "+changedField);
		
		// update "ping" widgets from model
		
		if (changedField==PlaygroundMvcModel.FieldChange.FIELD_PING_OUTPUT) {
			PingOutputTo pingOutput = model.getPingOutput();
			
			restResult.setValue(
					(restResult.getValue().length() == 0) 
					? pingOutput.toString()
					: (restResult.getValue() + "\n" + pingOutput)
					);
		}
		
		
		// update "item" widgets from model
		
		if (changedField==PlaygroundMvcModel.FieldChange.FIELD_SEARCH_FILTER) {
			String filter=model.getSearchFilter();
			LOGGER.info("changed("+changedField+") :: "+filter);
			
			getSearcheFilter().setValue(filter!=null ? filter : "");
			}
		if (changedField==PlaygroundMvcModel.FieldChange.FIELD_ITEMS) {
			Collection<CommentedMediaTo> col = model.getItems();
			LOGGER.info("changed("+changedField+") :: "+col);
			
			getMediaList().setContainerDataSource(new BeanItemContainer<>(CommentedMediaTo.class,
					col));
			getCommentForm().setVisible(false);
		}
		if (changedField==PlaygroundMvcModel.FieldChange.FIELD_COMMENTS) {
			Collection<CommentEto> col = model.getComments();
			LOGGER.info("changed("+changedField+") :: "+col);
			
		}

	}

}