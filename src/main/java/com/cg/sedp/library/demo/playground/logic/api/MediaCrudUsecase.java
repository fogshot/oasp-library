package com.cg.sedp.library.demo.playground.logic.api;

import java.util.Collection;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.cg.sedp.library.demo.playground.common.CommentEto;
import com.cg.sedp.library.demo.playground.common.CommentedMediaTo;

/**
 * CRUD use cases for media objects.
 */
public interface MediaCrudUsecase {

	/**
	 * Get all media (maybe restricted to prefix)
	 * 
	 * @param mediaTitlePrefixFilter
	 *            prefix, if null no filter is applied
	 * @return List of media (maybe empty, but all media objects are valid)
	 */
	@NotNull
	@Valid
	Collection<CommentedMediaTo> getMedia(String mediaTitlePrefixFilter);

	/**
	 * Save or update the comment for given media object and given user
	 * 
	 * @param commentEto object with comment and user to be saved or updated
	 */
	void saveComment(@NotNull @Valid CommentEto commentEto);

	/**
	 * Delete comment for given media object and given user
	 * 
	 * @param commentEto object with comment and user to be deleted
	 */
	void deleteComment(@NotNull @Valid CommentEto commentEto);
	
	

}