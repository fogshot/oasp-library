package com.cg.sedp.library.demo.playground.logic.api;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.cg.sedp.library.demo.playground.common.PingInputTo;
import com.cg.sedp.library.demo.playground.common.PingOutputTo;

/**
 * Ping service to demonstrate data exchange with REST calls.
 */
public interface PingUsecase {

	/**
	 * Ping a server for response
	 * 
	 * @param pingData
	 *            valid information passed in from caller
	 * @return valid ping information for caller
	 */
	@NotNull
	@Valid
	PingOutputTo ping(@NotNull @Valid PingInputTo pingData);

	/**
	 * Check if server is alive
	 * @return Simple alive message
	 */
	@NotNull String alive();
	
}
