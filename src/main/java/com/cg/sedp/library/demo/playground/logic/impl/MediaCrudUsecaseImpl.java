package com.cg.sedp.library.demo.playground.logic.impl;

import java.util.Collection;
import java.util.stream.Collectors;

import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.cg.sedp.library.ccc.user.common.UserException;
import com.cg.sedp.library.ccc.user.common.UserIdType;
import com.cg.sedp.library.ccc.user.service.api.UserInformationService;
import com.cg.sedp.library.demo.playground.common.CommentEto;
import com.cg.sedp.library.demo.playground.common.CommentedMediaTo;
import com.cg.sedp.library.demo.playground.logic.api.MediaCrudUsecase;
import com.cg.sedp.library.demo.playground.persistence.api.CommentRepositoryDao;
import com.cg.sedp.library.demo.playground.persistence.api.MediaRepositoryDao;
import com.cg.sedp.library.demo.playground.persistence.entity.Comment;
import com.cg.sedp.library.demo.playground.persistence.entity.PlaygroundMedia;

@Service
public class MediaCrudUsecaseImpl implements MediaCrudUsecase {

	@Autowired
	private MediaRepositoryDao mediaRepositoryDao;

	@Autowired
	private CommentRepositoryDao commentRepositoryDao;

	@Autowired
	private Validator validator;

	@Autowired
	private UserInformationService userInformationService;

	@Override
	public Collection<CommentedMediaTo> getMedia(String mediaTitlePrefixFilter) {
		return mediaRepositoryDao.findByTitleStartingWithOrderByTitleAsc(mediaTitlePrefixFilter) //
				.stream() //
				.map(this::mapMedia) //
				.collect(Collectors.toList());
	}

	@Override
	public void saveComment(CommentEto commentEto) {
		// get media to comment for
		PlaygroundMedia mediaForComment = mediaRepositoryDao.findOne(commentEto.getMediaId());
		if (null == mediaForComment)
			throw new IllegalStateException("medium to comment for does not exist (comment = " + commentEto + ")");
		Comment comment;
		if (null == commentEto.getId()) {
			// add a new comment
			comment = new Comment();
			comment.setComment(commentEto.getComment());
			comment.setMedia(mediaForComment);
			comment.setUserIdString(commentEto.getUserId().getUserIdString());
			commentRepositoryDao.save(comment);
		} else {
			// overwrite existing comment
			comment = commentRepositoryDao.findOne(commentEto.getId());
			if (null == comment)
				throw new IllegalStateException("comment to overwrite does not exist (comment = " + commentEto + ")");
			comment.setComment(commentEto.getComment());
		}

	}

	@Override
	public void deleteComment(CommentEto commentEto) {
		Comment comment = commentRepositoryDao.findOne(commentEto.getId());
		if (null != comment) {
			commentRepositoryDao.delete(comment);
		}
	}

	private CommentedMediaTo mapMedia(PlaygroundMedia media) {
		CommentedMediaTo commentedMediaTo = new CommentedMediaTo();
		commentedMediaTo.setAvailable(media.getAvailableFrom() == null);
		commentedMediaTo.setAvailableFrom(media.getAvailableFrom());
		commentedMediaTo.setComments(mapComment(media.getUserComments()));
		commentedMediaTo.setMediaId(media.getId());
		commentedMediaTo.setMediaType(media.getMediaType());
		commentedMediaTo.setTitle(media.getTitle());
		try {
			commentedMediaTo.setUserComment(
					getUsersComment(userInformationService.getUser().getUsername(), media.getUserComments(), media.getId()));
		} catch (UserException e) {
			// TODO [pvonnieb]: handle user exception
			e.printStackTrace();
		}
		Assert.state(validator.validate(commentedMediaTo).isEmpty());
		return commentedMediaTo;
	}

	private CommentEto mapComment(Comment comment) {
		CommentEto commentEto = new CommentEto();
		commentEto.setId(comment.getId());
		commentEto.setVersion(comment.getVersion());
		commentEto.setUserId(new UserIdType(comment.getUserIdString()));
		commentEto.setComment(comment.getComment());
		commentEto.setMediaId(comment.getMedia().getId());
		Assert.state(validator.validate(commentEto).isEmpty());
		return commentEto;
	}

	private CommentEto getUsersComment(String userId, Collection<Comment> userComments, long mediaId) {
		return userComments.stream()//
				.filter(c -> c.getUserIdString().equals(userId)) //
				.map(this::mapComment) //
				.findFirst() //
				.orElse(new CommentEto(new UserIdType(userId), "", mediaId));
	}

	private String mapComment(Collection<Comment> userComments) {
		return userComments//
				.stream() //
				.map(uc -> uc.getUserIdString() + ": " + uc.getComment()) //
				.collect(Collectors.joining("\n", "", ""));
	}

}