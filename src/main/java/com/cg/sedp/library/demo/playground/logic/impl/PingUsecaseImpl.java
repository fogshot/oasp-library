package com.cg.sedp.library.demo.playground.logic.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Service;

import com.cg.sedp.library.demo.playground.common.PingInputTo;
import com.cg.sedp.library.demo.playground.common.PingOutputTo;
import com.cg.sedp.library.demo.playground.logic.api.PingUsecase;
import com.cg.sedp.library.demo.playground.service.api.PingService;

@Service
public class PingUsecaseImpl implements PingUsecase {
	
	private SimpleDateFormat sdf = new SimpleDateFormat();
	
	@Override
	public @NotNull @Valid PingOutputTo ping(PingInputTo pingData) {
		return new PingOutputTo(PingService.SERVICE_ALIVE, pingData);
	}

	@Override
	public @NotNull String alive() {
		return PingService.SERVICE_ALIVE + " " + sdf.format(new Date());
	}

}