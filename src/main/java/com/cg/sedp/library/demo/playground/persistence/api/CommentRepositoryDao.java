package com.cg.sedp.library.demo.playground.persistence.api;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cg.sedp.library.demo.playground.persistence.entity.Comment;

/**
 * JPA repository handling comments.
 */
public interface CommentRepositoryDao extends JpaRepository<Comment, Long> {

}
