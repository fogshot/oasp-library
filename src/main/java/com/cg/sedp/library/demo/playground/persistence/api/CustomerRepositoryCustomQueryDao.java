package com.cg.sedp.library.demo.playground.persistence.api;

import java.util.List;

import com.cg.sedp.library.demo.playground.persistence.entity.Customer;

/**
 * Only needed if boilerplate code is to be written for self made custom queries.
 * 
 * Do only implement custom queries if you have to - in 90% of all
 * cases spring data declarative way to do it is sufficient and spares you from
 * boring boilerplate query implementation work, time that is much better invested
 * in testing
 */
public interface CustomerRepositoryCustomQueryDao {

	List<Customer> findByLastnameCustomQuery(String lastname);

}