package com.cg.sedp.library.demo.playground.persistence.api;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.cg.sedp.library.demo.playground.persistence.entity.Customer;

public interface CustomerRepositoryDao extends CrudRepository<Customer, Long> {

	List<Customer> findByLastname(String lastname);

}