package com.cg.sedp.library.demo.playground.persistence.api;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cg.sedp.library.demo.playground.persistence.entity.PlaygroundMedia;

/**
 * JPA repository handling comments.
 */
public interface MediaRepositoryDao extends JpaRepository<PlaygroundMedia, Long> {

	/**
	 * Find all media starting with a given prefix.
	 * 
	 * @param titlePrefix
	 *            Non-null, maybe empty, prefix a titlePrefix of a medium should
	 *            start with.
	 * @return List (maybe empty) of all media whose title starts with a given
	 *         prefix
	 */
	@NotNull
	List<PlaygroundMedia> findByTitleStartingWithOrderByTitleAsc(@NotNull String titlePrefix);

	@Query("select m from PlaygroundMedia m where m.title = :title")
	PlaygroundMedia findByTitle(@Param("title") String title);

}
