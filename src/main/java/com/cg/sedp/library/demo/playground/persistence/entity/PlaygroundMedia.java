package com.cg.sedp.library.demo.playground.persistence.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cg.sedp.library.demo.playground.common.PlaygroundMediaType;

@Entity
public class PlaygroundMedia {

	@Id
	private long id;

	@Version
	private long version;
	
	@NotNull
	private PlaygroundMediaType mediaType;

	@NotNull
	@Size(min=0, max=256)
	private String title;

	private Date availableFrom;
	
	@OneToMany(mappedBy="media", fetch=FetchType.EAGER)
	private Collection<Comment> userComments = new ArrayList<>();
		
	public PlaygroundMedia() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public PlaygroundMediaType getMediaType() {
		return mediaType;
	}

	public void setMediaType(PlaygroundMediaType mediaType) {
		this.mediaType = mediaType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getAvailableFrom() {
		return availableFrom;
	}

	public void setAvailableFrom(Date availableFrom) {
		this.availableFrom = availableFrom;
	}	

	public Collection<Comment> getUserComments() {
		return userComments;
	}

	@Override
	public String toString() {
		return "Media [id=" + id + ", version=" + version + ", mediaType=" + mediaType + ", title=" + title
				+ ", availableFrom=" + availableFrom + "]";
	}
		
}