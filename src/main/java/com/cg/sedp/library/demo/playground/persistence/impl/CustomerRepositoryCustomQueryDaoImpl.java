package com.cg.sedp.library.demo.playground.persistence.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.cg.sedp.library.demo.playground.persistence.api.CustomerRepositoryCustomQueryDao;
import com.cg.sedp.library.demo.playground.persistence.entity.Customer;

@Repository
public class CustomerRepositoryCustomQueryDaoImpl implements CustomerRepositoryCustomQueryDao {

	protected EntityManager entityManager;

	public EntityManager getEntityManager() {
		return entityManager;
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Customer> findByLastnameCustomQuery(String lastname) {
		Query query = getEntityManager().createQuery("select customer from Customer customer where customer.lastname like :lastnamePlaceholder");
		query.setParameter("lastnamePlaceholder", lastname);
		return query.getResultList();
	}

}
