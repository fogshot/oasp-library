package com.cg.sedp.library.demo.playground.service.api;

import java.util.Collection;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.cg.sedp.library.demo.playground.common.CommentEto;
import com.cg.sedp.library.demo.playground.common.CommentedMediaTo;

/**
 * CRUD services for media objects.
 */
public interface MediaCrudService {

	/**
	 * Get all media (maybe restricted to nme with prefix)
	 * 
	 * @param mediaTitlePrefixFilter
	 *            prefix, if null no filter is applied
	 * @return List of media (maye empty, but all media objects are valid)
	 */
	@NotNull
	@Valid
	Collection<CommentedMediaTo> getMedia(@NotNull String mediaTitlePrefixFilter);

	/**
	 * Save or update the comment for given media object and given user
	 * 
	 * @param commentEto object with comment and user to be saved or updated
	 */
	void saveComment(@NotNull @Valid CommentEto commentEto);

	/**
	 * Delete comment for given media object and given user
	 * 
	 * @param commentEto object with comment and user to be deleted
	 */
	void deleteComment(@NotNull @Valid CommentEto commentEto);

}