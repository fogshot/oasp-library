package com.cg.sedp.library.demo.playground.service.api;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.cg.sedp.library.demo.playground.common.PingInputTo;
import com.cg.sedp.library.demo.playground.common.PingOutputTo;

/**
 * Ping use case: creating a response from input.
 */
public interface PingService {
	
	static final String SERVICE_ALIVE = "alive";

	/**
	 * Ping a server for response
	 * @param pingData valid information passed in from caller
	 * @return detailed valid ping information for caller
	 */
	@NotNull
	@Valid
	PingOutputTo ping(@NotNull @Valid PingInputTo pingData);

	/**
	 * Ping a server for response to check it is alive.
	 * 
	 * @return timestamp starting with SERVICE_ALIVE ack message
	 */
	@NotNull
	String alive();

}
