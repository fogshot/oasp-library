package com.cg.sedp.library.demo.playground.service.impl;

import java.util.Collection;

import javax.validation.Valid;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.cg.sedp.library.demo.playground.common.CommentEto;
import com.cg.sedp.library.demo.playground.common.CommentedMediaTo;
import com.cg.sedp.library.demo.playground.logic.api.MediaCrudUsecase;
import com.cg.sedp.library.demo.playground.service.api.MediaCrudService;

@Service
public class MediaCrudServiceImpl implements MediaCrudService {

	@Autowired
	private MediaCrudUsecase mediaCrudUsecase;

	@Autowired
	private Validator validator;

	private final Logger logger = LoggerFactory.getLogger(MediaCrudServiceImpl.class);

	@Override
	public Collection<CommentedMediaTo> getMedia(String mediaTitlePrefixFilter) {
		logger.info("getMedia(mediaTitlePrefixFilter): " + mediaTitlePrefixFilter);
		Assert.notNull(mediaTitlePrefixFilter);
		Collection<CommentedMediaTo> returnValue = mediaCrudUsecase.getMedia(mediaTitlePrefixFilter);
		logger.info("getMedia(mediaTitlePrefixFilter) return: " + returnValue);
		return returnValue;
	}

	@Override
	@Transactional
	public void saveComment(@NotNull @Valid CommentEto commentEto) {
		logger.info("saveComment(commentEto): " + commentEto);
		Assert.notNull(commentEto);
		Assert.state(validator.validate(commentEto).isEmpty());
		mediaCrudUsecase.saveComment(commentEto);
		logger.info("saveComment(commentEto)");
	}

	@Override
	@Transactional
	public void deleteComment(@NotNull @Valid CommentEto commentEto) {
		logger.info("deleteComment(commentEto): " + commentEto);
		Assert.notNull(commentEto);
		Assert.state(validator.validate(commentEto).isEmpty());
		mediaCrudUsecase.deleteComment(commentEto);
		logger.info("deleteComment(commentEto)");
	}

}