package com.cg.sedp.library.demo.playground.service.impl;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cg.sedp.library.common.tools.ValidationChecker;
import com.cg.sedp.library.demo.playground.common.PingInputTo;
import com.cg.sedp.library.demo.playground.common.PingOutputTo;
import com.cg.sedp.library.demo.playground.logic.api.PingUsecase;
import com.cg.sedp.library.demo.playground.service.api.PingService;

@RestController
public class PingRestServiceImpl implements PingService {

	private static final String BASE_URL = "/api/v1/playground";
	public static final String PING_URL = BASE_URL + "/ping";
	public static final String PING_ALIVE_URL = BASE_URL + "/pingalive";

	private final Logger logger = LoggerFactory.getLogger(PingRestServiceImpl.class);

	@Autowired
	private PingUsecase usecase;

	@RequestMapping(path = PING_URL, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public @NotNull @Valid PingOutputTo ping(@RequestBody @NotNull @Valid PingInputTo pingData) {
		logger.info("ping(): " + pingData);
		ValidationChecker.validateNotNullObject(pingData);
		PingOutputTo returnValue = usecase.ping(pingData);
		ValidationChecker.validateNotNullObject(returnValue);
		logger.info("ping() return: " + returnValue);
		return returnValue;
	}

	@RequestMapping(path = PING_ALIVE_URL, method = RequestMethod.GET)
	@Override
	public @NotNull String alive() {
		logger.info("alive()");
		String returnValue = usecase.alive();
		ValidationChecker.validateNotNullObject(returnValue);
		logger.info("alive() return");
		return returnValue;
	}

}