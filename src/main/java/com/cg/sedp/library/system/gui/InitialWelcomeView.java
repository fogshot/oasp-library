package com.cg.sedp.library.system.gui;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.cg.sedp.library.ccc.user.common.UserEto;
import com.cg.sedp.library.ccc.user.common.UserException;
import com.cg.sedp.library.ccc.user.service.api.UserInformationService;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

/**
 * Initial welcome view when starting the application.
 */
@ViewScope
@SpringView(name = InitialWelcomeView.VIEW_NAME)
public class InitialWelcomeView extends VerticalLayout implements View {

	private static final long serialVersionUID = 1L;

	// empty string as this is default login
	// (do not change as this view is to be navigable in the beginning)
	public static final String VIEW_NAME = "";

	@Autowired
	private UserInformationService UserInformationService;

	@PostConstruct
	void init() {
		setMargin(true);
		setSpacing(true);
		addComponent(buildLayout());
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// the view is constructed in init() method
	}

	private VerticalLayout buildLayout() {
		Label welcome = new Label("Welcome " + getLoggedInUserName() + " to the Library!");
		return new VerticalLayout(//
				welcome, //
				new Label(), //
				new Label("Be quiet and enjoy your stay!"));
	}

	private String getLoggedInUserName() {
		UserEto loggedInUser;
		try {
			loggedInUser = UserInformationService.getUser();
			return loggedInUser.getFullname();
		} catch (UserException e) {
			e.printStackTrace();
			return "hacker using this system without login";
		}
	}
}