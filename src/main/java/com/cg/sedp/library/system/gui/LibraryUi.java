package com.cg.sedp.library.system.gui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.vaadin.spring.events.EventBus;
import org.vaadin.spring.events.annotation.EventBusListenerMethod;
import org.vaadin.spring.security.VaadinSecurity;
import org.vaadin.spring.security.util.SecurityExceptionUtils;
import org.vaadin.spring.security.util.SuccessfulLoginEvent;

import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.server.DefaultErrorHandler;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;

/**
 * Portal integrating all views of the application following the Single Page
 * Application (SPA) paradigm.
 * </p>
 * Depending on the login state it will branch or open the login or main screen of the application.
 */
@Title("SEDP Library Application")
@Theme("valo")
@SpringUI
@Push
public class LibraryUi extends UI {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ApplicationContext applicationContext;

	@Autowired
	private VaadinSecurity vaadinSecurity;

	@Autowired
	private EventBus.SessionEventBus eventBus;

	@Override
	protected void init(VaadinRequest request) {

		setErrorHandler(new DefaultErrorHandler() {

			private static final long serialVersionUID = 1L;

			@Override
			public void error(com.vaadin.server.ErrorEvent event) {
				if (SecurityExceptionUtils.isAccessDeniedException(event.getThrowable())) {
					Notification.show("Sorry, you don't have access rights to do that.");
				} else {
					super.error(event);
				}
			}
		});
		if (vaadinSecurity.isAuthenticated()) {
			showMainScreen();
		} else {
			showLoginScreen();
		}

	}

	private void showLoginScreen() {
		setContent(applicationContext.getBean(LoginScreen.class));
	}

	private void showMainScreen() {
		setContent(applicationContext.getBean(MainScreen.class));
	}

	@Override
	public void attach() {
		super.attach();
		eventBus.subscribe(this);
	}

	@Override
	public void detach() {
		eventBus.unsubscribe(this);
		super.detach();
	}

	@EventBusListenerMethod
	private void onLogin(SuccessfulLoginEvent loginEvent) {
		if (loginEvent.getSource().equals(this)) {
			access(new Runnable() {
				@Override
				public void run() {
					showMainScreen();
				}
			});
		} else {
			// login events from other UIs but this will reload this page
			// initializing it again
			getPage().reload();
		}
	}

	public void logout() {
		access(new Runnable() {
			@Override
			public void run() {
				vaadinSecurity.logout();
				showLoginScreen();
			}
		});
	}
}