package com.cg.sedp.library.system.gui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.vaadin.spring.annotation.PrototypeScope;
import org.vaadin.spring.events.EventBus;
import org.vaadin.spring.security.VaadinSecurity;
import org.vaadin.spring.security.util.SuccessfulLoginEvent;

import com.vaadin.event.ShortcutAction;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

/**
 * Full-screen UI component that allows the user to login.
 * </p>
 * Successful login is propagated using vaadin eventbus ({@link EventBus}).
 * Login is done using vaadin managed security (@link {@link VaadinSecurity}).
 */
@PrototypeScope
@SpringComponent
public class LoginScreen extends CustomComponent {

	private static final long serialVersionUID = 1L;

	private final Logger logger = LoggerFactory.getLogger(LoginScreen.class);

	@Autowired
	private VaadinSecurity vaadinSecurity;

	@Autowired
	private EventBus.SessionEventBus eventBus;

	private TextField userName;

	private PasswordField passwordField;

	private Button login;

	private Label loginFailedLabel;

	public LoginScreen() {
		initLayout();
	}

	private void initLayout() {
		FormLayout loginForm = new FormLayout();
		loginForm.setSizeUndefined();

		loginForm.addComponent(new Label("Login To Library Application"));
		loginForm.addComponent(userName = new TextField("Username"));
		userName.focus();
		loginForm.addComponent(passwordField = new PasswordField("Password"));
		loginForm.addComponent(login = new Button("Login"));
		login.addStyleName(ValoTheme.BUTTON_PRIMARY);
		login.setDisableOnClick(true);
		login.setClickShortcut(ShortcutAction.KeyCode.ENTER);
		login.addClickListener(new Button.ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(Button.ClickEvent event) {
				login();
			}
		});

		VerticalLayout loginLayout = new VerticalLayout();
		loginLayout.setSizeUndefined();

		loginLayout.addComponent(loginForm);
		loginLayout.setComponentAlignment(loginForm, Alignment.TOP_CENTER);

		loginLayout.addComponent(loginFailedLabel = new Label());
		loginLayout.setComponentAlignment(loginFailedLabel, Alignment.BOTTOM_CENTER);
		loginFailedLabel.setSizeUndefined();
		loginFailedLabel.addStyleName(ValoTheme.LABEL_FAILURE);
		loginFailedLabel.setVisible(false);

		VerticalLayout rootLayout = new VerticalLayout(loginLayout);
		rootLayout.setSizeFull();
		rootLayout.setComponentAlignment(loginLayout, Alignment.MIDDLE_CENTER);
		setCompositionRoot(rootLayout);
		setSizeFull();
	}

	private void login() {
		try {
			final Authentication authentication = vaadinSecurity.login(userName.getValue(), passwordField.getValue());
			loginFailedLabel.setVisible(false);
			eventBus.publish(this, new SuccessfulLoginEvent(getUI(), authentication));
			String loginMessage = "Successfully logged in with principal " + authentication.getName();
			logger.info(loginMessage);
		} catch (AuthenticationException ex) {
			userName.focus();
			userName.selectAll();
			passwordField.setValue("");
			String loginFailedMessage = String.format("Login failed: %s", ex.getMessage());
			loginFailedLabel.setValue(loginFailedMessage);
			loginFailedLabel.setVisible(true);
		} catch (Exception ex) {
			Notification.show("An unexpected error occurred", ex.getMessage(), Notification.Type.ERROR_MESSAGE);
			logger.error("Unexpected error while logging in", ex);
		} finally {
			login.setEnabled(true);
		}
	}

}
