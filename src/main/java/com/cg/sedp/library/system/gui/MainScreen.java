package com.cg.sedp.library.system.gui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.cg.sedp.library.branch.borrow.gui.BorrowView;
import com.cg.sedp.library.branch.catalog.gui.CatalogMgmtView;
import com.cg.sedp.library.branch.reservation.gui.ReservationView;
import com.cg.sedp.library.ccc.monitoring.gui.MonitoringView;
import com.cg.sedp.library.ccc.user.gui.UsersView;
import com.cg.sedp.library.crossbranch.remote.gui.ManageBranchesView;
import com.cg.sedp.library.crossbranch.virtual.gui.LibraryBrowserView;
import com.cg.sedp.library.demo.dummy.gui.DummyView;
import com.cg.sedp.library.demo.playground.gui.PlaygroundView;
import com.cg.sedp.library.demo.playground.guimvc.PlaygroundMvcView;
import com.cg.sedp.library.system.security.Authorities;
import com.cg.sedp.library.system.security.AuthorityManager;
import com.vaadin.navigator.Navigator;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

/**
 * Main screen of Library: a portal that application screens get integrated
 * into, a basic toolbar with buttons to navigate to applications sub views
 * with.
 */
@UIScope
@SpringComponent
public class MainScreen extends CustomComponent {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ApplicationContext applicationContext;

	@Autowired
	public MainScreen(SpringViewProvider springViewProvider, AuthorityManager authorityManager) {

		// basic layout container
		final VerticalLayout root = new VerticalLayout();
		root.setSizeFull();
		root.setMargin(true);
		root.setSpacing(true);
		setCompositionRoot(root);

		// titlebar layout container
		HorizontalLayout titlebar = new HorizontalLayout();
		titlebar.addStyleName("titlebar");
		titlebar.setWidth("100%");
		Label title = new Label("SEDP Library Application");
		title.addStyleName("title");
		titlebar.addComponent(title);
		titlebar.setComponentAlignment(title, Alignment.MIDDLE_CENTER);
		root.addComponent(titlebar);

		// navigation bar: add a button for every view of the application that
		// is navigable and the user is authorized for
		final CssLayout navigationBar = new CssLayout();
		navigationBar.addStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);

		if (authorityManager.hasAuthority(Authorities.LIBRARY_USER_AUTHORITY))
			navigationBar.addComponent(createNavigationButton("Home", InitialWelcomeView.VIEW_NAME));
		
		if (authorityManager.hasAuthority(Authorities.ADMIN_USER_AUTHORITY)) {
			navigationBar.addComponent(createNavigationButton("Catalog", LibraryBrowserView.VIEW_NAME));
		}
		
		navigationBar.addComponent(createNavigationButton("Borrow", BorrowView.VIEW_NAME));
		navigationBar.addComponent(createNavigationButton("Reservation", ReservationView.VIEW_NAME));
		
		if (authorityManager.hasAuthority(Authorities.ADMIN_USER_AUTHORITY)) {
			navigationBar.addComponent(createNavigationButton("Manage Catalog", CatalogMgmtView.VIEW_NAME));
			navigationBar.addComponent(createNavigationButton("Branches", ManageBranchesView.VIEW_NAME));
			navigationBar.addComponent(createNavigationButton("Users", UsersView.VIEW_NAME));
			navigationBar.addComponent(createNavigationButton("Monitoring", MonitoringView.VIEW_NAME));
		}
		if (authorityManager.hasAuthority(Authorities.DEVELOPER_AUTHORITY)) {
			navigationBar.addComponent(createNavigationButton("Playground", PlaygroundView.VIEW_NAME));
			navigationBar.addComponent(createNavigationButton("Playground-mvc", PlaygroundMvcView.VIEW_NAME));
			navigationBar.addComponent(createNavigationButton("Dummy", DummyView.VIEW_NAME));
		}

		// adding navigation bar + logout button to root
		Button logoutButton = createLogoutButton();
		HorizontalLayout navigationBarAndLogout = new HorizontalLayout(navigationBar, logoutButton);
		navigationBarAndLogout.setWidth("100%");
		navigationBar.setWidth("100%");
		navigationBarAndLogout.setExpandRatio(navigationBar, 1);

		root.addComponent(navigationBarAndLogout);

		// container for all views to render them
		final Panel viewContainer = new Panel();
		viewContainer.setSizeFull();
		root.addComponent(viewContainer);
		root.setExpandRatio(viewContainer, 1.0f);

		// Navigator (spring boot will automatically add all views of the system
		// to springViewProvider)
		Navigator navigator = new Navigator(UI.getCurrent(), viewContainer);
		navigator.addProvider(springViewProvider);
		navigator.navigateTo(navigator.getState());
	}

	private Button createNavigationButton(final String caption, final String viewName) {
		Button button = new Button(caption);
		button.addStyleName(ValoTheme.BUTTON_SMALL);
		button.addClickListener(event -> getUI().getNavigator().navigateTo(viewName));
		return button;
	}

	private Button createLogoutButton() {
		Button button = new Button("Logout");
		button.addStyleName(ValoTheme.BUTTON_SMALL);
		button.addClickListener(event -> {
			applicationContext.getBean(LibraryUi.class).logout();
		});
		return button;
	}
}
