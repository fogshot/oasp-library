package com.cg.sedp.library.system.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * Roles for the library application.
 */
public final class Authorities {

	/** role user of a library: can browse, reserve and borrow media */
	public static final GrantedAuthority LIBRARY_USER_AUTHORITY = new SimpleGrantedAuthority("ROLE_LIBRARYUSER");

	/** role admin of a library: can administer media catalog, users and monitor the system */
	public static final GrantedAuthority ADMIN_USER_AUTHORITY = new SimpleGrantedAuthority("ROLE_ADMINUSER");

	/** developer role: needed to access dialogs for development purpose like playground */
	public static final GrantedAuthority DEVELOPER_AUTHORITY = new SimpleGrantedAuthority("ROLE_DEVELOPER");
	
}
