package com.cg.sedp.library.system.security;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.vaadin.spring.security.VaadinSecurity;

import com.cg.sedp.library.AuthenticationManagerConfig;
import com.cg.sedp.library.ccc.user.common.UserEto;
import com.vaadin.spring.annotation.SpringComponent;

/**
 * Component fetch the credentials with to see who is logged in and what role he has been granted.
 */
@SpringComponent
public class AuthorityManager {

	@Autowired
	private VaadinSecurity vaadinSecurity;
	
	/**
	 * Check if logged in user is granted a given role
	 * @param grantedAuthority role to be checked if user has it (from {@link Authorities}
	 * @return true if logged in user is granted authority grantedAuthority, false otherwise
	 */
	public boolean hasAuthority(GrantedAuthority grantedAuthority) {
		return vaadinSecurity.hasAuthority(grantedAuthority.getAuthority());
	}
	
	/**
	 * Get the id of the logged in user.
	 * <p>
	 * Set at login from {@link AuthenticationManagerConfig} taking it
	 * from {@link UserEto#getUserId()}
	 * 
	 * @return id of logged in user 
	 */
	public @NotNull String getUsername() {
		return vaadinSecurity.getAuthentication().getName();
	}
	
}
