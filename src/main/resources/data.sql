---------------------------------------
--
--  Component: Playground
--

insert into PLAYGROUND_MEDIA (id, version, available_from, media_type, title) values (0, 0, '2016-12-24', 0, 'Bible');
insert into PLAYGROUND_MEDIA (id, version, available_from, media_type, title) values (1, 0, '2016-12-12', 1, 'Rocky');
insert into PLAYGROUND_MEDIA (id, version, available_from, media_type, title) values (2, 0, '2016-12-18', 2, 'Rocky Soundtrack');

insert into COMMENT (id, version, media_id, user_id_string, comment) values (0, 0, 0, 'su', 'Best Book EVER!');
insert into COMMENT (id, version, media_id, user_id_string, comment) values (1, 0, 0, 'alice', 'I dont believe in god');

---------------------------------------
--
--  Component: Borrow
--

insert into BORROW_ENTRY (id, media_item_identifier, return_by, extension_counter, extended_up_to, borrowed_by) values (0, '12001', '2017-09-08', 0, null, 'jbauer');
insert into BORROW_ENTRY (id, media_item_identifier, return_by, extension_counter, extended_up_to, borrowed_by) values (1, '12004', '2017-08-29', 0, null, 'jbauer');
insert into BORROW_ENTRY (id, media_item_identifier, return_by, extension_counter, extended_up_to, borrowed_by) values (2, '12002', '2017-09-08', 0, null, 'jbauer');
insert into BORROW_ENTRY (id, media_item_identifier, return_by, extension_counter, extended_up_to, borrowed_by) values (3, '12003', '2017-09-01', 0, null, 'jbauer');
insert into BORROW_ENTRY (id, media_item_identifier, return_by, extension_counter, extended_up_to, borrowed_by) values (4, '1', '2017-09-01', 0, null, 'su');

---------------------------------------
--
--  Component: Catalog
--

insert into media_item  (author,media_item_identifier,media_item_type, title,version) values ('Friedrich Nietzsche', '12001', 0,  'Why I Am so Clever',0);
insert into media_item  (author,media_item_identifier,media_item_type, title,version) values ('Warner Home Video', '12002', 1,  'Game of Thrones: Die komplette 7. Staffel',1);
insert into media_item  (author,media_item_identifier,media_item_type, title,version) values ('Various', '12003', 2,  'Bravo Hits,Vol.98',2);
insert into media_item  (author,media_item_identifier,media_item_type, title,version) values ('Walt Disney', '12004', 3,  'Guardians of the Galaxy Vol. 2',3);

---------------------------------------
--
--  Component: Reservation
--

insert into RESERVATION (id, version, user_id, media_item_identifier, reserved_up_to) values (0, 0, 'jbauer', '12001', '2016-12-24');
insert into RESERVATION (id, version, user_id, media_item_identifier, reserved_up_to) values (1, 0, 'jbauer', '12002', '2016-11-24');
insert into RESERVATION (id, version, user_id, media_item_identifier, reserved_up_to) values (2, 0, 'cbrian', '12003', '2016-11-24');
insert into RESERVATION (id, version, user_id, media_item_identifier, reserved_up_to) values (3, 0, 'cbrian', '12004', '2016-11-24');


---------------------------------------
--
--  Component: User
--

insert into USER (ID, VERSION, FIRST_NAME, LAST_NAME, LOGIN, PASSWORD, ADMIN_USER_RIGHT, DEVELOPER_USER_RIGHT, NORMAL_USER_RIGHT) values (0, 0, 'Super', 'User', 'su', 'su', true, false, false);
insert into USER (ID, VERSION, FIRST_NAME, LAST_NAME, LOGIN, PASSWORD, ADMIN_USER_RIGHT, DEVELOPER_USER_RIGHT, NORMAL_USER_RIGHT) values (1, 0, 'Jack', 'Bauer', 'jbauer', '24', false, false, false);
insert into USER (ID, VERSION, FIRST_NAME, LAST_NAME, LOGIN, PASSWORD, ADMIN_USER_RIGHT, DEVELOPER_USER_RIGHT, NORMAL_USER_RIGHT) values (2, 0, 'Chloe', 'OBrian', 'cbrian', 'password123' ,true, false, false);

--	private long id;
--
--	@Version
--	private long version;
--
--	private String firstName;
--
--	private String lastName;
--
--	private String login;
--
--	private String password;


---------------------------------------
--
--  Component: Monitoring
--

---------------------------------------
--
--  Component: Branch
--
insert into BRANCH (id, version, name, webservice_url) values (0, 0, 'München', 'http://localhost:8088');
insert into BRANCH (id, version, name, webservice_url) values (1, 0, 'Hamburg', 'http://localhost:8089');
insert into BRANCH (id, version, name, webservice_url) values (2, 0, 'Berlin', 'www.library2.com');

