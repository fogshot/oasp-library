package com.cg.sedp.library;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.cg.sedp.library.ccc.monitoring.MonitoringTests;
import com.cg.sedp.library.ccc.user.UserTests;
import com.cg.sedp.library.crossbranch.CrossbranchTests;
import com.cg.sedp.library.demo.playground.PlaygroundTests;
import com.cg.sedp.library.branch.catalog.CatalogTests;
import com.cg.sedp.library.branch.reservation.ReservationTests;
import com.cg.sedp.library.branch.borrow.BorrowTests;

/**
 * Suite that contains all test suites of all application components.
 * 
 * Run this suite from continuous integration after building the application, it
 * should run locally from every developer before committing changes to the
 * local development branch.
 * 
 * When adding a new component add the components test suite to this.
 *
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ //
		LibraryApplicationSmokeTest.class, //
		PlaygroundTests.class, //
		UserTests.class, //
		MonitoringTests.class, //
		CatalogTests.class, //
		BorrowTests.class, //
		ReservationTests.class, //
		CrossbranchTests.class, //
//
})
public class LibraryAllTests {

}
