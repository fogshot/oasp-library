package com.cg.sedp.library;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Smoketest: application starts and basic functionality is working.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@WebAppConfiguration
public class LibraryApplicationSmokeTest {

	@Test
	public void contextLoads() {
	}

}