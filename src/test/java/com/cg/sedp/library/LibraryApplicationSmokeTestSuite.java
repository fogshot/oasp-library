package com.cg.sedp.library;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.cg.sedp.library.demo.playground.persistence.api.MediaRepositoryDaoUnitTest;
import com.cg.sedp.library.demo.playground.service.api.PingServiceRestIntegrationTest;

/**
 * Simple smoketest suite that should be run from every developer
 * before committing changes to remote repository development branch.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ //
		LibraryApplicationSmokeTest.class, //
		MediaRepositoryDaoUnitTest.class, //
		PingServiceRestIntegrationTest.class })
public class LibraryApplicationSmokeTestSuite {

}
