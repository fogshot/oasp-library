package com.cg.sedp.library.branch.borrow.logic;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.branch.borrow.common.BorrowedItemEto;
import com.cg.sedp.library.branch.borrow.logic.api.BorrowedMedia;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.ccc.user.common.UserIdType;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
	TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DatabaseSetup(BorrowedMediaTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { BorrowedMediaTest.DATASET })
@DirtiesContext
@WebAppConfiguration
public class BorrowedMediaTest {

	@Autowired
	private BorrowedMedia borrowedMedia;
	
	public static final String DATASET = "classpath:datasets/borrowedItems-testdata.xml";
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");


	@Test
	public void testInit() {
		Assert.assertThat(borrowedMedia, is(notNullValue()));
	}
	
	@Test
	public void testGetAllMediaIds() {
		
		//when
		List<BorrowedItemEto> borrowedItems = borrowedMedia.getAllBorrowedMediaIDs();
		
		//then
		Assert.assertThat(borrowedItems, is(notNullValue()));
		Assert.assertThat(borrowedItems, hasSize(3));
		BorrowedItemEto borrowedItem = borrowedItems.get(0);
		Assert.assertThat(borrowedItem.getMediaItemIdentifier(), is(new MediaItemIdentifier("120013")));
		Assert.assertThat(borrowedItem.getBorrowedBy(), is(new UserIdType("aa")));
	}
	
	@Test
	public void testGetAllMediaIdsForUser() {
		
		//when
		List<BorrowedItemEto> borrowedItems = borrowedMedia.getBorrowedMediaIDs(new UserIdType("bb"));
		
		//then
		Assert.assertThat(borrowedItems, is(notNullValue()));
		Assert.assertThat(borrowedItems, hasSize(2));
		Assert.assertThat(borrowedItems.get(0).getMediaItemIdentifier(), is(new MediaItemIdentifier("120015")));
		Assert.assertThat(borrowedItems.get(1).getMediaItemIdentifier(), is(new MediaItemIdentifier("120016")));		
	}
	
	@Test
	public void testIsBorrowed() {
		
		//when
		boolean isBorrowed = borrowedMedia.isBorrowed(new MediaItemIdentifier("120015"));
		
		//then
		Assert.assertThat(isBorrowed, is(true));		
	}

	@Test
	public void testGetNumberOfBorrowedMedia() {
		
		//when
		int numberOfBorrowedMedia = borrowedMedia.getNumberOfBorrowedMedia(new UserIdType("bb"));
		
		//then
		Assert.assertThat(numberOfBorrowedMedia, is(2));
	}
	
}
