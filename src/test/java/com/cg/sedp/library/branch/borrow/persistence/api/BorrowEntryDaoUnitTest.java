package com.cg.sedp.library.branch.borrow.persistence.api;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.branch.borrow.persistence.entity.BorrowEntry;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.ccc.user.common.UserIdType;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;


@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DatabaseSetup(BorrowEntryDaoUnitTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = {BorrowEntryDaoUnitTest.DATASET })
@DirtiesContext
@WebAppConfiguration
public class BorrowEntryDaoUnitTest {


    public static final String DATASET = "classpath:datasets/borrowedItems-testdata.xml";
    
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    private BorrowEntryRepositoryDao borrowEntryRepositoryDao;

    @SuppressWarnings("deprecation")
	@Test
    public void findBorrowEntriesByBorrowedBy() throws ParseException {
        // when
        List <BorrowEntry> borrowEntryList = borrowEntryRepositoryDao.findByBorrowedBy(new UserIdType("aa"));
        assertThat(borrowEntryList, is(notNullValue()));
        assertThat(borrowEntryList, hasSize(1));
        BorrowEntry borrowEntry = borrowEntryList.get(0);
  
        
        // then
        
        assertThat(borrowEntry, is(notNullValue()));
        assertThat(borrowEntry.getId(), is(0l));
        assertThat(borrowEntry.getMediaItemIdentifier().toString(), is("120013"));
        assertThat(borrowEntry.getBorrowedBy().getUserIdString(), is("aa"));
        assertThat(dateFormat.format(borrowEntry.getReturnBy()), is("2000-09-28"));
        assertThat(dateFormat.format(borrowEntry.getExtendedUpTo()), is("2000-10-28"));
        assertThat(borrowEntry.getExtensionCounter(), is(1));
    }
    
    @Test
    public void findBorrowEntrybyMediaItemIdentifier() {
    
    	//when
    	BorrowEntry borrowEntry = borrowEntryRepositoryDao.findByMediaItemIdentifier(new MediaItemIdentifier("120013"));
    	
    	// then
        
        assertThat(borrowEntry, is(notNullValue()));
        assertThat(borrowEntry.getId(), is(0l));
        assertThat(borrowEntry.getBorrowedBy().getUserIdString(), is("aa"));
        assertThat(borrowEntry.getExtensionCounter(), is(1));
        assertThat(dateFormat.format(borrowEntry.getReturnBy()), is("2000-09-28"));
        assertThat(dateFormat.format(borrowEntry.getExtendedUpTo()), is("2000-10-28"));
        
        //when
    	
    	borrowEntry = borrowEntryRepositoryDao.findByMediaItemIdentifier(new MediaItemIdentifier("120014"));
    	
    	//then
    	
    	assertThat(borrowEntry, is(nullValue()));
    }
    
    @Test
    public void findBorrowEntrybyMediaItemIdentifierAndBorrowedBy() {
    	
    	//when
    	BorrowEntry borrowEntry = borrowEntryRepositoryDao.findByMediaItemIdentifierAndBorrowedBy(new MediaItemIdentifier("120013"), new UserIdType("aa"));
    	
    	// then
        assertThat(borrowEntry, is(notNullValue()));
        
        //when
        borrowEntry = borrowEntryRepositoryDao.findByMediaItemIdentifierAndBorrowedBy(new MediaItemIdentifier("120013"), new UserIdType("bb"));
        
        // then
        assertThat(borrowEntry, is(nullValue()));
    }

}