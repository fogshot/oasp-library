package com.cg.sedp.library.branch.borrow.service;

import static org.hamcrest.Matchers.*;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.branch.borrow.service.api.BorrowExecutionService;
import com.cg.sedp.library.demo.playground.service.api.MediaCrudServiceIntegrationTest;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
	TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DatabaseSetup(MediaCrudServiceIntegrationTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { MediaCrudServiceIntegrationTest.DATASET })
@DirtiesContext
@WebAppConfiguration
public class BorrowExecutionServiceTest {

	@Autowired
	@Qualifier(value="Stub")
	private BorrowExecutionService service;
	
	
	@Test
	public void testInit() {
		Assert.assertThat(service, is(notNullValue()));
	}
}
