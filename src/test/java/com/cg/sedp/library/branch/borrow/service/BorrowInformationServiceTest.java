package com.cg.sedp.library.branch.borrow.service;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.branch.borrow.persistence.api.BorrowEntryDaoUnitTest;
import com.cg.sedp.library.branch.borrow.service.api.BorrowInformationService;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.ccc.user.common.UserIdType;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
	TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DatabaseSetup(BorrowEntryDaoUnitTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { BorrowEntryDaoUnitTest.DATASET })
@DirtiesContext
@WebAppConfiguration
public class BorrowInformationServiceTest {

	@Autowired
	@Qualifier(value="Stub")
	private BorrowInformationService service;
	
	
	@Test
	public void testInit() {
		Assert.assertThat(service, is(notNullValue()));
	}
	
	@Test
	public void testGetBorrowedMediaIds() {
		
		List<MediaItemIdentifier> borrowedMediaIDs = service.getBorrowedMediaIDs(new UserIdType("su"));
		Assert.assertThat(borrowedMediaIDs, is(notNullValue()));
		Assert.assertThat(borrowedMediaIDs, hasSize(1));
	}
}
