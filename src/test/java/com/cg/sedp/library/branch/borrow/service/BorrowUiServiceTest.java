package com.cg.sedp.library.branch.borrow.service;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.branch.borrow.common.BorrowedItemCto;
import com.cg.sedp.library.branch.borrow.service.api.BorrowUiService;
import com.cg.sedp.library.demo.playground.service.api.MediaCrudServiceIntegrationTest;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
	TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DatabaseSetup(MediaCrudServiceIntegrationTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { MediaCrudServiceIntegrationTest.DATASET })
@DirtiesContext
@WebAppConfiguration
public class BorrowUiServiceTest {

	@Autowired
	@Qualifier(value="Stub")
	private BorrowUiService service;
	
	
	@Test
	public void testInit() {
		Assert.assertThat(service, is(notNullValue()));
	}
	
	@Test
	public void testGetAllBorrowedMediaForUser() {
		
		List<BorrowedItemCto> allBorrowedMediaForUser = service.getAllBorrowedMediaForUser(null);
		Assert.assertThat(allBorrowedMediaForUser, is(notNullValue()));
		Assert.assertThat(allBorrowedMediaForUser, hasSize(1));
	}
}
