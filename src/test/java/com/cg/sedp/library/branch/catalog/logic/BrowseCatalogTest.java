package com.cg.sedp.library.branch.catalog.logic;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.branch.catalog.common.MediaItemEto;
import com.cg.sedp.library.branch.catalog.common.MediaItemType;
import com.cg.sedp.library.branch.catalog.common.MediaNotFoundException;
import com.cg.sedp.library.branch.catalog.common.SearchCriteria;
import com.cg.sedp.library.branch.catalog.logic.api.BrowseCatalogUC;
import com.cg.sedp.library.branch.catalog.logic.impl.BrowseCatalogImpl;
import com.github.springtestdbunit.DbUnitTestExecutionListener;

@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
	TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DirtiesContext
@WebAppConfiguration
public class BrowseCatalogTest 

{
	@Autowired
	private BrowseCatalogImpl browseCatalog;
	
	@Test
	public void testInit() {
		Assert.assertThat(browseCatalog, is(notNullValue()));
	}
	
	@Ignore
	@Test
	public void testGetMediaById() throws MediaNotFoundException {
		MediaItemEto t = browseCatalog.getMedia(null);
		Assert.assertThat(t, is(notNullValue()));
	}
	
	@Test
	public void testGetAllMedia() {
		List<MediaItemEto> items = browseCatalog.getAllMedia();
		Assert.assertThat(items, is(notNullValue()));
		Assert.assertThat(items, hasSize(4));
	}
	
	//@Test
	/*public void testGetFilteredMedia() {
		
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setPatternTitle("test");
		searchCriteria.setPatternAuthor("test");
		Set<MediaItemType> mtypes = new HashSet<MediaItemType>();
		mtypes.add(MediaItemType.Book);
		searchCriteria.setSelectedType(mtypes);
		List<MediaItemEto> items = browseCatalog.getFilterdMedia(searchCriteria);	
		Assert.assertThat(items, is(notNullValue()));
		Assert.assertThat(items, hasSize(1));
		*/
	//}

}
