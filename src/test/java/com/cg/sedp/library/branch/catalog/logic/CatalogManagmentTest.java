package com.cg.sedp.library.branch.catalog.logic;

import static org.hamcrest.Matchers.notNullValue;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.is;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.branch.catalog.common.MediaItemEto;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.catalog.common.MediaItemType;
import com.cg.sedp.library.branch.catalog.logic.impl.BrowseCatalogImpl;
import com.cg.sedp.library.branch.catalog.logic.impl.CatalogManagmentImpl;
import com.cg.sedp.library.ccc.user.common.UserEto;
import com.github.springtestdbunit.DbUnitTestExecutionListener;


@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
	TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DirtiesContext
@WebAppConfiguration
public class CatalogManagmentTest 
{
	@Autowired
	private CatalogManagmentImpl catalogmanagment;
	
	@Autowired
	private BrowseCatalogImpl browseCatalog;
	
	
	@Test
	public void testInit() {
		Assert.assertThat(catalogmanagment, is(notNullValue()));
	}
	
	
	@Test
	public void addMediaItem()  
	{
		try
		{
		
		// add media
		MediaItemEto mediaItemEto = new MediaItemEto();
		mediaItemEto.setAuthor("Autor10");
		mediaItemEto.setTitle("Title10");
		mediaItemEto.setMediaItemType(MediaItemType.Book);
		mediaItemEto.setMediaItemIdentifier(new MediaItemIdentifier("12345620"));
		UserEto user=new UserEto();
		user.setAdminUserRight(true);
		catalogmanagment.addMedia(user,mediaItemEto);
		
		MediaItemEto t = browseCatalog.getMedia(new MediaItemIdentifier("12345620"));
		Assert.assertEquals("Autor10", t.getAuthor());
		// delete media
		catalogmanagment.deleteMedia(user,t.getMediaItemIdentifier());
		
		MediaItemEto t2 = browseCatalog.getMedia(new MediaItemIdentifier("12345620"));
		Assert.assertNull(t2);
		
		} 
		catch (Exception e) 
		{
			Assert.assertEquals(e.getClass().getName(),"com.cg.sedp.library.branch.catalog.common.MediaNotFoundException");	
		}
	}
	
	@Test
	public void EditMediaItem()  
	{
		try
		{
		// add media
		MediaItemEto mediaItemEto = new MediaItemEto();
		mediaItemEto.setAuthor("Autor10");
		mediaItemEto.setTitle("Title10");
		mediaItemEto.setMediaItemType(MediaItemType.Book);
		mediaItemEto.setMediaItemIdentifier(new MediaItemIdentifier("12345620"));
		UserEto user=new UserEto();
		user.setAdminUserRight(true);
		catalogmanagment.addMedia(user,mediaItemEto);
		MediaItemEto t = browseCatalog.getMedia(new MediaItemIdentifier("12345620"));
		Assert.assertEquals("Autor10", t.getAuthor());
		// update media		
		t.setAuthor("Autor20");
		catalogmanagment.saveMedia(user,t);
		// delete media
		catalogmanagment.deleteMedia(user,t.getMediaItemIdentifier());
		MediaItemEto t2 = browseCatalog.getMedia(new MediaItemIdentifier("12345620"));
		Assert.assertNull(t2);
		
		} 
		catch (Exception e) 
		{
			Assert.assertEquals(e.getClass().getName(),"com.cg.sedp.library.branch.catalog.common.MediaNotFoundException");	
		}
	}
}
