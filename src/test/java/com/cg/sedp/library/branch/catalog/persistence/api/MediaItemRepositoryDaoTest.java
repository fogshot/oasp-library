package com.cg.sedp.library.branch.catalog.persistence.api;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.catalog.common.MediaItemType;
import com.cg.sedp.library.branch.catalog.common.SearchCriteria;
import com.cg.sedp.library.branch.catalog.persistence.entity.MediaItem;
import com.cg.sedp.library.demo.dummy.persistence.api.AnotherDummyRepositoryDao;
import com.cg.sedp.library.demo.dummy.persistence.api.AnotherDummyRepositoryDaoUnitTest;
import com.cg.sedp.library.demo.dummy.persistence.entity.AnotherDummyEntity;
import com.cg.sedp.library.demo.dummy.persistence.entity.DummyType;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.google.gwt.user.server.rpc.core.java.util.HashMap_ServerCustomFieldSerializer;

@TestExecutionListeners({
    DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DatabaseSetup(MediaItemRepositoryDaoTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { MediaItemRepositoryDaoTest.DATASET })
@DirtiesContext
@WebAppConfiguration
public class MediaItemRepositoryDaoTest {
	
    public static final String DATASET = "classpath:datasets/catalogItems-testdata.xml";

    @Autowired
    private MediaItemRepositoryDao mediaItemRepository;

    @Test
    public void findAnotherEntity() {
        // when
        List<MediaItem> mediaItems = mediaItemRepository.findAll();
        // then
        assertThat(mediaItems, hasSize(5));
    }
	
	@Test
	public void testFindByMediaItemIdentifier() {
		// when
    	MediaItemIdentifier mediaItemIdentifier = new MediaItemIdentifier("id1");
    	MediaItem mediaItem = mediaItemRepository.findByMediaItemIdentifier(mediaItemIdentifier);
    	// then
    	assertThat(mediaItem, is(notNullValue()));
    	assertThat(mediaItem.getAuthor(), is("Author1"));
    	assertThat(mediaItem.getTitle(), is("Book1"));
    	assertThat(mediaItem.getMediaItemType(), is(MediaItemType.Book));
    	assertThat(mediaItem.getMediaItemIdentifier(), is(mediaItemIdentifier));
	}
	
	@Test
	public void testFindAllFilteredByTitle() {
		// when
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setPatternTitle("Book1");
		List<MediaItem> mediaItems = mediaItemRepository.findAllBySearchCriteria(searchCriteria);
		// then
		assertThat(mediaItems, is(notNullValue()));
		assertThat(mediaItems, hasSize(2));
	}
	
	@Test
	public void testFindAllFilteredByMediaItemIdentifier() {
		// when
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setSearchPattern("id2");
		List<MediaItem> mediaItems = mediaItemRepository.findAllBySearchCriteria(searchCriteria);
		// then
		assertThat(mediaItems, is(notNullValue()));
		assertThat(mediaItems, hasSize(1));
	}
	
	@Test
	public void testFindAllFilteredByAuthor() {
		// when
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setPatternAuthor("Author1");
		List<MediaItem> mediaItems = mediaItemRepository.findAllBySearchCriteria(searchCriteria);
		// then
		assertThat(mediaItems, is(notNullValue()));
		assertThat(mediaItems, hasSize(2));
	}
	
	@Test
	public void testFindAllFilteredByType() {
		// when
		SearchCriteria searchCriteria = new SearchCriteria();
		Set<MediaItemType> ids = new HashSet<MediaItemType>();
		ids.add(MediaItemType.Book);
		ids.add(MediaItemType.DVD);
		searchCriteria.setSelectedType(ids);
		List<MediaItem> mediaItems = mediaItemRepository.findAllBySearchCriteria(searchCriteria);
		// then
		assertThat(mediaItems, is(notNullValue()));
		assertThat(mediaItems, hasSize(4));
	}
}
