package com.cg.sedp.library.branch.catalog.persistence.impl;

import static org.junit.Assert.*;

import org.junit.Test;

import com.cg.sedp.library.branch.catalog.common.SearchCriteria;

public class MediaItemRepositoryDaoImplTest {

	private MediaItemRepositoryDaoImpl dao = new MediaItemRepositoryDaoImpl();
	
	@Test
	public void testBuildQueryString() {
		SearchCriteria searchCriteria = new SearchCriteria();
		
		String query = dao.buildQueryString(searchCriteria);
		
		assertTrue(query.toLowerCase().contains(".title"));
		assertTrue(query.toLowerCase().contains(".author"));
		assertTrue(query.toLowerCase().contains(".mediaitemidentifier"));
		assertFalse(query.toLowerCase().contains(".mediaitemtype"));
	}

}
