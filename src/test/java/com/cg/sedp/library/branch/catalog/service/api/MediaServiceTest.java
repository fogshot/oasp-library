package com.cg.sedp.library.branch.catalog.service.api;

import static com.cg.sedp.library.demo.playground.common.CommentedMediaToMatcher.commentedMediaTo;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.branch.catalog.common.MediaItemEto;
import com.cg.sedp.library.branch.catalog.common.MediaItemType;
import com.cg.sedp.library.branch.catalog.common.MediaNotFoundException;
import com.cg.sedp.library.branch.catalog.common.SearchCriteria;
import com.cg.sedp.library.branch.catalog.persistence.api.MediaItemRepositoryDaoTest;
import com.cg.sedp.library.branch.catalog.persistence.entity.MediaItem;
import com.cg.sedp.library.branch.catalog.service.impl.MediaServiceImpl;
import com.cg.sedp.library.demo.playground.common.CommentedMediaTo;
import com.cg.sedp.library.demo.playground.common.PlaygroundMediaType;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.google.web.bindery.requestfactory.shared.Service;

@TestExecutionListeners({
    DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DatabaseSetup(MediaItemRepositoryDaoTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { MediaItemRepositoryDaoTest.DATASET })
@DirtiesContext
@WebAppConfiguration
public class MediaServiceTest {
	
	@Autowired
	private MediaService mediaService;
	
	@Test
	public void testInit() {
		Assert.assertThat(mediaService, is(notNullValue()));
	}
	
	@Test(expected = MediaNotFoundException.class)
	public void testGetMediaById() throws MediaNotFoundException {
		MediaItemEto t = mediaService.getMediaById(null);
		Assert.assertThat(t, is(notNullValue()));
	}
	
	@Test
	public void testGetAllMedia() {
		List<MediaItemEto> items = mediaService.getAllMedia();
		Assert.assertThat(items, is(notNullValue()));
		Assert.assertThat(items, hasSize(5));
	}
	
	@Test
	public void testGetFilteredMedia() {
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setPatternTitle("Book1");
		List<MediaItemEto> items = mediaService.getFilteredMedia(searchCriteria);
		Assert.assertThat(items, is(notNullValue()));
		Assert.assertThat(items, hasSize(2));
	}

}
