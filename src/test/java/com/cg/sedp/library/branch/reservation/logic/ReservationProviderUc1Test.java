package com.cg.sedp.library.branch.reservation.logic;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.reservation.common.ReservedItemEto;
import com.cg.sedp.library.branch.reservation.logic.api.ReservationProviderUc1;
import com.cg.sedp.library.ccc.user.common.UserIdType;
import com.cg.sedp.library.demo.playground.persistence.api.MediaRepositoryDaoUnitTest;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
	TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DatabaseSetup(MediaRepositoryDaoUnitTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { MediaRepositoryDaoUnitTest.DATASET })
@DirtiesContext
@WebAppConfiguration
@Transactional
public class ReservationProviderUc1Test {
	
	@Autowired
	private ReservationProviderUc1 reservationProvider;
	
	@Test
	public void testInit() {
		Assert.assertThat(reservationProvider, notNullValue());
	}
	
	@Test
	public void testGetAllReservation() {
		Assert.assertThat(reservationProvider, notNullValue());
		List<ReservedItemEto> reservations = reservationProvider.getAllReservations();
		Assert.assertThat(reservations, notNullValue());
	}
	
	@Test
	public void testGetReservationOfUser() {
		Assert.assertThat(reservationProvider, notNullValue());
		UserIdType userId = new UserIdType("Test");
		List<ReservedItemEto> reservations = reservationProvider.getReservations(userId);
		Assert.assertThat(reservations, notNullValue());
	}
	
	@Test
	public void testGetReservationOfItem() {
		Assert.assertThat(reservationProvider, notNullValue());
		MediaItemIdentifier mediaId = new MediaItemIdentifier("dumpMedia");
		ReservedItemEto reservation = reservationProvider.getReservationEto(mediaId);
		Assert.assertThat(reservation, notNullValue());
		Assert.assertThat(reservation.getMediaItemIdentifier(), equalTo(mediaId));
	}
	
	@Test
	public void testGetReservationState() {
		Assert.assertThat(reservationProvider, notNullValue());
		boolean isReserved = reservationProvider.isReserved(null);
		Assert.assertTrue(isReserved);
	}
	

}
