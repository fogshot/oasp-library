package com.cg.sedp.library.branch.reservation.logic;

import static org.hamcrest.CoreMatchers.notNullValue;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.branch.reservation.logic.api.ReservationProviderUc2;
import com.cg.sedp.library.demo.playground.persistence.api.MediaRepositoryDaoUnitTest;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
	TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DatabaseSetup(MediaRepositoryDaoUnitTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { MediaRepositoryDaoUnitTest.DATASET })
@DirtiesContext
@WebAppConfiguration
@Transactional
public class ReservationProviderUc2Test {

	@Autowired
	private ReservationProviderUc2 reservationProiderUc2;
	
	@Test
	public void testInit() {
		Assert.assertThat(reservationProiderUc2, notNullValue());
	}
	
	@Test
	public void deleteReservation() {
		Assert.assertThat(reservationProiderUc2, notNullValue());
		boolean isReserved = reservationProiderUc2.deleteReservation(null);
		Assert.assertFalse(isReserved);
		
	}	
	

}
