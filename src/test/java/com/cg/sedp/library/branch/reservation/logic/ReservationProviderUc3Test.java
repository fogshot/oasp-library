package com.cg.sedp.library.branch.reservation.logic;
import static org.hamcrest.CoreMatchers.notNullValue;

import java.sql.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.reservation.logic.api.ReservationProviderUc3;
import com.cg.sedp.library.branch.reservation.logic.impl.ReservationProviderUc3Impl;
import com.cg.sedp.library.branch.reservation.persistence.api.ReservationDao;
import com.cg.sedp.library.branch.reservation.persistence.entity.Reservation;
import com.cg.sedp.library.ccc.user.common.UserIdType;



@RunWith(MockitoJUnitRunner.class)
public class ReservationProviderUc3Test {
	
	
	@InjectMocks
	ReservationProviderUc3 reservationProUC3 = new ReservationProviderUc3Impl();
	
	@Mock
	private ReservationDao dao;
	
	
	@Test
	public void setReservation() {
		Reservation reservation = new Reservation();
		UserIdType u = new UserIdType("User123");
		MediaItemIdentifier m = new MediaItemIdentifier("ident") ;
		Date date = new Date(13456L);
		reservation.setUserId(u);
		reservation.setMediaId(m);
		reservation.setReservedUpTo(date);
		dao.save(reservation);
		Assert.assertThat(reservationProUC3, notNullValue());
	
	}	

}
