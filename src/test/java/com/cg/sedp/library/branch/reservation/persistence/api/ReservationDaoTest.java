
package com.cg.sedp.library.branch.reservation.persistence.api;

import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.reservation.logic.api.ReservationProviderUc3;
import com.cg.sedp.library.branch.reservation.logic.impl.ReservationProviderUc3Impl;
import com.cg.sedp.library.branch.reservation.persistence.entity.Reservation;

import static org.hamcrest.CoreMatchers.notNullValue;

import java.sql.Date;

import javax.transaction.Transactional;

import static org.hamcrest.CoreMatchers.equalTo;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.ccc.user.common.UserIdType;
//import com.cg.sedp.library.demo.playground.persistence.api.MediaRepositoryDaoUnitTest;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
	TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DatabaseSetup(ReservationDaoTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { ReservationDaoTest.DATASET })
@DirtiesContext
@WebAppConfiguration
@Transactional

public class ReservationDaoTest {

	public static final String DATASET = "classpath:datasets/reservedItems-testdata.xml";
	
	@Autowired
	private ReservationDao dao;
	
	@Autowired
	ReservationProviderUc3 reservationProUC3 = new ReservationProviderUc3Impl();
	
	@Test
	public void testInit() {
		Assert.assertThat(dao, notNullValue());
	}

	@Test
	public void testPersist() {
		Reservation reservationEntry=new Reservation();
		UserIdType userId = new UserIdType("mmielenz");
		reservationEntry.setUserId(userId);
		MediaItemIdentifier mediaId = new MediaItemIdentifier("0001");
		reservationEntry.setMediaId(mediaId);
		Date date = new Date(123456L);
		reservationEntry.setReservedUpTo(date);
		

		Reservation reservationEntry2 = dao.save(reservationEntry);
		
		
		

		Reservation reservationEntry3 = dao.getOne(reservationEntry2.getId());
		Reservation reservationEntry4 = dao.getOne(0L);
		Reservation reservationEntry5 = dao.findByMediaId(mediaId);
		Reservation reservationEntry6 = dao.findByUserId(userId);
		
		
		Assert.assertThat(reservationEntry3, notNullValue());
		Assert.assertThat(reservationEntry3.getUserId(), equalTo(userId));
		Assert.assertThat(reservationEntry3.getMediaId(), equalTo(mediaId));
		Assert.assertThat(reservationEntry3.getReservedUpTo(), equalTo(date));
		
		Assert.assertThat(reservationEntry4, notNullValue());
		UserIdType userId2 = new UserIdType("jmielenz");
		
		
		Assert.assertThat(reservationEntry4.getUserId(), equalTo(userId2));
		
		Assert.assertThat(reservationEntry5.getMediaId(), equalTo(mediaId));
		
		Assert.assertThat(reservationEntry6.getUserId(), equalTo(userId));
	}
}