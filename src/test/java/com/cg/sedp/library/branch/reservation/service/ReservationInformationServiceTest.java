package com.cg.sedp.library.branch.reservation.service;

import static org.hamcrest.Matchers.*;

import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.branch.reservation.service.impl.ReservationInformationServiceStub;
import com.cg.sedp.library.demo.playground.persistence.api.MediaRepositoryDaoUnitTest;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;



@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
	TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DatabaseSetup(MediaRepositoryDaoUnitTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { MediaRepositoryDaoUnitTest.DATASET })
@DirtiesContext
@WebAppConfiguration
public class ReservationInformationServiceTest {
	
	@Autowired
	private ReservationInformationServiceStub reservationInformationService;
	
	@Test
	public void testInit() {
		Assert.assertThat(reservationInformationService, notNullValue()); 
	}
	
	@Test
	public void testgetReservations() {
		Assert.assertThat(reservationInformationService.getReservations(null), hasSize(1));
	}
	
	@Test
	public void testgetAllReservations() {
		Assert.assertThat(reservationInformationService.getAllReservations(), hasSize(1));
	}
	
	@Test
	public void testIsReserved(){
	//	Assert.assertThat(reservationInformationService.isReserved(null), );
	}
	
	@Test
	public void testIsUserReserved(){
	//	Assert.assertThat(reservationInformationService.isUserReserved(null), );
	}
}
