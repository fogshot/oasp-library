package com.cg.sedp.library.ccc.monitoring;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Test suite that enumerates all tests of the component monitoring.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({})
public class MonitoringTests {

}
