package com.cg.sedp.library.ccc.user;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.cg.sedp.library.ccc.user.common.datatypes.UserIdTypeUnitTest;
import com.cg.sedp.library.ccc.user.logic.BrowseUserTest;
import com.cg.sedp.library.ccc.user.logic.CRUDOnUserTest;
import com.cg.sedp.library.ccc.user.persistence.api.UserDaoTest;
import com.cg.sedp.library.ccc.user.service.UserCrudServiceTest;
import com.cg.sedp.library.ccc.user.service.UserInformationServiceTest;

/**
 * Test suite that enumerates all tests of the component user.
 * 
 * Every developer should add new tests to this suite.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ //
		UserIdTypeUnitTest.class, BrowseUserTest.class, CRUDOnUserTest.class, UserCrudServiceTest.class, UserDaoTest.class,
		// FIXME [pvonnieb]: reordering these tests may break UserCrudServiceTest!
		UserInformationServiceTest.class })
public class UserTests {

}
