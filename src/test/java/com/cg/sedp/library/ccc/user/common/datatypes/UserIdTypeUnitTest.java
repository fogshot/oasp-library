package com.cg.sedp.library.ccc.user.common.datatypes;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import com.cg.sedp.library.ccc.user.common.UserIdType;
import org.junit.Test;

/**
 * Testing UserId type.
 */
public class UserIdTypeUnitTest {

	private static final String VALID_USER_ID_STRING = "joedoe11";
	private static final String ANOTHER_VALID_USER_ID_STRING = "joedoe12";

	@Test
	public void testPositiveCaseUserId() {
		UserIdType userIdType = new UserIdType(VALID_USER_ID_STRING);
		assertThat(userIdType.getUserIdString(), is(equalTo(VALID_USER_ID_STRING)));
	}	

	@Test
	public void testPositiveCaseUserIdTypeEquals() {
		assertThat(new UserIdType(VALID_USER_ID_STRING), is(equalTo(new UserIdType(VALID_USER_ID_STRING))));
	}	

	@Test
	public void testNegativeCaseUserIdTypeEquals() {
		assertThat(new UserIdType(VALID_USER_ID_STRING), not(equalTo(new UserIdType(ANOTHER_VALID_USER_ID_STRING))));
	}	
	
	@Test(expected=IllegalStateException.class)
	public void testNotNullUserId() {
		new UserIdType(null);
	}

	@Test(expected=IllegalStateException.class)
	public void testUserIdSize0NotValid() {
		new UserIdType("");
	}

	@Test(expected=IllegalStateException.class)
	public void testUserIdSize9NotValid() {
		new UserIdType("123456789");
	}
		
}