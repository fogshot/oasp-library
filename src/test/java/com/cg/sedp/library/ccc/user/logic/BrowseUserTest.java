package com.cg.sedp.library.ccc.user.logic;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.ccc.user.common.UserFilterEto;
import com.cg.sedp.library.ccc.user.common.UserIdType;
import com.cg.sedp.library.ccc.user.logic.api.BrowseUserUseCase;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DatabaseSetup(BrowseUserTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { BrowseUserTest.DATASET })
@DirtiesContext
@WebAppConfiguration
public class BrowseUserTest {

	public static final String DATASET = "classpath:datasets/user-testdata.xml";

	@Autowired
	private BrowseUserUseCase browseUserUseCase;

	@Test
	public void checkBrowseUserUseCaseExists() {
		assertThat(browseUserUseCase, notNullValue());
	}

	@Test
	public void checkBrowseWithEmptyFilter() {
		assertThat(browseUserUseCase.findUsers(null), hasSize(6));
	}

	@Test
	public void checkBrowseWithUserID() {
		UserFilterEto filter = new UserFilterEto(new UserIdType("duser"), "", "");
		assertThat(browseUserUseCase.findUsers(filter), hasSize(1));
	}

	@Test
	public void checkBrowseWithEmptyAttributes() {
		UserFilterEto filter = new UserFilterEto(null, "", "");
		assertThat(browseUserUseCase.findUsers(filter), hasSize(6));
	}

	@Test
	public void checkBrowseWithFirstName() {
		UserFilterEto filter = new UserFilterEto(null, "Joe", "");
		assertThat(browseUserUseCase.findUsers(filter), hasSize(3));
	}

	@Test
	public void checkBrowseWithLastName() {
		UserFilterEto filter = new UserFilterEto(null, "", "Foo");
		assertThat(browseUserUseCase.findUsers(filter), hasSize(1));
	}

	@Test
	public void checkBrowseWithFirstNameAndLastName() {
		UserFilterEto filter = new UserFilterEto(null, "Joe", "Bar");
		assertThat(browseUserUseCase.findUsers(filter), hasSize(1));
	}
}
