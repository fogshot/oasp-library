package com.cg.sedp.library.ccc.user.logic;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.ccc.user.common.UserEto;
import com.cg.sedp.library.ccc.user.common.UserIdType;
import com.cg.sedp.library.ccc.user.logic.api.CRUDOnUserUseCase;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DatabaseSetup(CRUDOnUserTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { CRUDOnUserTest.DATASET })
@DirtiesContext
@WebAppConfiguration
public class CRUDOnUserTest {

	public static final String DATASET = "classpath:datasets/user-testdata.xml";

	@Autowired
	private CRUDOnUserUseCase crudOnUserUseCase;

	@Test
	public void checkCRUDOnUserUseCaseExists() {
		assertThat(crudOnUserUseCase, notNullValue());
	}

	@Test
	public void checkCRUDOnUserUseCaseReturnsOneEntry() {
		try {
			assertThat(crudOnUserUseCase.readUser(new UserIdType("duser")), notNullValue());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void checkCRUDOnUserUseCaseCreatesEntry() {
		try {
			crudOnUserUseCase.readUser(new UserIdType("jvolmer"));
		} catch (Exception e) {
			assertThat(e.getMessage(), is("Leer!"));
		}

		UserEto testUserEto = new UserEto();
		testUserEto.setAdminUserRight(false);
		testUserEto.setDeveloperUserRight(false);
		testUserEto.setNormalUserRight(true);
		testUserEto.setFirstName("Joe");
		testUserEto.setLastName("Volmer");
		testUserEto.setEncryptedPassword("password");
		testUserEto.setUserId("jvolmer");
		testUserEto.setEncryptedPassword("password");

		crudOnUserUseCase.createUser(testUserEto);

		try {
			assertThat(crudOnUserUseCase.readUser(new UserIdType("jvolmer")), notNullValue());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void checkCRUDOnUserUseCaseDeletesEntry() {
		try {
			crudOnUserUseCase.readUser(new UserIdType("dbohlen"));
		} catch (Exception e) {
			assertThat(e.getMessage(), is("Leer!"));
		}

		UserEto testUserEto = new UserEto();
		testUserEto.setAdminUserRight(false);
		testUserEto.setDeveloperUserRight(false);
		testUserEto.setNormalUserRight(true);
		testUserEto.setFirstName("Dieter");
		testUserEto.setLastName("Bohlen");
		testUserEto.setEncryptedPassword("password");
		testUserEto.setUserId("dbohlen");
		testUserEto.setEncryptedPassword("password");

		crudOnUserUseCase.createUser(testUserEto);

		try {
			assertThat(crudOnUserUseCase.readUser(new UserIdType("dbohlen")), notNullValue());
		} catch (Exception e) {
			fail(e.getMessage());
		}

		try {
			crudOnUserUseCase.deleteUser(testUserEto.getUserId());
		} catch (Exception e) {
			fail(e.getMessage());
		}

		try {
			crudOnUserUseCase.readUser(new UserIdType("dbohlen"));
		} catch (Exception e) {
			assertThat(e.getMessage(), is("Leer!"));
		}
	}

	@Test
	public void checkCRUDOnUserUseCaseUpdatesEntry() {
		try {
			crudOnUserUseCase.readUser(new UserIdType("tanders"));
		} catch (Exception e) {
			assertThat(e.getMessage(), is("Leer!"));
		}

		UserEto testUserEto = new UserEto();
		testUserEto.setAdminUserRight(false);
		testUserEto.setDeveloperUserRight(false);
		testUserEto.setNormalUserRight(true);
		testUserEto.setFirstName("Thomas");
		testUserEto.setLastName("Anders");
		testUserEto.setEncryptedPassword("password");
		testUserEto.setUserId("tanders");
		testUserEto.setEncryptedPassword("passwort");

		crudOnUserUseCase.createUser(testUserEto);

		UserEto testUserEto2 = new UserEto();
		testUserEto2.setAdminUserRight(true);
		testUserEto2.setDeveloperUserRight(true);
		testUserEto2.setNormalUserRight(false);
		testUserEto2.setFirstName("Thomas2");
		testUserEto2.setLastName("Anders2");
		testUserEto2.setEncryptedPassword("password2");
		testUserEto2.setUserId("tanders");

		UserEto testUserEto3 = null;

		try {
			crudOnUserUseCase.updateUser(testUserEto2);
			testUserEto3 = crudOnUserUseCase.readUser(new UserIdType("tanders"));
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertThat(testUserEto2.toString(), is(testUserEto3.toString()));
	}
}
