package com.cg.sedp.library.ccc.user.persistence.api;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.ccc.user.persistence.entity.User;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DatabaseSetup(UserDaoTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { UserDaoTest.DATASET })
@DirtiesContext
@WebAppConfiguration
public class UserDaoTest {

	public static final String DATASET = "classpath:datasets/user-testdata.xml";

	@Autowired
	private UserRepositoryDao userRepository;

	@Test
	public void findByLastName() {
		// when
		List<User> customers = userRepository.findByLastName("Doe");
		// then
		assertThat(customers, is(notNullValue()));
		assertThat(customers.size(), is(1));
		User joeDoe = customers.get(0);
		assertThat(joeDoe.getFirstName(), is("Joe"));
		assertThat(joeDoe.getLastName(), is("Doe"));
		assertThat(joeDoe.getLogin(), is("jdoe"));
	}
	
	@Test
	public void findByFirstName() {
		// when
		List<User> customers = userRepository.findByFirstName("Dummy");
		// then
		assertThat(customers, is(notNullValue()));
		assertThat(customers.size(), is(1));
		User dummyUser = customers.get(0);
		assertThat(dummyUser.getFirstName(), is("Dummy"));
		assertThat(dummyUser.getLastName(), is("User"));
		assertThat(dummyUser.getLogin(), is("duser"));
	}
	
	@Test
	public void findByLogin() {
		// when
		List<User> customers = userRepository.findByLogin("duser");
		// then
		assertThat(customers, is(notNullValue()));
		assertThat(customers.size(), is(1));
		User dummyUser = customers.get(0);
		assertThat(dummyUser.getFirstName(), is("Dummy"));
		assertThat(dummyUser.getLastName(), is("User"));
		assertThat(dummyUser.getLogin(), is("duser"));
	}
	
	@Test
	public void findByFirstAndLastName() {
		// when
		List<User> customers = userRepository.findByFirstNameEqualsAndLastNameEquals("Joe","Doe");
		// then
		assertThat(customers, is(notNullValue()));
		assertThat(customers.size(), is(1));
		User dummyUser = customers.get(0);
		assertThat(dummyUser.getFirstName(), is("Joe"));
		assertThat(dummyUser.getLastName(), is("Doe"));
		assertThat(dummyUser.getLogin(), is("jdoe"));
	}
	
	@Test
	public void findByFirstNameWithMultipleResults() {
		// when
		List<User> customers = userRepository.findByFirstName("Joe");
		// then
		assertThat(customers, is(notNullValue()));
		assertThat(customers.size(), is(3));
	}

}
