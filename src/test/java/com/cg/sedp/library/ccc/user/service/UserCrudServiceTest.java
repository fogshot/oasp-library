package com.cg.sedp.library.ccc.user.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.ccc.user.common.UserEto;
import com.cg.sedp.library.ccc.user.common.UserException;
import com.cg.sedp.library.ccc.user.common.UserIdType;
import com.cg.sedp.library.ccc.user.logic.api.CRUDOnUserUseCase;
import com.cg.sedp.library.ccc.user.service.api.UserCrudService;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

/**
 * Tests that tests the service offering CRUD operations on users.
 * 
 * @author pvonnieb
 */
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@WebAppConfiguration
@DatabaseSetup(UserCrudServiceTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { UserCrudServiceTest.DATASET })
public class UserCrudServiceTest {

	public static final String DATASET = "classpath:datasets/user-testdata.xml";

	/** the subject of our test, with mocks injected into it. */
	@InjectMocks
	@Autowired
	private UserCrudService userCrudService;

	/** a mock of the CRUDOnUserUseCase class of the logic layer. */
	@Mock
	private CRUDOnUserUseCase userCrud;

	/** a username of an admin user. Used in most tests. */
	private static String adminUsername;

	/**
	 * Set up the username that will be used throughout these tests.
	 */
	@BeforeClass
	public static void setupTest() {
		adminUsername = "admin";
	}

	/**
	 * Initialize the mock for our UseCase.
	 * @throws Exception 
	 */
	@Before
	public void setupMocks() throws Exception {
		UserEto user = new UserEto();
		user.setUserId(adminUsername);

		MockitoAnnotations.initMocks(this);
		when(userCrud.readUser(eq(new UserIdType(adminUsername)))).thenReturn(user);
	}

	/**
	 * Test if {@link UserCrudService#getUser(UserIdType)} returns a user for a
	 * given UserId.
	 * 
	 * @throws UserException
	 */
	@Test
	public void testGetUser() throws Exception {
		UserIdType userId = new UserIdType(adminUsername);
		UserEto user = userCrudService.getUser(userId);
		assertThat(user, notNullValue());
		assertThat(user.getUserId(), equalTo(userId));
		verify(userCrud).readUser(userId);
	}
}
