package com.cg.sedp.library.crossbranch;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.cg.sedp.library.crossbranch.logic.api.BorrowReserveTests;
import com.cg.sedp.library.crossbranch.logic.api.BrowseCatalogUsecaseTest;
import com.cg.sedp.library.crossbranch.remote.persistence.api.BranchDaoTests;

@RunWith(Suite.class)
@SuiteClasses({ //
	// TODO: Add new TestClasses here
	BrowseCatalogUsecaseTest.class, 
	BorrowReserveTests.class,
	BranchDaoTests.class, //
//	CrossbranchPersistenceTest.class, // still empty
//	CrossbranchServiceTest.class, // still empty
//	LibraryServiceTest.class, // which one? .remote or .service.api?
})
public class CrossbranchTests {

}
