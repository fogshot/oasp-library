package com.cg.sedp.library.crossbranch.logic.api;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.branch.borrow.common.AlreadyBorrowedException;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.ccc.user.common.UserEto;
import com.cg.sedp.library.ccc.user.common.UserException;
import com.cg.sedp.library.ccc.user.service.api.UserInformationService;
import com.cg.sedp.library.crossbranch.local.logic.api.BorrowReserveUsecase;
import com.cg.sedp.library.demo.playground.persistence.api.CustomerDaoUnitTest;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DatabaseSetup(CustomerDaoUnitTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { CustomerDaoUnitTest.DATASET })
@DirtiesContext
@WebAppConfiguration
public class BorrowReserveTests {

	@InjectMocks
	@Autowired
	BorrowReserveUsecase borrowReserveUsecase;

	@Mock
	UserInformationService userService;

	MediaItemIdentifier mediaId;

	@Before
	public void initSetup() {
		MockitoAnnotations.initMocks(this);
		UserEto user = new UserEto();
		user.setUserId("admin");
		try {
			when(userService.getUser()).thenReturn(user);
			mediaId = new MediaItemIdentifier("testMedia");
		} catch (UserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	@Test
	public void BorrowTest() throws AlreadyBorrowedException, UserException {
		borrowReserveUsecase.borrowMedia(mediaId);
		verify(userService).getUser();
	}

	@Test
	public void ReserveTest() throws UserException {
		borrowReserveUsecase.reserveMedia(mediaId);
		verify(userService).getUser();
	}
}
