package com.cg.sedp.library.crossbranch.logic.api;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.branch.catalog.common.SearchCriteria;
import com.cg.sedp.library.crossbranch.common.MediaSearchResultCto;
import com.cg.sedp.library.crossbranch.datatypes.LibraryIdType;
import com.cg.sedp.library.crossbranch.local.logic.api.BrowseCatalogUsecase;
import com.github.springtestdbunit.DbUnitTestExecutionListener;

@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DirtiesContext
@WebAppConfiguration
public class BrowseCatalogUsecaseTest {

    @Autowired
    private BrowseCatalogUsecase browseCatalog;

    LibraryIdType libraryId;
    
    @Before
    public void init() {
    	libraryId = new LibraryIdType("lib0001");
    	// maybe set libraryId = null and implement the usecase/the test such that null means to search in the local library?
    }
    
    @After
    public void cleanup() {
    	libraryId = null;
    }
    
    /**
     * Test the Stub implementation.
     * TODO: This test will fail for a real implementation. It could be modified for a system/integration test once a test-database exists.
     */
   @Test
    public void test_Stub() {
        // when
    	List<MediaSearchResultCto> searchResult = browseCatalog.getMediaSearchResults(null, null);
        // then
        assertThat(searchResult, is(notNullValue()));
        assertThat(searchResult.size(), is(1));
        assertThat(searchResult.get(0).getMedia().getAuthor(), is("Stub Buchautor"));
        assertThat(searchResult.get(0).isReserved(), is(true));
        assertThat(searchResult.get(0).getReservedFor(), is("user0001"));
        assertThat(searchResult.get(0).isBorrowed(), is(false));
    }

    /**
     * Test search results without filter.
     * We expect that returning the complete catalog of {@link #libraryId} is not an empty List.
     */
    @Test
    public void test_NoFilter() {
        // when
    	List<MediaSearchResultCto> searchResult = browseCatalog.getMediaSearchResults(null, libraryId);
        // then
        assertThat(searchResult, is(notNullValue()));
    }

    /**
     * Test search results with insane {@link SearchCriteria}.
     * We expect that the catalog of {@link #libraryId} does not contain any matching item.
     */
    @Test
    public void test_WithInsaneFilter() {
    	// given
    	SearchCriteria filter = new SearchCriteria("in$anep4tt3rn");
    	filter.setPatternTitle("no book is gonna named like this (ever?)");
        // when
    	List<MediaSearchResultCto> searchResult = browseCatalog.getMediaSearchResults(filter, libraryId);
        // then
        assertThat(searchResult.size(), is(0));
    }

    /**
     * Test integrity: each search result which is a reserved item must return a not-null {@link userIdString}.
     */
    @Test
    public void test_CtoIntegrity_Reserved() {
        // when
    	List<MediaSearchResultCto> searchResult = browseCatalog.getMediaSearchResults(null, libraryId);
        // then
    	if (searchResult.size() == 0) { return; }
    	for (Iterator<MediaSearchResultCto> iterator = searchResult.iterator(); iterator.hasNext();) {
			MediaSearchResultCto mediaSearchResultCto = (MediaSearchResultCto) iterator.next();
			if (mediaSearchResultCto.isReserved()) {
				assertThat(mediaSearchResultCto.getReservedFor(), is(notNullValue()));
			}
		}
    }

    /**
     * Test integrity: each search result which is a borrowed item must return a not-null {@link userIdString}.
     */
    @Test
    public void test_CtoIntegrity_Borrowed() {
        // when
    	List<MediaSearchResultCto> searchResult = browseCatalog.getMediaSearchResults(null, libraryId);
        // then
    	if (searchResult.size() == 0) { return; }
    	for (Iterator<MediaSearchResultCto> iterator = searchResult.iterator(); iterator.hasNext();) {
			MediaSearchResultCto mediaSearchResultCto = (MediaSearchResultCto) iterator.next();
			if (mediaSearchResultCto.isBorrowed()) {
				assertThat(mediaSearchResultCto.getBorrowedBy(), is(notNullValue()));
			}
		}
    }

//    /**
//     * Test integrity of {@link MediaSearchResultCto}: if a ReservedItemEto or BorrowedItemEto is attached,
//     * they must have the same {@link MediaItemIdentifier} as the {@link MediaItemEto}.
//     */
//    @Test
//    public void test_CtoIntegrity_Medium() {
//        // when
//    	List<MediaSearchResultCto> searchResult = browseCatalog.getMediaSearchResults(null, libraryId);
//        // then
//    	if (searchResult.size() == 0) { return; }
//    	for (Iterator<MediaSearchResultCto> iterator = searchResult.iterator(); iterator.hasNext();) {
//			MediaSearchResultCto mediaSearchResultCto = (MediaSearchResultCto) iterator.next();
//			if (mediaSearchResultCto.isReserved()) {
//				assertThat(mediaSearchResultCto.getMedia().getMediaItemIdentifier(), equals( thereIsNoGetterForTheReservedItemEto ));
//			}
//			//and the same for borrowed...
//		}
//    }

}
