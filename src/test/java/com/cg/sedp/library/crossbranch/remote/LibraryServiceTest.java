package com.cg.sedp.library.crossbranch.remote;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.crossbranch.remote.persistence.entity.Branch;
import com.cg.sedp.library.crossbranch.remote.service.api.LibraryService;
import com.cg.sedp.library.crossbranch.virtual.common.BranchInfo;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DirtiesContext
@WebAppConfiguration


public class LibraryServiceTest {
	
	@Autowired
	LibraryService libraryService;
	
	@Test
	public void getBranchTest(){
		
		List<BranchInfo> libraryList = libraryService.getBranches();
		
		assertThat(libraryList, is(notNullValue()));
		
	}
	
}
