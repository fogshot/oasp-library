package com.cg.sedp.library.crossbranch.remote.persistence.api;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.crossbranch.remote.persistence.entity.Branch;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DatabaseSetup(BranchDaoTests.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { BranchDaoTests.DATASET })
@DirtiesContext
@WebAppConfiguration

public class BranchDaoTests {
	
	
	public static final String DATASET = "classpath:datasets/branch-testdata.xml";
	
	@Autowired
	public BranchDao branchDao;
	
	@Test
	public void findlibraryTest(){
		Branch library= branchDao.findById("2");
		assertThat(library, notNullValue());
		assertThat(library.getName(), equalTo("Lib2"));
	}
	
	

}
