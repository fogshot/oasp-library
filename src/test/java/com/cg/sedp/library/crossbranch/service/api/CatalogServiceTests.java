package com.cg.sedp.library.crossbranch.service.api;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.branch.borrow.common.AlreadyBorrowedException;
import com.cg.sedp.library.branch.borrow.common.NotBorrowedException;
import com.cg.sedp.library.branch.catalog.common.MediaItemIdentifier;
import com.cg.sedp.library.branch.common.OwnedByDifferentUserException;
import com.cg.sedp.library.branch.reservation.common.AlreadyReservedException;
import com.cg.sedp.library.branch.reservation.common.NotReservedException;
import com.cg.sedp.library.crossbranch.local.service.api.CatalogService;
import com.cg.sedp.library.crossbranch.remote.common.RemoteBranchCommunicationException;
import com.cg.sedp.library.crossbranch.virtual.gui.NoBranchSelectedException;
import com.cg.sedp.library.demo.playground.persistence.api.CustomerDaoUnitTest;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
	TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DatabaseSetup(CustomerDaoUnitTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { CustomerDaoUnitTest.DATASET })
@DirtiesContext
@WebAppConfiguration

public class CatalogServiceTests {
	
	@Autowired
	CatalogService catalogService;
	
	MediaItemIdentifier mediaId;
	
	@Before
	public void initSetup(){
		mediaId = new MediaItemIdentifier("testMedia");
	}
	
	@Test
	public void borrowTest() throws AlreadyBorrowedException, NoBranchSelectedException,
	RemoteBranchCommunicationException, NotBorrowedException, OwnedByDifferentUserException{
		catalogService.borrow(mediaId);
	}
	
	@Test
	public void reserveTest() throws AlreadyReservedException, NoBranchSelectedException,
	RemoteBranchCommunicationException, NotReservedException, OwnedByDifferentUserException{
		catalogService.reserve(mediaId);
	}

}
