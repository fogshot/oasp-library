package com.cg.sedp.library.demo.dummy.persistence.api;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.demo.dummy.persistence.entity.AnotherDummyEntity;
import com.cg.sedp.library.demo.dummy.persistence.entity.DummyType;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DatabaseSetup(AnotherDummyRepositoryDaoUnitTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { AnotherDummyRepositoryDaoUnitTest.DATASET })
@DirtiesContext
@WebAppConfiguration
public class AnotherDummyRepositoryDaoUnitTest {

    public static final String DATASET = "classpath:datasets/dummy-testdata.xml";

    @Autowired
    private AnotherDummyRepositoryDao anotherDummyRepositoryDao;

    @Test
    public void findAnotherEntity() {
        // when
        AnotherDummyEntity anotherDummyEntity = anotherDummyRepositoryDao.findByDummyType(new DummyType("dummy"));
        // then
        assertThat(anotherDummyEntity, is(notNullValue()));
        assertThat(anotherDummyEntity.getId(), is(0l));
        assertThat(anotherDummyEntity.getVersion(), is(0l));
        assertThat(anotherDummyEntity.getStringValue(), is("Jack"));
        assertThat(anotherDummyEntity.getDummyType().getDummyId(), is("dummy"));
    }

}