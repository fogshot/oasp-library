package com.cg.sedp.library.demo.playground;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.cg.sedp.library.demo.playground.logic.api.MediaCrudUsecaseUnitTest;
import com.cg.sedp.library.demo.playground.persistence.api.CustomerDaoUnitTest;
import com.cg.sedp.library.demo.playground.persistence.api.MediaRepositoryDaoUnitTest;
import com.cg.sedp.library.demo.playground.service.api.MediaCrudServiceIntegrationTest;
import com.cg.sedp.library.demo.playground.service.api.PingServiceRestIntegrationTest;

/**
 * Test suite that enumerates all tests of the component playground.
 * 
 * Every developer should add new tests to this suite.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ //
	MediaCrudUsecaseUnitTest.class, //
	CustomerDaoUnitTest.class, //
	MediaRepositoryDaoUnitTest.class, //
	MediaCrudServiceIntegrationTest.class, //
	PingServiceRestIntegrationTest.class })
public class PlaygroundTests {

}
