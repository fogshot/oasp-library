package com.cg.sedp.library.demo.playground.common;

import java.text.SimpleDateFormat;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

import com.cg.sedp.library.demo.playground.common.CommentedMediaTo;
import com.cg.sedp.library.demo.playground.common.PlaygroundMediaType;

public class CommentedMediaToMatcher extends BaseMatcher<CommentedMediaTo>  {
	
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

	private String title;
	private PlaygroundMediaType mediaType;
	private boolean available;
	private String availableDate;
	
	private CommentedMediaToMatcher(final String title, final PlaygroundMediaType mediaType,
			final boolean available, final String availableDate) {
		this.title = title;
		this.mediaType = mediaType;
		this.available = available;
		this.availableDate = availableDate;
	}
	
	public static BaseMatcher<CommentedMediaTo> commentedMediaTo(final String title, final PlaygroundMediaType mediaType,
			final boolean available, final String availableDate) {
		return new CommentedMediaToMatcher(title, mediaType, available, availableDate);
	}

	@Override
	public boolean matches(Object item) {
		final CommentedMediaTo commentedMedia = (CommentedMediaTo) item;
		return (commentedMedia.getMediaType() == mediaType) && //
				commentedMedia.getTitle().equals(title) && //
				commentedMedia.getTitle().equals(title) && //
				(commentedMedia.isAvailable() == available) && //
				((null == availableDate) ? true
						: (dateFormat.format(commentedMedia.getAvailableFrom()).equals(availableDate)));
	}

	@Override
	public void describeTo(Description description) {
		description.appendText(" does not match to CommentedMediaTo " + title + " / " + mediaType + " / "
				+ available + " / " + availableDate);
	}

}
