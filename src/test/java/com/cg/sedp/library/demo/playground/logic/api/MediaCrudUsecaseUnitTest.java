package com.cg.sedp.library.demo.playground.logic.api;

import static com.cg.sedp.library.demo.playground.common.CommentedMediaToMatcher.commentedMediaTo;
import static com.cg.sedp.library.demo.playground.common.CommentedMediaToMatcher.dateFormat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.startsWith;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.validation.Validator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.cg.sedp.library.ccc.user.common.UserEto;
import com.cg.sedp.library.ccc.user.common.UserException;
import com.cg.sedp.library.ccc.user.service.api.UserInformationService;
import com.cg.sedp.library.demo.playground.common.CommentedMediaTo;
import com.cg.sedp.library.demo.playground.common.PlaygroundMediaType;
import com.cg.sedp.library.demo.playground.logic.api.MediaCrudUsecase;
import com.cg.sedp.library.demo.playground.logic.impl.MediaCrudUsecaseImpl;
import com.cg.sedp.library.demo.playground.persistence.api.CommentRepositoryDao;
import com.cg.sedp.library.demo.playground.persistence.api.MediaRepositoryDao;
import com.cg.sedp.library.demo.playground.persistence.entity.PlaygroundMedia;

@RunWith(MockitoJUnitRunner.class)
public class MediaCrudUsecaseUnitTest {

	@InjectMocks
	private MediaCrudUsecase mediaCrudUsecase = new MediaCrudUsecaseImpl();

	@Mock
	private MediaRepositoryDao mediaRepositoryDao;

	@Mock
	private CommentRepositoryDao commentRepositoryDao;

	@Mock
	private Validator validator;

	@Mock
	private UserInformationService userInformationService;

	@Before
	public void setupMocks() throws ParseException, UserException {
		List<PlaygroundMedia> mediaStartingWithBi = new ArrayList<>();
		addMedia(mediaStartingWithBi, 0l, 0l, PlaygroundMediaType.Book, "Bible");
		addMedia(mediaStartingWithBi, 1l, 0l, PlaygroundMediaType.DVD, "Bielefeld - the city");
		addMedia(mediaStartingWithBi, 2l, 0l, PlaygroundMediaType.CD, "Bielefeld - soundtrack of a city");
		when(mediaRepositoryDao.findByTitleStartingWithOrderByTitleAsc(startsWith("Bi")))
				.thenReturn(mediaStartingWithBi);
		List<PlaygroundMedia> someMedia = new ArrayList<>();
		addMedia(someMedia, 0l, 0l, PlaygroundMediaType.Book, "Bible");
		addMedia(someMedia, 1l, 0l, PlaygroundMediaType.CD, "Hamburger Hafenklänge", "01.01.2020");
		when(mediaRepositoryDao.findByTitleStartingWithOrderByTitleAsc("")).thenReturn(someMedia);
		UserEto dummyUserSu = new UserEto();
		dummyUserSu.setUserId("su");
		when(userInformationService.getUser()).thenReturn(dummyUserSu);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testGetMediaStartingWithBi() throws Exception {
		// when
		Collection<CommentedMediaTo> someCommentedMedia = mediaCrudUsecase.getMedia("Bi");
		// then result+mocks
		assertThat(someCommentedMedia.size(), is(3));
		assertThat(someCommentedMedia, //
				containsInAnyOrder( //
						commentedMediaTo("Bible", PlaygroundMediaType.Book, true, null),
						commentedMediaTo("Bielefeld - the city", PlaygroundMediaType.DVD, true, null),
						commentedMediaTo("Bielefeld - soundtrack of a city", PlaygroundMediaType.CD, true, null)));
		verify(mediaRepositoryDao).findByTitleStartingWithOrderByTitleAsc("Bi");
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testGetMedia() throws Exception {
		// when
		Collection<CommentedMediaTo> someCommentedMedia = mediaCrudUsecase.getMedia("");
		// then result+mocks
		assertThat(someCommentedMedia.size(), is(2));
		assertThat(someCommentedMedia, //
				containsInAnyOrder( //
						commentedMediaTo("Bible", PlaygroundMediaType.Book, true, null),
						commentedMediaTo("Hamburger Hafenklänge", PlaygroundMediaType.CD, false, "01.01.2020")));
		verify(mediaRepositoryDao).findByTitleStartingWithOrderByTitleAsc("");
	}

	private void addMedia(List<PlaygroundMedia> allMedia, long id, long version, PlaygroundMediaType mediaType, String title)
			throws ParseException {
		addMedia(allMedia, id, version, mediaType, title, null);
	}

	private void addMedia(List<PlaygroundMedia> allMedia, long id, long version, PlaygroundMediaType mediaType, String title,
			String availableDate) throws ParseException {
		PlaygroundMedia media = new PlaygroundMedia();
		allMedia.add(media);
		media.setId(id);
		media.setVersion(version);
		media.setMediaType(mediaType);
		media.setTitle(title);
		if (null != availableDate)
			media.setAvailableFrom(dateFormat.parse(availableDate));
	}

}