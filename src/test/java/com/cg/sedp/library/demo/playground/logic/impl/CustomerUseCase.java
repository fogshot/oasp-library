package com.cg.sedp.library.demo.playground.logic.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cg.sedp.library.demo.playground.persistence.api.CustomerRepositoryDao;
import com.cg.sedp.library.demo.playground.persistence.entity.Customer;

/**
 * Test usecase just needed for test of DAO to have something at service level
 * to change customers.
 */
@Service
public class CustomerUseCase {
	
	@Autowired
	private CustomerRepositoryDao customerRepository;

	@Transactional
	public Customer saveCustomer(Long id, String newFirstname, String newLastname) {
		Customer toChange = customerRepository.findOne(id);
		toChange.setFirstName(newFirstname);
		toChange.setLastname(newLastname);
		return toChange;
	}
}
