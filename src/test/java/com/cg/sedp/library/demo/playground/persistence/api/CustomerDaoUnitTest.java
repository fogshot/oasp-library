package com.cg.sedp.library.demo.playground.persistence.api;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.demo.playground.logic.impl.CustomerUseCase;
import com.cg.sedp.library.demo.playground.persistence.api.CustomerRepositoryCustomQueryDao;
import com.cg.sedp.library.demo.playground.persistence.api.CustomerRepositoryDao;
import com.cg.sedp.library.demo.playground.persistence.entity.Customer;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DatabaseSetup(CustomerDaoUnitTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { CustomerDaoUnitTest.DATASET })
@DirtiesContext
@WebAppConfiguration
public class CustomerDaoUnitTest {

	public static final String DATASET = "classpath:datasets/customer-testdata.xml";

	@Autowired
	private CustomerRepositoryDao customerRepository;

	@Autowired
	private CustomerRepositoryCustomQueryDao customerRepositoryCustomQueryDao;
	
	@Autowired
	private CustomerUseCase customerUseCase;
	
	@Test
	public void findJack() {
		// when
		List<Customer> customers = customerRepository.findByLastname("Bauer");
		// then
		assertThat(customers, is(notNullValue()));
		assertThat(customers.size(), is(1));
		Customer jackBauer = customers.get(0);
		assertThat(jackBauer.getFirstname(), is("Jack"));
		assertThat(jackBauer.getLastname(), is("Bauer"));
	}

	@Test
	public void findDoelfer() {
		// when / then
		assertThat(customerRepository.findByLastname("Doelfer").size(), is(0));
	}

	@Test
	public void findDoelferWithCustomQuery() {
		// when / then
		assertThat(customerRepositoryCustomQueryDao.findByLastnameCustomQuery("Doelfer").size(), is(0));
	}	

	@Test
	public void findJackWithCustomQuery() {
		// when
		List<Customer> customers = customerRepositoryCustomQueryDao.findByLastnameCustomQuery("Bauer");
		// then
		assertThat(customers, is(notNullValue()));
		assertThat(customers.size(), is(1));
		Customer jackBauer = customers.get(0);
		assertThat(jackBauer.getFirstname(), is("Jack"));
		assertThat(jackBauer.getLastname(), is("Bauer"));
	}
	
	@Test
	public void findNull() {
		assertThat(customerRepository.findByLastname(null).size(), is(0));
	}
	
	@Test
	public void changeNameJack() {
		// when
		Customer jackBower = customerUseCase.saveCustomer(0l, "Jack", "Bower");
		// then
		assertThat(jackBower.getFirstname(), is("Jack"));
		assertThat(jackBower.getLastname(), is("Bower"));
		assertThat(jackBower.getVersion(), is(2L));
	}

}