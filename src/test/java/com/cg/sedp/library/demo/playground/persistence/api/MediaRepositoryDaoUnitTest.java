package com.cg.sedp.library.demo.playground.persistence.api;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import java.text.SimpleDateFormat;
import java.util.List;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.demo.playground.common.PlaygroundMediaType;
import com.cg.sedp.library.demo.playground.persistence.api.MediaRepositoryDao;
import com.cg.sedp.library.demo.playground.persistence.entity.Comment;
import com.cg.sedp.library.demo.playground.persistence.entity.PlaygroundMedia;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DatabaseSetup(MediaRepositoryDaoUnitTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { MediaRepositoryDaoUnitTest.DATASET })
@DirtiesContext
@WebAppConfiguration
public class MediaRepositoryDaoUnitTest {

	public static final String DATASET = "classpath:datasets/playground-testdata.xml";

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	@Autowired
	private MediaRepositoryDao mediaRepository;

	@SuppressWarnings("unchecked")
	@Test
	public void findOneBible() {
		// when
		PlaygroundMedia bible = mediaRepository.findOne(0l);
		// then
		assertThat(bible, is(notNullValue()));
		assertThat(bible.getVersion(), is(0l));
		assertThat(bible.getTitle(), is("Bible"));
		assertThat(bible.getMediaType(), is(PlaygroundMediaType.Book));
		assertThat(dateFormat.format(bible.getAvailableFrom()), is("2016-12-24"));
		assertThat(bible.getUserComments().size(), is(2));
		assertThat(bible.getUserComments(), //
				containsInAnyOrder( //
						comment(0, 0, "alice", "The book of the book"), //
						comment(1, 0, "bob", "Ok, sometimes a bit boring (if i may say so)")));
	}

	@Test
	public void findOneUnknown() {
		// when / then
		assertThat(mediaRepository.findOne(12l), is(nullValue()));
	}

	@Test(expected = InvalidDataAccessApiUsageException.class)
	public void findOneNull() {
		mediaRepository.findOne(null);
	}

	@Test(expected = InvalidDataAccessApiUsageException.class)
	public void findByTitleNull() {
		mediaRepository.findByTitleStartingWithOrderByTitleAsc(null);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void findByTitleEmptyString() {
		// when
		List<PlaygroundMedia> allMedia = mediaRepository.findByTitleStartingWithOrderByTitleAsc("");
		// then
		assertThat(allMedia, hasSize(3));
		assertThat(allMedia,
				containsInAnyOrder(//
						media("Bible", PlaygroundMediaType.Book), //
						media("Bielefeld - Der Film", PlaygroundMediaType.DVD), //
						media("Soundtrack zu Bielefeld", PlaygroundMediaType.CD)));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void findByTitleStartingBi() {
		// when
		List<PlaygroundMedia> allMedia = mediaRepository.findByTitleStartingWithOrderByTitleAsc("Bi");
		// then
		assertThat(allMedia, hasSize(2));
		assertThat(allMedia,
				containsInAnyOrder(//
						media("Bible", PlaygroundMediaType.Book), //
						media("Bielefeld - Der Film", PlaygroundMediaType.DVD)));
	}

	private Matcher<Comment> comment(final long id, final long version, final String userId,
			final String commentString) {
		return new BaseMatcher<Comment>() {

			@Override
			public boolean matches(Object item) {
				final Comment comment = (Comment) item;
				return (comment.getId() == id) && (comment.getVersion() == version)
						&& userId.equals(comment.getUserIdString()) && commentString.equals(comment.getComment());
			}

			@Override
			public void describeTo(Description description) {
				description.appendText(
						" does not match to comment " + id + " / " + version + " / " + userId + " / " + commentString);
			}

		};
	}

	private Matcher<PlaygroundMedia> media(final String title, final PlaygroundMediaType mediaType) {
		return new BaseMatcher<PlaygroundMedia>() {

			@Override
			public boolean matches(Object item) {
				final PlaygroundMedia media = (PlaygroundMedia) item;
				return media.getTitle().equals(title) && (media.getMediaType() == mediaType);
			}

			@Override
			public void describeTo(Description description) {
				description.appendText(" does not match to media " + title + " / " + mediaType);
			}

		};
	}

}