package com.cg.sedp.library.demo.playground.service.api;

import static com.cg.sedp.library.demo.playground.common.CommentedMediaToMatcher.commentedMediaTo;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.ccc.user.common.UserEto;
import com.cg.sedp.library.ccc.user.common.UserException;
import com.cg.sedp.library.ccc.user.common.UserIdType;
import com.cg.sedp.library.ccc.user.service.api.UserInformationService;
import com.cg.sedp.library.demo.playground.common.CommentEto;
import com.cg.sedp.library.demo.playground.common.CommentedMediaTo;
import com.cg.sedp.library.demo.playground.common.PlaygroundMediaType;
import com.cg.sedp.library.demo.playground.logic.api.MediaCrudUsecase;
import com.cg.sedp.library.demo.playground.persistence.api.CommentRepositoryDao;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LibraryApplication.class)
@DatabaseSetup(MediaCrudServiceIntegrationTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { MediaCrudServiceIntegrationTest.DATASET })
@DirtiesContext
@WebAppConfiguration
public class MediaCrudServiceIntegrationTest {

	public static final String DATASET = "classpath:datasets/playground-testdata.xml";

	private static final long BIBLE_ID = 0l;
	private static final String SUPERUSER_ID = "su";
	private static final UserIdType SUPERUSER_USERID_TYPE = new UserIdType(SUPERUSER_ID);
	private static final String BIBLE_TITLE = "Bible";

	@Autowired
	@InjectMocks
	private MediaCrudService mediaCrudService;

	@Autowired
	@InjectMocks
	private MediaCrudUsecase mediaCrudUsecase;

	@Autowired
	private CommentRepositoryDao commentRepositoryDao;

	@Mock
	private UserInformationService userInformationService;

	@Before
	public void setupMocks() throws UserException {
		MockitoAnnotations.initMocks(this);
		UserEto dummyUserSu = new UserEto();
		dummyUserSu.setUserId(SUPERUSER_ID);
		when(userInformationService.getUser()).thenReturn(dummyUserSu);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getMediaPositiveCaseAll() {
		/// when
		Collection<CommentedMediaTo> allMedia = mediaCrudService.getMedia("");
		// then
		assertThat(allMedia.size(), is(3));
		assertThat(allMedia, //
				containsInAnyOrder( //
						commentedMediaTo(BIBLE_TITLE,               PlaygroundMediaType.Book, false, "24.12.2016"), //
						commentedMediaTo("Soundtrack zu Bielefeld", PlaygroundMediaType.CD, true, null), //
						commentedMediaTo("Bielefeld - Der Film",    PlaygroundMediaType.DVD, true, null)));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getMediaPositiveCaseBi() {
		/// when
		Collection<CommentedMediaTo> allMedia = mediaCrudService.getMedia("Bi");
		// then
		assertThat(allMedia.size(), is(2));
		assertThat(allMedia, //
				containsInAnyOrder( //
						commentedMediaTo(BIBLE_TITLE,               PlaygroundMediaType.Book, false, "24.12.2016"), //
						commentedMediaTo("Bielefeld - Der Film",    PlaygroundMediaType.DVD, true, null)));
	}

	@Test(expected = IllegalArgumentException.class)
	public void getMediaCalledWithNull() {
		mediaCrudService.getMedia(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void saveCommentWithNull() {
		mediaCrudService.saveComment(null);
	}

	@Test(expected = IllegalStateException.class)
	public void saveCommentWithInvalidEmptyArgument() {
		mediaCrudService.saveComment(new CommentEto());
	}

	@Test
	public void saveCommentNewComment() {
		// given
		CommentEto bibleComment = new CommentEto();
		bibleComment.setComment("the book of the books");
		bibleComment.setMediaId(BIBLE_ID);
		bibleComment.setUserId(SUPERUSER_USERID_TYPE);
		// when
		mediaCrudService.saveComment(bibleComment);
		// then
		Collection<CommentedMediaTo> bibleMedia = mediaCrudService.getMedia(BIBLE_TITLE);
		CommentEto userComment = filterForBibleCommentFromSuperuser(bibleMedia);
		assertThat(userComment.getComment(), is("the book of the books"));
		assertThat(userComment.getVersion(), is(0L));
	}

	@Test
	public void saveCommentInitiallyNoSuperUserComent() {
		// when
		Collection<CommentedMediaTo> bibleMedia = mediaCrudService.getMedia(BIBLE_TITLE);
		// then
		CommentEto userComment = filterForBibleCommentFromSuperuser(bibleMedia);
		assertThat(userComment.getComment(), is(""));
		assertThat(userComment.getUserId(), is(SUPERUSER_USERID_TYPE));
		assertThat(userComment.getId(), is(nullValue()));
		assertThat(userComment.getVersion(), is(0L));
	}

	@Test
	public void saveCommentUpdatingComment() {
		// given
		CommentEto bibleComment = new CommentEto();
		bibleComment.setComment("the book of the books");
		bibleComment.setMediaId(BIBLE_ID);
		bibleComment.setUserId(SUPERUSER_USERID_TYPE);
		mediaCrudService.saveComment(bibleComment);
		Collection<CommentedMediaTo> bibleMedia = mediaCrudService.getMedia(BIBLE_TITLE);
		CommentEto commentReloaded = filterForBibleCommentFromSuperuser(bibleMedia);
		Long commentId = commentReloaded.getId();
		assertThat(commentRepositoryDao.findOne(commentId).getVersion(), is(0L));
		// when
		commentReloaded.setComment("finished it, very boring");
		mediaCrudService.saveComment(commentReloaded);
		// then
		assertThat(commentRepositoryDao.findOne(commentId).getVersion(), is(1L));
	}

	private CommentEto filterForBibleCommentFromSuperuser(Collection<CommentedMediaTo> bibleMedia) {
		CommentEto returnValue =  bibleMedia.stream().filter(x -> x.getTitle().equals(BIBLE_TITLE)).findFirst().orElse(null).getUserComment();
		assertThat(returnValue, is(notNullValue()));
		return returnValue;
	}

}