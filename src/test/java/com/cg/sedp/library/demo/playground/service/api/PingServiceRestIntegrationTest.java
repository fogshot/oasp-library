package com.cg.sedp.library.demo.playground.service.api;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import com.cg.sedp.library.LibraryApplication;
import com.cg.sedp.library.common.tools.AbstractRestTestClass;
import com.cg.sedp.library.demo.playground.common.PingInputTo;
import com.cg.sedp.library.demo.playground.service.api.PingService;
import com.cg.sedp.library.demo.playground.service.impl.PingRestServiceImpl;

@SpringApplicationConfiguration(classes = LibraryApplication.class)
public class PingServiceRestIntegrationTest extends AbstractRestTestClass {
	
	@Before
	public void init() throws Exception {
		setup();
	}

	@Test
	public void testPing() throws Exception {
		// when
		PingInputTo pingInput;
		ResultActions resultActions = mockMvc.perform(//
				post(PingRestServiceImpl.PING_URL) //
						.contentType(jsonUtf8) //
						.content(json(pingInput = new PingInputTo("alice"))));
		// then
		resultActions.andDo(print()) //
				.andExpect(status().isOk())//
				.andExpect(jsonPath("$.callerId", is(pingInput.getCallerId())))
				.andExpect(jsonPath("$.callTimestampFormatted", is(pingInput.getCallTimestampFormatted())))
				.andExpect(jsonPath("$.message", is(PingService.SERVICE_ALIVE)));
	}

	@Test
	public void testAlive() throws Exception {
		// when
		ResultActions resultActions = mockMvc.perform(//
				get(PingRestServiceImpl.PING_ALIVE_URL));
		// then
		MvcResult mvcResult = resultActions.andDo(print()) //
								.andExpect(status().isOk())//
								.andReturn();
		assertThat(mvcResult.getResponse().getContentAsString(), startsWith(PingService.SERVICE_ALIVE));
	}
	
}